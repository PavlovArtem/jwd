package by.pavlov.trainapp.command;

import by.pavlov.trainapp.entity.Wagon;
import by.pavlov.trainapp.service.WagonService;
import by.pavlov.trainapp.service.WagonStatisticService;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.MessageFormat;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CommandDisplayAllWagon implements Command {

    private final WagonService wagonService;
    private final WagonStatisticService wagonStatisticService;

    public CommandDisplayAllWagon(WagonService wagonService, WagonStatisticService wagonStatisticService) {
        this.wagonService = wagonService;
        this.wagonStatisticService = wagonStatisticService;
    }

    @Override
    public String execute(Map<String, String> userData) {

        return formResponse();
    }

    private String formResponse() {
        InputStream resourceTemplate = getClass().getResourceAsStream("/index.html");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(resourceTemplate));
        String template = bufferedReader.lines().collect(Collectors.joining());

        StringBuilder trainResponse = new StringBuilder();
        trainResponse.append("<p>")
                .append(getTrainStatistic())
                .append("</p>");
        trainResponse.append("<p>Sort options</p>\n" +
                "<form action=\"/train\" method=\"POST\" >\n" +
                "\t<p><input type=\"checkbox\" name=\"option_id\" value=\"ID_SORT\"> Id<Br>\n" +
                "\t<input type=\"checkbox\" name=\"option_name\" value=\"NAME_SORT\"> Name <Br>\n" +
                "\t<input type=\"checkbox\" name=\"option_passengers\" value=\"PASSENGERS_NUMBER_SORT\"> Passengers <Br>\n" +
                "\t<input type=\"checkbox\" name=\"option_baggage\" value=\"BAGGAGE_NUMBER_SORT\"> Baggage<Br>\n" +
                "\t<input type=\"checkbox\" name=\"type\" value=\"WAGON_TYPE_SORT\"> Type</p>\n" +
                "\t<p>Order</p>\n" +
                "\t<p><input type=\"radio\" id=\"order1\" name=\"order\" value=\"ASC\"><label for=\"order1\"> Ascending</label> \n" +
                "\t<input type=\"radio\" id=\"order2\" name=\"order\" value=\"DSC\"><label for=\"order2\">Descending</label>\n" +
                "\t<input type=\"hidden\" name=\"commandName\" value=\"SORT_WAGONS\">\n" +
                "\t<button> Sort </button>\n" +
                "</form>");
        trainResponse.append("<ul");

        for (Wagon wagon : getAllWagon()) {
            trainResponse.append("<li>");
            trainResponse.append(wagon.toString());
            trainResponse.append("</li>");
        }
        trainResponse.append("</ul>");

        return MessageFormat.format(template, trainResponse);
    }

    private List<Wagon> getAllWagon() {
        return wagonService.findAllWagon();
    }

    private String getTrainStatistic() {
        Integer numberOfPassengers = wagonStatisticService.calculateNumberOfPassengers();
        Integer numberOfBaggage = wagonStatisticService.calculateNumberOfBaggage();
        return "Number of passengers :" + numberOfPassengers + "\n"
                + "Number of baggage :" + numberOfBaggage + "\n";
    }

}
