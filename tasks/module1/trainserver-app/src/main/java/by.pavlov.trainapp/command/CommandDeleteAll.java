package by.pavlov.trainapp.command;

import by.pavlov.trainapp.service.WagonService;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.MessageFormat;
import java.util.Map;
import java.util.stream.Collectors;

public class CommandDeleteAll implements Command {

    private final WagonService wagonService;

    public CommandDeleteAll(WagonService wagonService) {
        this.wagonService = wagonService;
    }


    @Override
    public String execute(Map<String, String> userData) {

        return formResponse();
    }

    private String formResponse() {

        String response = "";

        if (wagonService.findAllWagon().isEmpty()) {
            response = "Nothing to delete";
        } else {
            wagonService.deleteAllWagon();
            response = "Wagons successfully deleted";
        }

        return MessageFormat.format(getTemplate(), response);


    }

    private String getTemplate() {
        InputStream resourceTemplate = getClass().getResourceAsStream("/index.html");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(resourceTemplate));
        return bufferedReader.lines().collect(Collectors.joining());
    }

}

