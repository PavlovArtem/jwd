package by.pavlov.trainapp.command;

public interface CommandFactory {
    Command getCommand(String commandName);
}
