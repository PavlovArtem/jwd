package by.pavlov.trainapp.command;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.MessageFormat;
import java.util.Map;
import java.util.stream.Collectors;

public class CommandMainPage implements Command {


    @Override
    public String execute(Map<String, String> userData) {

        return formResponse();
    }

    private String formResponse() {
        InputStream resourceAsStream = getClass().getResourceAsStream("/index.html");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(resourceAsStream));
        String template = bufferedReader.lines().collect(Collectors.joining());

        StringBuilder response = new StringBuilder();
        response.append("<p> Hello user </p>");


        return MessageFormat.format(template, response);
    }


}
