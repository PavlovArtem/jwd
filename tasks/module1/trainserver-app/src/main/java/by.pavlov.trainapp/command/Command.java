package by.pavlov.trainapp.command;

import java.util.Map;

public interface Command {
    String execute(Map<String, String> userData);
}
