package by.pavlov.trainapp.command;

import by.pavlov.trainapp.entity.Wagon;
import by.pavlov.trainapp.service.WagonService;
import by.pavlov.trainapp.util.CSVDataConverter;
import by.pavlov.trainapp.util.DataConverter;
import by.pavlov.trainapp.util.DataReader;
import by.pavlov.trainapp.util.FileDataReader;
import by.pavlov.trainapp.validation.CSVDataValidator;
import by.pavlov.trainapp.validation.PathValidator;
import by.pavlov.trainapp.validation.ValidationResult;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.MessageFormat;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CommandReadTrainDataFromFile implements Command {

    private static final Logger logger = Logger.getLogger(CommandReadTrainDataFromFile.class);

    private final WagonService wagonService;
    private final PathValidator pathValidator = new PathValidator();
    private final CSVDataValidator dataValidator = new CSVDataValidator();
    private final DataReader fileDataReader = new FileDataReader();
    private final DataConverter dataConverter = new CSVDataConverter();


    public CommandReadTrainDataFromFile(WagonService wagonService) {
        this.wagonService = wagonService;
    }

    @Override
    public String execute(Map<String, String> userData) {


        String message = "";

        String path = userData.get("path");
        ValidationResult validationResult = pathValidator.validate(path);


        if (validationResult.isNotValid()) {
            logger.info("unsuccessful read of file" + validationResult.getMessage());
            return formResponse(validationResult.getMessage());
        }

        String fileData = readFile(path);
        validationResult = dataValidator.validate(fileData);
        if (validationResult.isNotValid()) {
            logger.info("unsuccessful read of file" + validationResult.getMessage());
            return formResponse(validationResult.getMessage());
        }

        List<Wagon> wagonsFromFileData = getWagonsFromFileData(fileData);
        uploadConvertedFileData(wagonsFromFileData);
        message = "Data from file successfully uploaded";
        return formResponse(message);
    }

    private String formResponse(String message) {
        String template = getTemplate();

        StringBuilder response = new StringBuilder();
        response.append("<p>" + message + "</p>");
        response.append("<form action=\"/train\" method=\"GET\"> \n" +
                "\t<input type=\"hidden\" name=\"commandName\" value=\"DISPLAY_ALL_WAGON\">\n" +
                "\t<button> Show all </button>\n" +
                "</form>\n" +
                "<form action=\"/train\" method=\"GET\"> \n" +
                "\t<input type=\"hidden\" name=\"commandName\" value=\"MAIN_PAGE\">\n" +
                "\t<button> Main </button>\n" +
                "</form>");


        return MessageFormat.format(template, response.toString());

    }

    private String getTemplate() {
        InputStream resourceTemplate = getClass().getResourceAsStream("/index.html");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(resourceTemplate));
        return bufferedReader.lines().collect(Collectors.joining());
    }


    private void uploadConvertedFileData(List<Wagon> wagons) {

        wagonService.uploadWagon(wagons);
    }

    private List<Wagon> getWagonsFromFileData(String fileData) {
        return dataConverter.convert(fileData);
    }

    private String readFile(String path) {

        String fileData = "";

        try {
            fileData = fileDataReader.read(path);
        } catch (IOException exception) {
            logger.warn("file can't be read", exception);

        }

        return fileData;
    }


}
