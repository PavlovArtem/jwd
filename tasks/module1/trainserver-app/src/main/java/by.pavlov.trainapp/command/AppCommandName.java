package by.pavlov.trainapp.command;

public enum AppCommandName {
    MAIN_PAGE,
    LOAD_FROM_FILE,
    DISPLAY_ALL_WAGON,
    SORT_WAGONS,
    DELETE_ALL_WAGON


}
