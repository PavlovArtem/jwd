package by.pavlov.trainapp.command;

import by.pavlov.trainapp.entity.Wagon;
import by.pavlov.trainapp.service.WagonService;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CommandSortWagons implements Command {

    private final WagonService wagonService;

    public CommandSortWagons(WagonService wagonService) {
        this.wagonService = wagonService;
    }


    @Override
    public String execute(Map<String, String> userData) {

        return formResponse(userData);
    }

    private String formResponse(Map<String, String> userData) {

        InputStream resourceTemplate = getClass().getResourceAsStream("/index.html");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(resourceTemplate));
        String template = bufferedReader.lines().collect(Collectors.joining());

        StringBuilder trainResponse = new StringBuilder();
        trainResponse.append("<p>Sort options</p>\n" +
                "<form action=\"/train\" method=\"POST\" >\n" +
                "\t<p><input type=\"checkbox\" name=\"option_id\" value=\"ID_SORT\"> Id<Br>\n" +
                "\t<input type=\"checkbox\" name=\"option_name\" value=\"NAME_SORT\"> Name <Br>\n" +
                "\t<input type=\"checkbox\" name=\"option_passengers\" value=\"PASSENGERS_NUMBER_SORT\"> Passengers <Br>\n" +
                "\t<input type=\"checkbox\" name=\"option_baggage\" value=\"BAGGAGE_NUMBER_SORT\"> Baggage<Br>\n" +
                "\t<input type=\"checkbox\" name=\"type\" value=\"WAGON_TYPE_SORT\"> Type</p>\n" +
                "\t<p>Order</p>\n" +
                "\t<p><input type=\"radio\" id=\"order1\" name=\"order\" value=\"ASC\"><label for=\"order1\"> Ascending</label> \n" +
                "\t<input type=\"radio\" id=\"order2\" name=\"order\" value=\"DSC\"><label for=\"order2\">Descending</label>\n" +
                "\t<input type=\"hidden\" name=\"commandName\" value=\"SORT_WAGONS\">\n" +
                "\t<button> Sort </button>\n" +
                "</form>");
        trainResponse.append("<ul");

        for (Wagon wagon : getSortedWagons(
                getCriteriaFromUserData(userData))) {
            trainResponse.append("<li>");
            trainResponse.append(wagon.toString());
            trainResponse.append("</li>");
        }
        trainResponse.append("</ul>");

        return MessageFormat.format(template, trainResponse);
    }

    private List<Wagon> getSortedWagons(Map<String, String> criteria) {
        return wagonService.sortAllByCriteria(criteria);
    }


    private Map<String, String> getCriteriaFromUserData(Map<String, String> userData) {

        Map<String, String> criteria = new HashMap<>();

        for (Map.Entry<String, String> data : userData.entrySet()) {
            String dataKey = data.getKey();

            if (dataKey.startsWith("option") || dataKey.startsWith("order")) {
                criteria.put(dataKey, data.getValue());
            }
        }

        return criteria;
    }


}
