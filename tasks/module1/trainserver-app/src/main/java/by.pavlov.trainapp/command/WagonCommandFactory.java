package by.pavlov.trainapp.command;

import java.util.Map;

public class WagonCommandFactory implements CommandFactory {

    private final Map<String, Command> commands;

    public WagonCommandFactory(Map<String, Command> commands) {
        this.commands = commands;
    }

    @Override
    public Command getCommand(String commandName) {
        return commands.getOrDefault(commandName, commands.get(AppCommandName.MAIN_PAGE.name()));
    }


}
