package by.pavlov.trainapp;

import by.pavlov.trainapp.command.*;
import by.pavlov.trainapp.controller.TrainCreatorHandler;
import by.pavlov.trainapp.dao.WagonDAO;
import by.pavlov.trainapp.dao.WagonInMemoryDAO;
import by.pavlov.trainapp.service.WagonService;
import by.pavlov.trainapp.service.WagonServiceImpl;
import by.pavlov.trainapp.service.WagonStatisticService;
import by.pavlov.trainapp.service.WagonStatisticServiceImpl;
import com.sun.net.httpserver.HttpServer;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class TrainServerApp {

    private final static Logger logger = Logger.getLogger(TrainServerApp.class);

    public static void main(String[] args) throws IOException {


        InetSocketAddress localhost = new InetSocketAddress("localhost", 8080);
        HttpServer httpServer;
        try {
            httpServer = HttpServer.create(localhost, 0);
        } catch (IOException exception) {
            logger.log(Level.ERROR, "Error occurred while creating server " + exception.getMessage());
            return;
        }


        WagonDAO wagonDAO = new WagonInMemoryDAO(5);
        WagonService wagonService = new WagonServiceImpl(wagonDAO);
        WagonStatisticService wagonStatisticService = new WagonStatisticServiceImpl(wagonService);

        //commands init
        Map<String, Command> commands = new HashMap<>();
        commands.put(AppCommandName.MAIN_PAGE.name(), new CommandMainPage());
        commands.put(AppCommandName.DISPLAY_ALL_WAGON.name(), new CommandDisplayAllWagon(wagonService,wagonStatisticService));
        commands.put(AppCommandName.LOAD_FROM_FILE.name(), new CommandReadTrainDataFromFile(wagonService));
        commands.put(AppCommandName.SORT_WAGONS.name(), new CommandSortWagons(wagonService));
        commands.put(AppCommandName.DELETE_ALL_WAGON.name(), new CommandDeleteAll(wagonService));
        CommandFactory commandFactory = new WagonCommandFactory(commands);

        httpServer.createContext("/train", new TrainCreatorHandler(commandFactory));

        ExecutorService executor = Executors.newFixedThreadPool(4);
        httpServer.setExecutor(executor);
        httpServer.start();

        logger.log(Level.INFO, "Server started on " + httpServer.getAddress().toString());

    }


}
