package by.pavlov.trainapp.entity;

import java.util.Objects;

public class Wagon {

    private final Long id;
    private final String name;
    private final Integer numberOfPassengers;
    private final Integer numberOfBaggage;
    private final WagonType wagonType;

    public Wagon(Long id, String name, Integer numberOfPassengers, Integer numberOfBaggage, WagonType wagonType) {
        this.id = id;
        this.name = name;
        this.numberOfPassengers = numberOfPassengers;
        this.numberOfBaggage = numberOfBaggage;
        this.wagonType = wagonType;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getNumberOfPassengers() {
        return numberOfPassengers;
    }

    public Integer getNumberOfBaggage() {
        return numberOfBaggage;
    }

    public WagonType getWagonType() {
        return wagonType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Wagon wagon = (Wagon) o;
        return numberOfPassengers == wagon.numberOfPassengers &&
                numberOfBaggage == wagon.numberOfBaggage &&
                Objects.equals(id, wagon.id) &&
                Objects.equals(name, wagon.name) &&
                wagonType == wagon.wagonType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, numberOfPassengers, numberOfBaggage, wagonType);
    }

    @Override
    public String toString() {
        return "Wagon{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", numberOfPassengers=" + numberOfPassengers +
                ", numberOfBaggage=" + numberOfBaggage +
                ", wagonType=" + wagonType +
                '}';
    }
}
