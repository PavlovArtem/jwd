package by.pavlov.trainapp.util;

import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;


public class FileDataReader implements DataReader {
    private static final Logger logger = Logger.getLogger(FileDataReader.class);

    @Override
    public String read(String path) throws IOException {


        StringBuilder stringBuilder = new StringBuilder();
        try {

            FileReader fileReader = new FileReader(path);

            try (BufferedReader br = new BufferedReader(fileReader)) {
                String currentLine;

                while ((currentLine = br.readLine()) != null) {
                    stringBuilder.append(currentLine);
                    stringBuilder.append("\n");
                }

            } catch (IOException exception) {
                logger.error("error while reading .csv file");
            }
        } catch (FileNotFoundException e) {
            logger.error("file not found with path " + path);
        }


        return stringBuilder.toString();
    }


}
