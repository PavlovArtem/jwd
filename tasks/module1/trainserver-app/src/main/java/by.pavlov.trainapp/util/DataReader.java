package by.pavlov.trainapp.util;

import java.io.IOException;

public interface DataReader {
    String read(String path) throws IOException;
}
