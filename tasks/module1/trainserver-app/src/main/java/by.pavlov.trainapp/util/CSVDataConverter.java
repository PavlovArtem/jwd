package by.pavlov.trainapp.util;

import by.pavlov.trainapp.entity.Wagon;
import by.pavlov.trainapp.entity.WagonType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CSVDataConverter implements DataConverter {

    @Override
    public List<Wagon> convert(String data) {
        List<Wagon> wagonList = new ArrayList<>();

        List<String> lines = Arrays.asList(data.split("\n"));
        for (int i = 1; i < lines.size(); i++) {
            List<String> line = Arrays.asList(lines.get(i).split(","));
            wagonList.add(new Wagon(
                    Long.valueOf(line.get(0)),
                    line.get(1),
                    Integer.valueOf(line.get(2)),
                    Integer.valueOf(line.get(3)),
                    WagonType.valueOf(line.get(4))
            ));
        }

        return wagonList;
    }
}
