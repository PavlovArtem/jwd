package by.pavlov.trainapp.util;

import by.pavlov.trainapp.entity.Wagon;

import java.util.List;

public interface DataConverter {

    List<Wagon> convert(String data);

}
