package by.pavlov.trainapp.service;

import by.pavlov.trainapp.entity.Wagon;

import java.util.List;
import java.util.Map;

public interface WagonService {

    List<Wagon> findAllWagon();

    List<Wagon> sortAllByCriteria(Map<String,String> criteria);

    void uploadWagon(List<Wagon> wagons);

    void deleteWagonById(Long id);

    void deleteAllWagon();


}
