package by.pavlov.trainapp.service;

import by.pavlov.trainapp.entity.Wagon;

import java.util.IntSummaryStatistics;
import java.util.stream.Collectors;

public class WagonStatisticServiceImpl implements WagonStatisticService {

    private final WagonService wagonService;

    public WagonStatisticServiceImpl(WagonService wagonService) {
        this.wagonService = wagonService;
    }

    @Override
    public Integer calculateNumberOfPassengers() {

        IntSummaryStatistics sum = wagonService.findAllWagon()
                .stream()
                .collect(Collectors.summarizingInt(Wagon::getNumberOfPassengers));

        return (int) sum.getSum();
    }

    @Override
    public Integer calculateNumberOfBaggage() {
        IntSummaryStatistics sum = wagonService.findAllWagon()
                .stream()
                .collect(Collectors.summarizingInt(Wagon::getNumberOfBaggage));
        return (int) sum.getSum();
    }
}
