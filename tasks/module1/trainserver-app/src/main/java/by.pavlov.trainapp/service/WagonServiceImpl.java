package by.pavlov.trainapp.service;

import by.pavlov.trainapp.dao.WagonDAO;
import by.pavlov.trainapp.entity.Wagon;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class WagonServiceImpl implements WagonService {

    private final WagonDAO wagonDAO;

    public WagonServiceImpl(WagonDAO wagonDAO) {
        this.wagonDAO = wagonDAO;
    }

    @Override
    public List<Wagon> findAllWagon() {
        return wagonDAO.findAllWagon();
    }

    @Override
    public List<Wagon> sortAllByCriteria(Map<String,String> criteria) {


        return wagonDAO.sortAllByCriteria(criteria);
    }

    @Override
    public void uploadWagon(List<Wagon> wagons) {
        wagonDAO.addAllWagon(wagons);
    }

    @Override
    public void deleteWagonById(Long id) {
        wagonDAO.deleteWagonById(id);
    }

    @Override
    public void deleteAllWagon() {
        wagonDAO.deleteAllWagon();
    }


}
