package by.pavlov.trainapp.service;

public interface WagonStatisticService {

    Integer calculateNumberOfPassengers();

    Integer calculateNumberOfBaggage();

}
