package by.pavlov.trainapp.controller;

import by.pavlov.trainapp.command.Command;
import by.pavlov.trainapp.command.CommandFactory;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import org.apache.log4j.Logger;

import java.io.*;
import java.net.URI;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

public class TrainCreatorHandler implements HttpHandler {

    private final static Logger logger = Logger.getLogger(TrainCreatorHandler.class);
    private final CommandFactory commandFactory;


    public TrainCreatorHandler(CommandFactory commandFactory) {
        this.commandFactory = commandFactory;
    }

    @Override
    public void handle(HttpExchange exchange) throws IOException {

        logger.info(exchange.getRequestURI().toString());

        Map<String, String> params = getRequestParams(exchange);

        Command command = commandFactory.getCommand(params.get("commandName"));
        String view = command.execute(params);

        OutputStream responseBody = exchange.getResponseBody();
        try {
            exchange.sendResponseHeaders(200, view.length());
            responseBody.write(view.getBytes());
            responseBody.flush();
            responseBody.close();
        } catch (IOException ex) {
            logger.error("error occurred while writing response", ex);
            exchange.sendResponseHeaders(500, view.length());
        }

    }

    private Map<String, String> getRequestParams(HttpExchange exchange) throws UnsupportedEncodingException {
        List<String> query = new ArrayList<>();

        if ("POST".equals(exchange.getRequestMethod())) {
            query.add(URLDecoder.decode(new BufferedReader(new InputStreamReader(exchange.getRequestBody(), StandardCharsets.UTF_8))
                    .lines()
                    .collect(Collectors.joining()), StandardCharsets.UTF_8.name()));
        }

        URI requestURI = exchange.getRequestURI();
        String getQuery = requestURI.getQuery();

        if (getQuery != null) {
            query.add(getQuery);
        }

        return parseRequest(query);
    }

    private Map<String, String> parseRequest(List<String> query) {
        Map<String, String> queryParams = new HashMap<>();

        List<String> params = new ArrayList<>();
        for (String param : query) {
            params.addAll(Arrays.asList(param.split("&")));
        }

        for (String s : params) {
            String[] keyValue = s.split("=");
            queryParams.put(keyValue[0], keyValue[1]);
        }

        return queryParams;
    }

}
