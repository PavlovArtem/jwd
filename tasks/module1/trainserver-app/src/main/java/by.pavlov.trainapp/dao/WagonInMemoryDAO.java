package by.pavlov.trainapp.dao;

import by.pavlov.trainapp.entity.Wagon;
import by.pavlov.trainapp.entity.WagonType;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class WagonInMemoryDAO implements WagonDAO {

    private final List<Wagon> wagonList;

    public WagonInMemoryDAO(int quantity) {
        wagonList = new ArrayList<>();
        for (int i = 0; i < quantity; i++) {
            Long id = (long) (i + 1);
            String name = "Wagon number" + i;
            WagonType wagonType = i % 2 == 0 ? WagonType.CARGO : WagonType.PASSENGER;
            int numberOfPassengers = i % 2 == 0 ? 10 : 15 + i;
            int numberOfBaggage = i % 2 == 0 ? 5 : 10;
            wagonList.add(new Wagon(id, name, numberOfPassengers, numberOfBaggage, wagonType));
        }

    }


    @Override
    public List<Wagon> findAllWagon() {
        return new ArrayList<>(wagonList);
    }

    @Override
    public void addAllWagon(List<Wagon> wagons) {
        wagonList.addAll(wagons);
    }

    @Override
    public List<Wagon> sortAllByCriteria(Map<String, String> criteria) {


        List<WagonComparator> comparators = getComparatorFromCriteria(criteria);

        if ("ASC".equals(criteria.get("order"))) {
            wagonList.sort(WagonComparator.getComparator(comparators));
        } else {
            wagonList.sort(WagonComparator.descending(WagonComparator.getComparator(comparators)));
        }

        return new ArrayList<>(wagonList);
    }

    private List<WagonComparator> getComparatorFromCriteria(Map<String, String> criteria) {
        List<WagonComparator> comparatorList = new ArrayList<>();

        for (Map.Entry<String, String> option : criteria.entrySet()) {
            if (option.getKey().startsWith("option")){
                comparatorList.add(WagonComparator.valueOf(option.getValue()));
            }

        }

        return comparatorList;
    }


    @Override
    public void deleteWagonById(Long id) {
        wagonList.removeIf(wagon -> wagon.getId().equals(id));
    }

    @Override
    public void deleteAllWagon() {
        wagonList.clear();
    }

}
