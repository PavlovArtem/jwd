package by.pavlov.trainapp.dao;

import by.pavlov.trainapp.entity.Wagon;

import java.util.List;
import java.util.Map;

public interface WagonDAO {

    List<Wagon> findAllWagon();

    List<Wagon> sortAllByCriteria(Map<String,String> criteria);

    void addAllWagon(List<Wagon> wagons);

    void deleteWagonById(Long id);

    void deleteAllWagon();

}
