package by.pavlov.trainapp.dao;

import by.pavlov.trainapp.entity.Wagon;

import java.util.Comparator;
import java.util.List;

public enum WagonComparator implements Comparator<Wagon> {
    ID_SORT {
        public int compare(Wagon w1, Wagon w2) {
            return w1.getId().compareTo(w2.getId());
        }
    },
    NAME_SORT {
        public int compare(Wagon w1, Wagon w2) {
            return w1.getName().compareTo(w2.getName());
        }
    },
    PASSENGERS_NUMBER_SORT {
        public int compare(Wagon w1, Wagon w2) {
            return w1.getNumberOfPassengers().compareTo(w2.getNumberOfPassengers());
        }
    },
    BAGGAGE_NUMBER_SORT {
        public int compare(Wagon w1, Wagon w2) {
            return w1.getNumberOfBaggage().compareTo(w2.getNumberOfBaggage());
        }
    },
    WAGON_TYPE_SORT {
        public int compare(Wagon w1, Wagon w2) {
            return w1.getWagonType().compareTo(w2.getWagonType());
        }
    };

    public static Comparator<Wagon> descending(final Comparator<Wagon> wagonComparator){
        return (w1,w2) -> {
            return -1 * wagonComparator.compare(w1,w2);
        };
    }

    public static Comparator<Wagon> getComparator(final List<WagonComparator> wagonComparators) {
        return (w1, w2) -> {
            for (WagonComparator option : wagonComparators) {
                int result = option.compare(w1, w2);
                if (result != 0) {
                    return result;
                }
            }
            return 0;
        };
    }

}
