package by.pavlov.trainapp.validation;

public class ValidationResult {

    private String message;

    public boolean isNotValid(){
        return message != null && !message.isEmpty();

    }
    public String getMessage(){return message;}

    public static ValidationResult reject(String message){
        ValidationResult validationResult = new ValidationResult();
        validationResult.message = message;
        return validationResult;
    }

    public static ValidationResult success(){
        return new ValidationResult();
    }

}
