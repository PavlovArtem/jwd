package by.pavlov.trainapp.validation;

import java.nio.file.Files;
import java.nio.file.Paths;

public class PathValidator {

    public ValidationResult validate(String path) {
        if (!Files.exists(Paths.get(path))) {
            return ValidationResult.reject("Incorrect file path " + path);
        }

        return ValidationResult.success();
    }

}
