package by.pavlov.trainapp.validation;

import by.pavlov.trainapp.entity.WagonType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CSVDataValidator {


    public ValidationResult validate(String data) {

        if (data.isEmpty()) {
            return ValidationResult.reject("Data is empty");
        } else {
            return validateLines(data);
        }

    }


    private ValidationResult validateLines(String data) {

        List<String> incorrectData = new ArrayList<>(0);
        List<String> lines = Arrays.asList(data.split("\n"));
        for (int i = 1; i < lines.size(); i++) {
            List<String> line = Arrays.asList(lines.get(i).split(","));
            if (line.size() != 5) {
                incorrectData.add("Invalid number of argument in line "
                        + i + " "
                        + String.join(",", line + "\n"));
            } else {
                try {
                    Long.valueOf(line.get(0)); //id check for Long
                    Integer.valueOf(line.get(2)); //passengers check for int
                    Integer.valueOf(line.get(3)); //baggage check for int
                    WagonType.valueOf(line.get(4)); //wagon type check for exist type
                } catch (Exception exception) {
                    incorrectData.add("Wrong type in line "
                            + i + " "
                            + String.join(",", line + "\n"));
                }
            }
        }
        if (!incorrectData.isEmpty()) {
            return ValidationResult.reject(String.join("\n", incorrectData));
        }

        return ValidationResult.success();


    }

}



