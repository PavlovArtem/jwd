package by.pavlov.trainapp;

import by.pavlov.trainapp.command.*;
import by.pavlov.trainapp.dao.WagonDAO;
import by.pavlov.trainapp.dao.WagonInMemoryDAO;
import by.pavlov.trainapp.entity.Wagon;
import by.pavlov.trainapp.service.WagonService;
import by.pavlov.trainapp.service.WagonServiceImpl;
import by.pavlov.trainapp.service.WagonStatisticService;
import by.pavlov.trainapp.service.WagonStatisticServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(JUnit4.class)
public class TrainAppTest {


    private final WagonDAO wagonDAO = new WagonInMemoryDAO(5);
    private final WagonService wagonService = new WagonServiceImpl(wagonDAO);
    private final WagonStatisticService wagonStatisticService = new WagonStatisticServiceImpl(wagonService);

    private final CommandFactory commandFactory = new WagonCommandFactory(commandsInit());

    private Map<String, Command> commandsInit() {
        Map<String, Command> commands = new HashMap<>();
        commands.put(AppCommandName.MAIN_PAGE.name(), new CommandMainPage());
        commands.put(AppCommandName.DISPLAY_ALL_WAGON.name(), new CommandDisplayAllWagon(wagonService, wagonStatisticService));
        commands.put(AppCommandName.LOAD_FROM_FILE.name(), new CommandReadTrainDataFromFile(wagonService));
        commands.put(AppCommandName.SORT_WAGONS.name(), new CommandSortWagons(wagonService));
        commands.put(AppCommandName.DELETE_ALL_WAGON.name(), new CommandDeleteAll(wagonService));
        return commands;
    }


    @Test
    public void testMainPageCommand() {
        String expected = "<!DOCTYPE html><html lang=\"en\"><head>    <meta charset=\"UTF-8\">    <title>Title</title></head><body><h1>Train Service</h1><h2>Menu</h2><ul>    <li>        <a href=\"?commandName=DISPLAY_ALL_WAGON\"> View all </a>    </li>    <li>        <p>Read from file</p>        <form action=\"/train\" method=\"POST\">            <input type=\"hidden\" name=\"commandName\" value=\"LOAD_FROM_FILE\">            <input type=\"text\" name=\"path\" placeholder=\"File path: \">            <button> Read </button>        </form>    </li>    <li>        <p></p>        <form action=\"/train\" method=\"POST\">            <input type=\"hidden\" name=\"commandName\" value=\"DELETE_ALL_WAGON\">            <button> Delete all</button>        </form>    </li></ul><p> Hello user </p></body></html>";
        Map<String, String> userData = new HashMap<>();

        Command command = commandFactory.getCommand(userData.get("commandName"));
        String actual = command.execute(userData);

        Assert.assertEquals(expected,actual);
    }

    @Test
    public void testDisplayAllWagonCommand(){
        String expected = "<!DOCTYPE html><html lang=\"en\"><head>    <meta charset=\"UTF-8\">    <title>Title</title></head><body><h1>Train Service</h1><h2>Menu</h2><ul>    <li>        <a href=\"?commandName=DISPLAY_ALL_WAGON\"> View all </a>    </li>    <li>        <p>Read from file</p>        <form action=\"/train\" method=\"POST\">            <input type=\"hidden\" name=\"commandName\" value=\"LOAD_FROM_FILE\">            <input type=\"text\" name=\"path\" placeholder=\"File path: \">            <button> Read </button>        </form>    </li>    <li>        <p></p>        <form action=\"/train\" method=\"POST\">            <input type=\"hidden\" name=\"commandName\" value=\"DELETE_ALL_WAGON\">            <button> Delete all</button>        </form>    </li></ul><p>Number of passengers :64\n" +
                "Number of baggage :35\n" +
                "</p><p>Sort options</p>\n" +
                "<form action=\"/train\" method=\"POST\" >\n" +
                "\t<p><input type=\"checkbox\" name=\"option_id\" value=\"ID_SORT\"> Id<Br>\n" +
                "\t<input type=\"checkbox\" name=\"option_name\" value=\"NAME_SORT\"> Name <Br>\n" +
                "\t<input type=\"checkbox\" name=\"option_passengers\" value=\"PASSENGERS_NUMBER_SORT\"> Passengers <Br>\n" +
                "\t<input type=\"checkbox\" name=\"option_baggage\" value=\"BAGGAGE_NUMBER_SORT\"> Baggage<Br>\n" +
                "\t<input type=\"checkbox\" name=\"type\" value=\"WAGON_TYPE_SORT\"> Type</p>\n" +
                "\t<p>Order</p>\n" +
                "\t<p><input type=\"radio\" id=\"order1\" name=\"order\" value=\"ASC\"><label for=\"order1\"> Ascending</label> \n" +
                "\t<input type=\"radio\" id=\"order2\" name=\"order\" value=\"DSC\"><label for=\"order2\">Descending</label>\n" +
                "\t<input type=\"hidden\" name=\"commandName\" value=\"SORT_WAGONS\">\n" +
                "\t<button> Sort </button>\n" +
                "</form><ul<li>Wagon{id=1, name='Wagon number0', numberOfPassengers=10, numberOfBaggage=5, wagonType=CARGO}</li><li>Wagon{id=2, name='Wagon number1', numberOfPassengers=16, numberOfBaggage=10, wagonType=PASSENGER}</li><li>Wagon{id=3, name='Wagon number2', numberOfPassengers=10, numberOfBaggage=5, wagonType=CARGO}</li><li>Wagon{id=4, name='Wagon number3', numberOfPassengers=18, numberOfBaggage=10, wagonType=PASSENGER}</li><li>Wagon{id=5, name='Wagon number4', numberOfPassengers=10, numberOfBaggage=5, wagonType=CARGO}</li></ul></body></html>";
        Map<String, String> userData = new HashMap<>();
        userData.put("commandName","DISPLAY_ALL_WAGON");

        Command command = commandFactory.getCommand(userData.get("commandName"));
        String actual = command.execute(userData);

        Assert.assertEquals(expected,actual);
    }


    @Test
    public void testSortWagons(){
        List<Wagon> beforeSort = wagonService.findAllWagon();

        Map<String, String> userData = new HashMap<>();
        userData.put("commandName","SORT_WAGONS");
        userData.put("option_passengers","PASSENGERS_NUMBER_SORT");
        userData.put("option_baggage","BAGGAGE_NUMBER_SORT");
        userData.put("order","DSC");
        Command command = commandFactory.getCommand(userData.get("commandName"));
        command.execute(userData);

        Wagon actual = wagonService.findAllWagon().get(0);
        Assert.assertNotEquals(beforeSort.get(0), actual);
    }


    @Test
    public void testDeleteAllWagons(){

        int expectedCapacity = 0;
        Map<String, String> userData = new HashMap<>();
        userData.put("commandName","DELETE_ALL_WAGON");

        Command command = commandFactory.getCommand(userData.get("commandName"));
        command.execute(userData);

        int size = wagonService.findAllWagon().size();
        Assert.assertEquals(expectedCapacity, size);

    }


    @Test
    public void testAddWagonsFromCSVFile(){
        int expectedCapacity = 15;
        Map<String, String> userData = new HashMap<>();
        userData.put("commandName","LOAD_FROM_FILE");
        userData.put("path","src/test/resources/test_train_data.csv");

        Command command = commandFactory.getCommand(userData.get("commandName"));
        command.execute(userData);

        int size = wagonService.findAllWagon().size();
        Assert.assertEquals(expectedCapacity, size);
    }







}
