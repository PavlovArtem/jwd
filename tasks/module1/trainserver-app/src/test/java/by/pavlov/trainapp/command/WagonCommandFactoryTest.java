package by.pavlov.trainapp.command;


import by.pavlov.trainapp.service.WagonService;
import by.pavlov.trainapp.service.WagonServiceImpl;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mock;

import static org.mockito.Mockito.mock;

@RunWith(JUnit4.class)
public class WagonCommandFactoryTest {


    private WagonService wagonService;

    private WagonCommandFactory wagonCommandFactory;

    @Before
    public void initTest() {
        wagonService = mock(WagonServiceImpl.class);
        //wagonCommandFactory = new WagonCommandFactory();
    }

    @After
    public void afterTest() {
        wagonService = null;
        wagonCommandFactory = null;
    }

    @Test
    public void testGetCommand() {
        Command command = wagonCommandFactory.getCommand("DISPLAY_ALL_WAGON");
        Assertions.assertEquals(CommandDisplayAllWagon.class, command.getClass());
    }


}
