package by.pavlov.trainapp.util;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.FileNotFoundException;
import java.io.IOException;

@RunWith(JUnit4.class)
public class FileDataReaderTest {

    @Test
    public void readDataTest() {
        FileDataReader fileReader = new FileDataReader();
        String expected = "id,name,passengerQuantity,baggageAmount,wagonType\n" +
                "1,WBAKP9C52FD686251,27,72,CARGO\n" +
                "2,WBASN4C57AC106516,67,94,CARGO\n" +
                "3,WBAVD13536K371106,16,69,CARGO\n" +
                "4,4USBT53595L047287,21,16,PASSENGER\n" +
                "5,JM1CR2W38A0364684,86,4,PASSENGER\n" +
                "6,3VW1K7AJ3FM935319,98,54,PASSENGER\n" +
                "7,1YVHZ8BH0A5441653,93,65,PASSENGER\n" +
                "8,1GYS3MKJ9FR023384,76,60,PASSENGER\n" +
                "9,2B3CK9CV7AH530967,49,62,CARGO\n" +
                "10,5FRYD3H20EB891400,20,63,CARGO\n";

        String actual = "";
        try {
            actual = fileReader.read("src/test/resources/test_train_data.csv");
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
        } catch (IOException exception) {
            exception.printStackTrace();
        }
        Assert.assertEquals(expected, actual);
    }


}
