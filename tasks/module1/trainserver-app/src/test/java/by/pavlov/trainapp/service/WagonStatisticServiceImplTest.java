package by.pavlov.trainapp.service;

import by.pavlov.trainapp.dao.WagonDAO;
import by.pavlov.trainapp.dao.WagonInMemoryDAO;
import by.pavlov.trainapp.entity.Wagon;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class WagonStatisticServiceImplTest {

    private final WagonDAO wagonDAO = new WagonInMemoryDAO(5);
    private final WagonService wagonService = new WagonServiceImpl(wagonDAO);
    private final WagonStatisticService wagonStatisticService = new WagonStatisticServiceImpl(wagonService);


    @Test
    public void testCalculateNumberOfPassenger(){

        Assertions.assertEquals(64,(int) wagonStatisticService.calculateNumberOfPassengers());

    }

    @Test
    public void testCalculateNumberOfBaggage(){
        Assertions.assertEquals(35,(int) wagonStatisticService.calculateNumberOfBaggage());
    }



}
