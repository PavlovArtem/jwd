package by.pavlov.trainapp.dao;

import by.pavlov.trainapp.entity.Wagon;
import by.pavlov.trainapp.entity.WagonType;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.*;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

@RunWith(JUnit4.class)
public class WagonInMemoryTest {

    private WagonInMemoryDAO wagonInMemoryDAO;

    @Before
    public void initTest() {
        wagonInMemoryDAO = new WagonInMemoryDAO(10);
    }

    @After
    public void afterTest() {
        wagonInMemoryDAO = null;
    }

    @Test
    public void testFindAllWagon() {
        assertEquals(10, wagonInMemoryDAO.findAllWagon().size());
    }

    @Test
    public void testSortAllWagonByCriteria(){
        Wagon expectedWagon = new Wagon(2L,"A1S2",50,25, WagonType.PASSENGER);

        List<Wagon> wagons = Arrays.asList(
                new Wagon(1L,"A1S2",50,20, WagonType.PASSENGER),
                expectedWagon,
                new Wagon(3L,"A1S3",35,10, WagonType.PASSENGER),
                new Wagon(4L,"A1S4",35,20, WagonType.PASSENGER)
        );
        WagonDAO wagonDAO = new WagonInMemoryDAO(0);
        wagonDAO.addAllWagon(wagons);

        Map<String, String> criteria = new HashMap<>();
        criteria.put("option_passengers","PASSENGERS_NUMBER_SORT");
        criteria.put("option_id","BAGGAGE_NUMBER_SORT");
        criteria.put("order","DSC");

        List<Wagon> sorted = wagonDAO.sortAllByCriteria(criteria);

        assertEquals(expectedWagon,sorted.get(0));

    }

}
