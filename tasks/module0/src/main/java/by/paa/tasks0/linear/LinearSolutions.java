package by.paa.tasks0.linear;

import java.math.BigDecimal;

/***
 * 2. Найдите значение функции: с = 3 + а.
 * 3. Найдите значение функции: z = 2 * x + ( y – 2 ) * 5.
 * 4. Найдите значение функции: z = ( (a – 3 ) * b / 2) + c.
 */
public class LinearSolutions {

    /**
     * 1. Даны два действительных числа х и у. Вычислить их сумму, разность, произведение и частное.
     */
    public void taskOne(double x, double y) {
        System.out.println("x = " + x + " y = " + y);
        System.out.println("сумма = " + (x + y));
        System.out.println("разность = " + (x + y));
        System.out.println("произведение = " + (x + y));
        if (x != 0 || y != 0){
            System.out.println("частное = " + (x / y));
        } else {
            System.out.println("невозможно поделить!");
        }
    }

    /***
     * 2. Найдите значение функции: с = 3 + а.
     * @param a
     * @return c
     */
    public void taskTwo(double a) {

        double c = 3 + a;
        System.out.println(c);


    }

    /***
     *  3. Найдите значение функции: z = 2 * x + ( y – 2 ) * 5.
     * @param x
     * @param y
     * @return
     */
    public void taskThree(double x, double y) {


        double z = 2 * x + (y - 2) * 5;
        System.out.println(z);

    }


    /***
     * 4. Найдите значение функции: z = ( (a – 3 ) * b / 2) + c.
     * @return
     */
    public double taskFour(double a, double b, double c) {

        double z = ((a - 3) * b / 2) + c;

        return z;

    }


    /***
     * 5. Составить алгоритм нахождения среднего арифметического двух чисел
     */
    public double taskFive(double a, double b) {

        double result = 0;
        if (a != 0 && b != 0) {
            result = (a + b) / 2;
        }
        return result;
    }


    /***
     * 6. Написать код для решения задачи. В n малых бидонах 80 л молока.
     * Сколько литров молока в m больших бидонах,
     * если в каждом большом бидоне на 12 л. больше, чем в малом?
     * @param n
     * @param m
     * @return количество молока в m бидонах
     */
    public void taskSix(int n, int m) {

        if (n <= 0 && m <= 0) {
            System.out.println("количество молока в m бидонах = 0");
        }

        double nVolume = 80 / n;
        double mVolume = nVolume + 12;

        System.out.println("количество молока в m бидонах = "  + (mVolume * m));

    }

    /***
     * 7. Дан прямоугольник, ширина которого в два раза меньше длины. Найти площадь прямоугольника
     * @param length
     * @return площадь прямоугольника if length != 0
     */
    public double taskSeven(double length) {

        if (length != 0) {
            double width = length / 2;
            double square = length * width;

            return square;
        }
        return 0;
    }

    /***
     * 8. Вычислить значение выражения по формуле (все переменные принимают действительные значения):
     * 𝑏+√𝑏^2+4𝑎𝑐 / 2𝑎 −𝑎^3𝑐+𝑏^−2 = result
     * @return
     */
    public double taskEight(double a, double b, double c) {

        double result = ((b + Math.sqrt(Math.pow(b, 2) + 4 * a * c)) / (2 * a)) - (Math.pow(a, 3) * c + Math.pow(b, -2));
        return result;
    }

    /***
     * 9. Вычислить значение выражения по формуле (все переменные принимают действительные значения): 𝑎𝑐∗𝑏𝑑−𝑎𝑏−𝑐𝑐𝑑
     *
     * @param a
     * @param b
     * @param c
     * @param d
     * @return
     */
    public BigDecimal taskNine(double a, double b, double c, double d) {
        BigDecimal result = BigDecimal.valueOf((a / c) * (b / d) - (((a * b) - c) / (c * d)));
        return result;

    }


    /***
     * 11. Вычислить периметр и площадь прямоугольного треугольника по длинам а и b двух катетов.
     * @return S = result[0], P = result[1]
     */
    public double[] taskEleven(double a, double b) {

        double square = (a * b) / 2;
        double hypotenuse = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
        double perimeter = hypotenuse + a + b;

        double[] results = {square, perimeter};
        return results;
    }

    /***
     * 12. Вычислить расстояние между двумя точками с данными координатами (х1, у1)и (x2, у2).
     * @return
     */
    public double taskTwelve(double x1, double y1, double x2, double y2) {

        double distance = 0;

        if (x1 == x2) {
            distance = Math.abs(y1 - y2);
        } else if (y1 == y2) {
            distance = Math.abs(x1 - x2);
        } else {
            double cathetA = Math.abs(x1 - x2);
            double cathetB = Math.abs(y1 - y2);

            cathetA = Math.pow(cathetA, 2);
            cathetB = Math.pow(cathetB, 2);

            distance = Math.sqrt(cathetA + cathetB);
        }

        return distance;
    }


    /**
     * 13. Заданы координаты трех вершин треугольника (х1 у2),(х2, у2) ),(х3, у3). Найти его периметр и площадь.
     *
     * @return @return S = result[0], P = result[1]
     */
    public double[] taskThirteen(double x1, double y1, double x2, double y2, double x3, double y3) {

        double AB = taskTwelve(x1, y1, x2, y2);
        double BC = taskTwelve(x2, y2, x3, y3);
        double AC = taskTwelve(x3, y3, x1, y1);

        double perimeter = AB + BC + AC;

        double p = (AB + BC + AC) / 2;
        double square = Math.sqrt((p * (p - AB)) * (p - BC) * (p - AC));

        double[] results = {square, perimeter};
        return results;
    }


    /**
     * 14. Вычислить длину окружности и площадь круга одного и того же заданного радиуса R.
     *
     * @return answer[0] = S, answer[1] = P
     */
    public double[] taskFourteen(double r) {
        double square = Math.PI * Math.pow(r, 2);
        double circle = 2 * Math.PI * r;

        double[] answer = {square, circle};

        return answer;
    }

    /**
     * 15. Написать программу, которая выводит на экран первые четыре степени числа π.
     */
    public void taskFifteen() {
        int exponent = 1;
        double pI = Math.PI;

        while (exponent <= 4) {
            System.out.println(Math.pow(pI, exponent));
            ++exponent;
        }
    }

    /**
     * 16. Найти произведение цифр заданного четырехзначного числа.
     */
    public void taskSixteen(int input) {
        int delimeter = 1000;
        double result = 1;

        for (int i = 0; i < 4; i++) {

            result *= input / delimeter;
            input = input % delimeter;
            delimeter = delimeter / 10;
        }
        System.out.println(result);
    }


    /**
     * 17. Даны два числа. Найти среднее арифметическое кубов этих чисел и среднее геометрическое модулей этих чисел.
     */
    public void taskSeventeen(int a, int b) {

        int average = (int) ((Math.pow(a, 3) + Math.pow(b, 3)) / 2);
        double geometric = Math.sqrt(Math.abs(a) * Math.abs(b));

        System.out.println("average = " + average + " geometric = " + geometric);

    }


    /**
     *
     */
}
