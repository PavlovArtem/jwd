package by.paa.tasks0.cycles;

import java.util.Scanner;

public class CyclesSolutions {


    /**
     * 1. Необходимо вывести на экран числа от 1 до 5.
     */
    public void taskOne() {
        for (int i = 1; i < 6; i++) {
            System.out.println(i);
        }
    }


    /**
     * 2. Необходимо вывести на экран числа от 5 до 1.
     */
    public void taskTwo() {
        for (int i = 5; i > 0; i--) {
            System.out.println(i);
        }
    }

    /**
     * 3. Необходимо вывести на экран таблицу умножения на 3:
     */
    public static void taskThree() {
        int number = 3;
        for (int i = 1; i <= 10; i++) {
            System.out.println(number + " x " + i + " = " + (number * i));
        }
    }

    /**
     * 4. С помощью оператора while напишите программу вывода всех четных чисел в диапазоне от 2 до 100
     * включительно.
     */
    public void taskFour() {
        int rangeStart = 2;
        int rangeEnd = 100;

        while (rangeStart <= rangeEnd) {
            if (rangeStart % 2 == 0) {
                System.out.print(rangeStart + " ");
            }
            ++rangeStart;
        }
    }

    /**
     * 5. С помощью оператора while напишите программу определения суммы всех нечетных чисел в
     * диапазоне от 1 до 99 включительно.
     *
     * @return сумма нечетных чисел
     */
    public double taskFive() {

        int rangeStart = 1;
        int rangeEnd = 99;
        int result = 0;

        while (rangeStart <= rangeEnd) {
            if (rangeStart % 2 != 0) {
                result += rangeStart;
            }
            ++rangeStart;
        }

        return result;
    }

    /**
     * 6. Напишите программу, где пользователь вводит любое целое положительное число. А программа
     * суммирует все числа от 1 до введенного пользователем числа.
     */
    public void taskSix() {
        System.out.print("Enter positive number: ");
        Scanner scanner = new Scanner(System.in);
        int result = 0;

        int inputNumber = 0;

        try {
            inputNumber = scanner.nextInt();
        } catch (Exception ex) {
            System.out.println("Wrong Input!");
        }

        if (inputNumber > 0) {

            for (int i = 1; i < inputNumber; i++) {
                result += i;
            }
        }

        System.out.println(result);
    }


    /*
7. Вычислить значения функции на отрезке [а,b] c шагом h:

    y =  x,x >2
       -x,x <= 2
 */
    public void taskSeven(int a, int b, int h) {
        while (a <= b) {
            int y = 0;
            if (a > 2) {
                y = a;
            } else if (a > 0) {
                y = a * -1;
            } else {
                y = a;
            }
            System.out.println("x = " + a + " y = " + y);
            a += h;
        }
    }

    /**
     *8. Вычислить значения функции на отрезке [а,b] c шагом h:
     */
    public void taskEight(int a, int b, int h, int c) {
        while (a <= b) {
            int y = 0;
            if (a == 15) {
                y = (a + c) * 2;
            } else {
                y = (a - c) + 6;
            }
            System.out.println("x = " + a + " y = " + y);
            a += h;
        }
    }

    /**
     * 9. Найти сумму квадратов первых ста чисел.
     */
    public void taskNine() {
        int maxNumber = 100;
        long result = 0L;

        for (int i = 1; i <= maxNumber; i++) {
            result += Math.pow(i, 2);
        }

        System.out.println(result);
    }

    /**
     * 10. Составить программу нахождения произведения квадратов первых двухсот чисел
     */
    public void taskTen() {
        int number = 200;
        long result = 1L;

        for (int i = 1; i <= number; i++) {
            result *= Math.pow(i, 2);
        }

        System.out.println(result);
    }


    /**
     * 12. Последовательность аn строится так: а1 = 1, an =6+ аn-1 , для каждого n >1 Составьте программу
     * нахождения произведения первых 10 членов этой последовательности.
     */
    public void taskTwelve() {
        int aN = 1;
        //an = 6 + an - 1
        int result = aN;

        int[] sequence = new int[10];
        sequence[0] = aN;
        long seqResult = sequence[0];

        for (int i = 1; i < 10; i++) {
            sequence[i] = 6 + sequence[i - 1];
            seqResult *= sequence[i];
        }

        System.out.println(result + " arr " + seqResult);

    }

    /**
     * 13. Составить таблицу значений функции y = 5 - x
     * 2
     * /2 на отрезке [-5; 5] с шагом 0.5.
     */
    public void taskThirteen() {
        double rangeStart = -5d;
        double rangeEnd = 5d;
        double step = 0.5;

        System.out.println(" x     y ");
        while (rangeStart <= rangeEnd) {
            double y = 5 - Math.pow(rangeStart, 2) / 2;
            System.out.println(rangeStart + "  " + y);
            rangeStart += step;

        }

    }

    /**
     * 14. Дано натуральное n. вычислить: 1 + 1/2 + 1/3 + 1/4 + ... + 1/n.
     */
    public void taskFourteen(int n) {
        double result = 1;
        for (int i = 2; i <= n; i++) {
            double b = 1.0 / i;
            result += b;
        }

        System.out.println("result: " + result);
    }

    /**
     * 15. Вычислить : 1+2+4+8+...+ 2 в 10 степени
     */
    public void taskFifteen() {

        int result = 1;

        for (int i = 1; i <= 10; i++) {
            result += Math.pow(2, i);
        }

        System.out.println("result: " + result);

    }

    /**
     * 16. Вычислить: (1+2)*(1+2+3)*...*(1+2+...+10).
     */
    public void taskSixteen() {

        int lastSum = 1;
        long result = 1;

        for (int i = 2; i <= 10; i++) {
            lastSum += i;
            result *= lastSum;

        }

        System.out.println(result);
    }


    /**
     * 18. Даны числовой ряд и некоторое число е. Найти сумму тех членов ряда, модуль которых больше или
     * равен заданному е. Общий член ряда имеет вид: an = (-1)^n-1/n
     */

    public void taskEighteen() {
        int e = 2;
        int n = 1;
        double a;
        double result = 0;

        while (Math.abs(result) <= e) {
            a = Math.pow(-1, n - 1) / n;
            result += a;
        }

        System.out.println(result);
    }

    /**
     * 21. Составить программу для вычисления значений функции F(x) на отрезке [а, b] с шагом h. Результат
     * представить в виде таблицы, первый столбец которой – значения аргумента, второй - соответствующие
     * значения функции:
     */
    public void taskTwentyOne(int a, int b, int h) {
        int arg = 0;
        double result;
        System.out.println("arg       result");

        while (a <= b) {
            double argSin =Math.sin(arg);
            arg = a;
            result = arg - argSin;
            System.out.println(arg + "       " + result);
            a += h;
        }
    }


    /**
     * 22. Составить программу для вычисления значений функции F(x) на отрезке [а, b] с шагом h. Результат
     * представить в виде таблицы, первый столбец которой – значения аргумента, второй - соответствующие
     * значения функции:
     * F(x) = sin^2(x);
     */
    public void taskTwentyTwo(int a, int b, int h) {
        int arg = 0;
        double result;
        System.out.println("arg       result");

        while (a <= b) {
            double argSin = Math.toRadians(arg);
            arg = a;
            result = arg - Math.sin( argSin);
            System.out.println(arg + "       " + result);
            a += h;
        }

    }


}
