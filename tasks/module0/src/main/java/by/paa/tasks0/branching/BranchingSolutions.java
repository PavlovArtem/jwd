package by.paa.tasks0.branching;

import java.util.Scanner;


public class BranchingSolutions {


    /**
     * 1. Составить программу сравнения двух чисел 1 и 2. Если 1 меньше 2 – вывести на экран цифру 7, в противном случае – цифру 8.
     */
    public void taskOne(int a, int b) {
        if (a < b) {
            System.out.println(7);
        } else {
            System.out.println(8);
        }

    }


    /**
     * 2. Составить программу сравнения двух чисел 1 и 2. Если 1 меньше 2 – вывести на экран слово «yes», в противном случае – слово «no»
     */
    public void taskTwo(int a, int b) {
        if (a < b) {
            System.out.println("yes");
        } else {
            System.out.println("no");
        }
    }

    /**
     * 3. Составить программу сравнения введенного числа а и цифры 3. Вывести на экран слово «yes», если число а меньше 3; если больше, то вывести слово «no».
     */
    public void taskThree() {

        int equalToNumber = 3;
        Scanner scanner = new Scanner(System.in);


        try {
            System.out.println("Type number: ");
            double inputNumber = scanner.nextDouble();
            if (inputNumber >= equalToNumber) {
                System.out.println("yes");
            } else {
                System.out.println("no");
            }
        } catch (Exception ex) {
            System.out.println("Wrong input");
        }


    }


    /**
     * 4. Составить программу: равны ли два числа а и b?
     *
     * @return true if equals
     */
    public boolean taskFour(double a, double b) {
        return a == b;

    }

    /***
     * 4. Составить программу: равны ли два числа а и b?
     * 5. Составить программу: определения наименьшего из двух чисел а и b.
     * 6. Составить программу: определения наибольшего из двух чисел а и b.
     * @param a
     * @param b
     * @return 1 if a > b, 0 if a == b, -1 if a < b
     */
    public int twoNumberEquals(double a, double b) {

        if (a > b) {
            return 1;
        } else if (a == b) {
            return 0;
        } else {
            return -1;
        }
    }

    /***
     *7. Составить программу нахождения модуля выражения a*x*x + b*x + c при заданных значениях a, b, c и х
     */
    public double taskSeven(double a, double b, double c, double x) {

        double result = Math.abs(a * x * x + b * x + c);
        return result;
    }

    /***
     *8. Составить программу нахождения наименьшего из квадратов двух чисел а и b.
     * @param a
     * @param b
     * @return 1 if a > b, 0 if a == b, -1 if a < b
     */
    public int taskEight(double a, double b) {
        a = a * a;
        b = b * b;
        if (a > b) {
            return 1;
        } else if (a < b) {
            return -1;
        } else {
            return 0;
        }
    }

    /***
     * 9. Составить программу, которая определит по трем введенным сторонам, является ли данный треугольник равносторонним.
     * @return if triangle is equilateral returns true, else false.
     */
    public boolean taskNine(double a, double b, double c) {

        if (a == b && b == c) {
            return true;
        } else {
            return false;
        }

    }


    /**
     * 10. Составить программу, которая определит площадь какого круга меньше.
     */
    public boolean taskTen(double circleSquare1, double circleSquare2) {

        return circleSquare1 < circleSquare2;

    }


    /**
     * 12. Даны три действительных числа.
     * Возвести в квадрат те из них, значения которых неотрицательны,
     * и в четвертую степень — отрицательные.
     */
    public void taskTwelve(double a, double b, double c) {

        double[] results = {a, b, c};

        for (double number : results) {

            if (number > 0) {
                number = Math.pow(number, 2);
            } else if (number < 0) {
                number = Math.pow(number, 4);
            }
            System.out.println(number);

        }
    }

    /**
     * 13. Даны две точки А(х1, у1) и В(х2, у2).
     * Составить алгоритм, определяющий, которая из точек находится ближе к началу координат.
     */
    public void taskThirteen(int x1, int y1, int x2, int y2) {

        int[] inputX = {x1, x2};
        int[] inputY = {y1, y2};
        double[] distance = new double[2];
        double startPoint = 0;

        for (int i = 0; i < 2; i++) {
            if (x1 == startPoint) {
                distance[i] = Math.abs(inputY[i] - startPoint);
            } else if (y1 == startPoint) {
                distance[i] = Math.abs(inputX[i] - startPoint);
            } else {
                double cathetA = Math.abs(inputX[i] - startPoint);
                double cathetB = Math.abs(inputY[i] - startPoint);

                cathetA = Math.pow(cathetA, 2);
                cathetB = Math.pow(cathetB, 2);

                distance[i] = Math.sqrt(cathetA + cathetB);
            }
        }

        if (distance[0] < distance[1]) {
            System.out.println("Точка А ближе к началу");
        } else if (distance[0] > distance[1]) {
            System.out.println("Точка B ближе к началу");
        } else {
            System.out.println("Точки равны по отношению к началу");
        }
    }

    /**
     * 14. Даны два угла треугольника (в градусах).
     * Определить, существует ли такой треугольник, и если да, то будет ли он прямоугольным.
     */
    public void taskFourteen(int angleA, int angleB) {
        int angleC;
        if ((angleA + angleB) < 180) {
            angleC = 180 - (angleA + angleB);
            if (angleA == 90 || angleB == 90 || angleC == 90) {
                System.out.println("Это прямоугольный треугольник");
            } else {
                System.out.println("Это не прямоугольный треугольник");
            }
        } else if ((angleA + angleB) >= 180) {
            System.out.println("Такого треугольника не существует");
        }

    }


    /**
     * 15. Даны действительные числа х и у, не равные друг другу.
     * Меньшее из этих двух чисел заменить половиной их суммы,
     * а большее — их удвоенным произведением.
     */
    public void taskFifteen(double x, double y) {
        double lowerNumber = Math.min(x, y);
        double higherNumber = Math.max(x, y);

        double lowerResult = (lowerNumber + higherNumber) / 2;
        double higherResult = (higherNumber * lowerNumber) * 2;

        System.out.println("измененное наименьшее = " + lowerResult + " измененное наибольшее = " + higherResult);
    }


    /**
     * 16. На плоскости ХОY задана своими координатами точка А.
     * Указать, где она расположена (на какой оси или в каком координатном угле).
     */
    public void taskSixteen(int xA, int yA) {
        if (xA > 0 && yA > 0) {
            System.out.println("Точка находится в 1ом координатном угле");
        } else if (xA < 0 && yA > 0) {
            System.out.println("Точка находится во 2ом координатном угле");
        } else if (xA < 0 && yA < 0) {
            System.out.println("Точка находится во 3ом координатном угле");
        } else if (xA > 0 && yA < 0) {
            System.out.println("Точка находится во 4ом координатном угле");
        } else {
            System.out.println("Точка находится на пересечении");
        }
    }


    /**
     * 17. Даны целые числа т, п. Если числа не равны,
     * то заменить каждое из них одним и тем же числом, равным большему из исходных, а если равны, то заменить числа нулями.
     */
    public void taskSeventeen(int m, int n) {
        if (m == n) {
            m = 0;
            n = 0;
        } else {
            m = Math.max(m, n);
            n = m;
        }

        System.out.println("m = " + m + " n = " + n);
    }

    /**
     * 18. Подсчитать количество отрицательных среди чисел а, b, с.
     */
    public void taskEighteen(int... nums) {
        int negativeNums = 0;
        for (int n : nums) {
            if (n < 0) {
                ++negativeNums;
            }
        }
        System.out.println("Кол-во отрицательных чисел = " + negativeNums);

    }


    /**
     * 19. Подсчитать количество положительных среди чисел а, b, с.
     */
    public void taskNineteen(int... nums) {
        int positiveNums = 0;
        for (int n : nums) {
            if (n > 0) {
                ++positiveNums;
            }
        }
        System.out.println("Кол-во отрицательных чисел = " + positiveNums);
    }

    /**
     * 20. Определить, делителем каких чисел а, b, с является число k.
     */
    public void taskTwenty(int a, int b, int c, int k) {
        if (a % k == 0) {
            System.out.println("Число " + k + "является делителем числа " + a);
        }
        if (b % k == 0) {
            System.out.println("Число " + k + "является делителем числа " + b);
        }
        if (c % k == 0) {
            System.out.println("Число " + k + "является делителем числа " + c);
        }
    }


    /**
     * 21. Программа — льстец. На экране высвечивается вопрос «Кто ты: мальчик или девочка?
     * Введи Д или М». В зависимости от ответа на экране должен появиться текст
     * «Мне нравятся девочки!» или «Мне нравятся мальчики!».
     */
    public void taskTwentyOne() {
        final String loveGirls = "Мне нравятся девочки!";
        final String loveBoys = "Мне нравятся мальчики!";
        System.out.println("Кто ты: мальчик или девочка? Введи Д или М");
        Scanner scanner = new Scanner(System.in);
        final String input = scanner.next();

        switch (input) {
            case "Д":
                System.out.println(loveGirls);
                break;
            case "М":
                System.out.println(loveBoys);
                break;
            default:
                System.out.println("Нуу я даже не знаю");
                break;
        }
    }
}
