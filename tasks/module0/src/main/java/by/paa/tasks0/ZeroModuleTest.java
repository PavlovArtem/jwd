package by.paa.tasks0;


import by.paa.tasks0.branching.BranchingSolutions;
import by.paa.tasks0.cycles.CyclesSolutions;
import by.paa.tasks0.linear.LinearSolutions;

public class ZeroModuleTest {
    public static void main(String[] args) {


        BranchingSolutions branchingSolutions = new BranchingSolutions();

        branchingSolutions.taskOne(1, 2);
        System.out.println("===================================");
        branchingSolutions.taskTwo(1, 2);
        System.out.println("===================================");
        branchingSolutions.taskThree();
        System.out.println("===================================");
        System.out.println(branchingSolutions.taskFour(4, 4));
        System.out.println("===================================");
        System.out.println(branchingSolutions.taskSeven(-2, -3, 1, 2));
        System.out.println("===================================");
        System.out.println(branchingSolutions.taskEight(-3, 2));
        System.out.println("===================================");
        System.out.println(branchingSolutions.taskNine(3, 3, 2));
        System.out.println("===================================");
        System.out.println(branchingSolutions.taskTen(25, 25));
        System.out.println("===================================");
        branchingSolutions.taskThirteen(1, 2, 1, 3);
        System.out.println("===================================");
        branchingSolutions.taskFourteen(30, 60);
        System.out.println("===================================");
        branchingSolutions.taskFifteen(1, 2);
        System.out.println("===================================");
        branchingSolutions.taskSixteen(1, 2);
        System.out.println("===================================");
        branchingSolutions.taskSeventeen(1, 2);
        System.out.println("===================================");
        branchingSolutions.taskEighteen(1, -2, -3);
        System.out.println("===================================");
        branchingSolutions.taskNineteen(-1, -2, 3);
        System.out.println("===================================");
        branchingSolutions.taskTwenty(3, 2, 9, 3);
        System.out.println("===================================");
        branchingSolutions.taskTwentyOne();
        System.out.println("===================================");


        CyclesSolutions cyclesSolutions = new CyclesSolutions();

        cyclesSolutions.taskOne();
        System.out.println("================================");
        cyclesSolutions.taskTwo();
        System.out.println("================================");
        cyclesSolutions.taskFour();
        System.out.println("================================");
        System.out.println(cyclesSolutions.taskFive());
        System.out.println("================================");
        cyclesSolutions.taskSeven(-2, 4, 1);
        System.out.println("================================");
        cyclesSolutions.taskEight(10,16,1,2);
        System.out.println("================================");
        cyclesSolutions.taskNine();
        System.out.println("================================");
        cyclesSolutions.taskTen();
        System.out.println("================================");
        cyclesSolutions.taskTwelve();
        System.out.println("================================");
        cyclesSolutions.taskThirteen();
        System.out.println("================================");
        cyclesSolutions.taskFourteen(5);
        System.out.println("================================");
        cyclesSolutions.taskFifteen();
        System.out.println("================================");
        cyclesSolutions.taskSixteen();
        System.out.println("================================");
        cyclesSolutions.taskEighteen();
        System.out.println("================================");
        cyclesSolutions.taskTwentyOne(1, 30, 1);
        System.out.println("================================");


        LinearSolutions linearSolutions = new LinearSolutions();


        linearSolutions.taskOne(1,2);
        System.out.println("====================================================");
        linearSolutions.taskTwo(2);
        System.out.println("====================================================");
        linearSolutions.taskThree(4,2);
        System.out.println("====================================================");
        linearSolutions.taskFive(5,10);
        System.out.println("====================================================");
        linearSolutions.taskSix(5,7);
        System.out.println("====================================================");
        linearSolutions.taskSeven(15);
        System.out.println("====================================================");
        System.out.println(linearSolutions.taskEight(2, 4, 6));
        System.out.println("====================================================");
        System.out.println(linearSolutions.taskNine(2, 3, 4, 5));
        System.out.println("====================================================");
        System.out.println("====================================================");
        double[] resultTaskEleven = linearSolutions.taskEleven(5, 2);
        System.out.println("S = " + resultTaskEleven[0] + " P = " + resultTaskEleven[1]);
        System.out.println("====================================================");
        System.out.println("distance = " + linearSolutions.taskTwelve(1, 2, 4, 2));
        System.out.println("distance = " + linearSolutions.taskTwelve(1, 2, 4, 4));
        System.out.println("====================================================");
        double[] resultTaskTh = linearSolutions.taskThirteen(1, 1, 2, 2, 4, 2);
        System.out.println("S = " + resultTaskTh[0] + " P = " + resultTaskTh[1]);
        System.out.println("====================================================");
        System.out.println("====================================================");
        double[] resultTaskFourteen = linearSolutions.taskFourteen(3);
        System.out.println("S = " + resultTaskFourteen[0] + " P = " + resultTaskFourteen[1]);
        System.out.println("====================================================");
        linearSolutions.taskFifteen();
        System.out.println("====================================================");
        linearSolutions.taskSixteen(5432);
        System.out.println("====================================================");
        linearSolutions.taskSeventeen(32, 64);
        System.out.println("====================================================");


        System.out.println("Спасибо за внимание и удачного дня!)");


    }
}
