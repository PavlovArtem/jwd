package by.paa.tasks3.firstHalf;


/**
 * 4. Написать метод(методы) для нахождения наименьшего
 * общего кратного трех натуральных чисел.
 */
public class TaskFour {

    private int gcd;


    public void showGcdAndLcm(int a, int b, int c) {
        if (a <= 0 || b<= 0|| c<=0){
            System.out.println("Incorrect input!");
        }

        String answer = String.format("НОД для чисел %d, %d, %d = %d НОК = %d", a, b, c, gcdThreeNums(a, b, c), lcmForThreeNums(a, b, c));
        System.out.println(answer);
    }

    private int lcmForThreeNums(int a, int b, int c) {
        return lcm(a,lcm(b,c));
    }

    private int lcm(int a, int b) {
        return a*b/this.gcd;
    }

    private int gcdThreeNums(int a, int b, int c) {
        int[] nums = {a, b, c};

        int gcd = nums[0];
        for (int i = 0; i < nums.length; i++) {
            gcd = getGcd(gcd, nums[i]);
        }

        this.gcd = gcd;
        return gcd;
    }


    private int getGcd(int a, int b) {
        while (b != 0) {
            int tmp = a % b;
            a = b;
            b = tmp;
        }
        return a;
    }

}
