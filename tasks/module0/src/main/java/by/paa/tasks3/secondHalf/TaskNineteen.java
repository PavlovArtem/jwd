package by.paa.tasks3.secondHalf;


/**
 * 19. Написать программу, определяющую сумму n - значных чисел, содержащих только нечетные цифры. Определить
 * также, сколько четных цифр в найденной сумме. Для решения задачи использовать декомпозицию.
 */
public class TaskNineteen {

    public String showResults(int n) {
        int sumOfOdd = sumOfOddNSignificantNumbers(n);
        int evenDigitsCounter = countOfEvenDigitsInResult(sumOfOdd);
        String answer = String.format("Сумма n - значных чисел, содержащих только нечетные цифры = %d\n" +
                "Количество четных цифр в найденной сумме = %d", sumOfOdd, evenDigitsCounter);
        return answer;
    }

    public int countOfEvenDigitsInResult(int n) {
        int evenNumsCounter = 0;
        int[] resultDigits = createArrayFromNumber(n);
        for (int i = 0; i < resultDigits.length; i++) {
            if (resultDigits[i] % 2 == 0) {
                ++evenNumsCounter;
            }
        }
        return evenNumsCounter;
    }


    private int sumOfOddNSignificantNumbers(int n) {
        int result = 0;
        int rangeStart = 1;
        int rangeEnd = 1;
        for (int i = 0; i <= n; i++) {
            if (i < n) {
                rangeStart *= 10;
            } else {
                rangeEnd = rangeStart * 10;
            }
        }

        for (int i = rangeStart; i < rangeEnd; i++) {
            result += sumOfOddDigits(i);
        }

        return result;
    }


    private int sumOfOddDigits(int n) {
        int[] digits = createArrayFromNumber(n);
        int result = 0;
        for (int i = 0; i < digits.length; i++) {
            if (isOddDigit(digits[i])) {
                result += digits[i];
            } else {
                return 0;
            }
        }
        return result;
    }

    private int[] createArrayFromNumber(int n) {
        String string = String.valueOf(Math.abs(n));
        int[] result = new int[string.length()];

        for (int i = 0; i < string.length(); i++) {
            int toAdd = Character.getNumericValue(string.charAt(i));
            result[i] = toAdd;
        }
        return result;
    }


    private boolean isOddDigit(final int digit) {
        return digit % 2 != 0;
    }

}
