package by.paa.tasks3.secondHalf;


import java.util.LinkedList;
import java.util.List;

/**
 * 15. Даны натуральные числа К и N. Написать метод(методы) формирования массива А, элементами которого
 * являются числа, сумма цифр которых равна К и которые не большее N.
 */
public class TaskFifteen {

    public List<Integer> sequenceOfDigitsEqualsK(int K, int N) {
        List<Integer> resultList = new LinkedList<>();

        for (int i = K; i <= N; i++) {
            if (isEqualsK(i, K)) {
                resultList.add(i);
            }
        }

        return resultList;

    }

    private boolean isEqualsK(int n, int K) {
        return sumOfDigits(n) == K;
    }


    private int sumOfDigits(int n) {
        int result = 0;
        for (int num : createArrayFromNumber(n)) {
            result += num;
        }
        return result;
    }

    private int[] createArrayFromNumber(int n) {
        String string = String.valueOf(Math.abs(n));
        int[] result = new int[string.length()];

        for (int i = 0; i < string.length(); i++) {
            result[i] = Character.getNumericValue(string.charAt(i));
        }

        return result;
    }

}
