package by.paa.tasks3.secondHalf;


import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * 16. Два простых числа называются «близнецами», если они отличаются друг от друга на 2 (например, 41 и 43). Найти
 * и напечатать все пары «близнецов» из отрезка [n,2n], где n - заданное натуральное число больше 2. Для решения
 * задачи использовать декомпозицию.
 */
public class TaskSixteen {

    public List<Integer> getResultList(int n) {
        List<Integer> resultList = new LinkedList<>();
        List<Integer> range = setRange(n);
        int previousNum = range.get(0);
        int nextNum = range.get(1);

        for (int i = 1; i < range.size(); i++) {
            if (previousNum + 2 == nextNum) {
                resultList.add(previousNum);
                resultList.add(nextNum);
            }

            previousNum = range.get(i - 1);
            nextNum = range.get(i);
        }
        return resultList;
    }


    private boolean isSimpleNumber(int n) {
        if (n < 2) {
            return false;
        }
        int s = (int) Math.sqrt(n);
        for (int i = 2; i <= s; i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }

    private List<Integer> setRange(int n) {
        if (n < 2) {
            return new LinkedList<>();
        }

        List<Integer> resultList = new LinkedList<>();
        int rangeEnd = n * 2;
        int index = 0;
        while (n != rangeEnd) {
            if (isSimpleNumber(n)) {
                resultList.add(n);
                index++;
                n++;
            } else {
                n++;
            }
        }
        return resultList;
    }


}
