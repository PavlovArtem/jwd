package by.paa.tasks3.firstHalf;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 7. На плоскости заданы своими координатами n точек. Написать метод(методы), определяющие, между какими из
 * пар точек самое большое расстояние. Указание. Координаты точек занести в массив.
 */
public class TaskSeven {

    public int[] graitestDistanceBetweenPoints(int... points) {

        double maxDistance = 0;
        int firstPoint = 0;
        int secondPoint = 0;

        Point[] pointsArray = getPointsFromArray(points);
        if (pointsArray.length == 0) {
            return new int[0];
        }

        for (int i = 1; i < pointsArray.length; i++) {


            for (int j = i; j < pointsArray.length; j++) {
                double tmp = distanceBetweenTwoPoints(pointsArray[i - 1].X, pointsArray[i - 1].Y,
                        pointsArray[j].X, pointsArray[j].Y);

                if (tmp > maxDistance) {
                    maxDistance = tmp;
                    firstPoint = i - 1;
                    secondPoint = j;
                }
            }
        }

        int[] result = {pointsArray[firstPoint].X, pointsArray[firstPoint].Y, pointsArray[secondPoint].X, pointsArray[secondPoint].Y};
        return result;
    }

    /**
     * @param coordinates
     * @return empty array if coordinates length is odd
     */
    public Point[] getPointsFromArray(int... coordinates) {
        if (coordinates.length % 2 != 0) {
            return new Point[0];
        }

        Point[] points = new Point[coordinates.length / 2];

        for (int i = 1, j = 0; i < coordinates.length; i += 2) {
            points[j] = new Point(coordinates[i - 1], coordinates[i]);
            j++;

        }

        return points;
    }


    public double distanceBetweenTwoPoints(double x1, double y1, double x2, double y2) {
        double distance = 0;

        if (x1 == x2) {
            distance = Math.abs(y1 - y2);
        } else if (y1 == y2) {
            distance = Math.abs(x1 - x2);
        } else {
            double cathetA = Math.abs(x1 - x2);
            double cathetB = Math.abs(y1 - y2);

            cathetA = Math.pow(cathetA, 2);
            cathetB = Math.pow(cathetB, 2);

            distance = Math.sqrt(cathetA + cathetB);
        }

        return distance;
    }


    private class Point {
        int X;
        int Y;

        Point(int X, int Y) {
            this.X = X;
            this.Y = Y;
        }

    }


}
