package by.paa.tasks3.secondHalf;

/**
 * 11. Задан массив D. Определить следующие суммы: D[l] + D[2] + D[3]; D[3] + D[4] + D[5]; D[4] +D[5] +D[6].
 * Пояснение. Составить метод(методы) для вычисления суммы трех последовательно расположенных элементов
 * массива с номерами от k до m.
 */
public class TaskEleven {

    public void getResult(int[]array,int k, int m){
        for (int num: calcSequences(array,k,m)){
            System.out.print(num + " ");
        }
    }

    private int[] calcSequences(int[] array, int k, int m) {
        if (k < 0 || m > array.length) {
            return new int[1];
        }

        int[] result = new int[array.length / 3];
        int counter = 0;
        int addToResult = 0;

        for (int i = k, j = 0; i < m; i++) {
            if (counter < 3) {
                addToResult += array[i];
                counter++;
            } else {
                result[j] = addToResult;
                ++j;
                i -= 2;
                counter = 0;
                addToResult = 0;
            }
        }
        return result;
    }


}
