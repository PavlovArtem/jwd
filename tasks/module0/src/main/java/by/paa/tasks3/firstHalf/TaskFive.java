package by.paa.tasks3.firstHalf;


/**
 * 5. Написать метод(методы) для нахождения
 * суммы большего и меньшего из трех чисел.
 */
public class TaskFive {

    public void calculateSum(int a, int b, int c){
        System.out.println(String.format("сумма наибольшего и наименьшего = %d",sumOfGreaterAndLess(a,b,c) ));
    }

    private int sumOfGreaterAndLess(int... nums){

        int max = nums[0];
        int min = nums[0];

        for (int num: nums){
            if (max < num){
                max = num;
            }
            if (min > num){
                min = num;
            }
        }

        return max + min;
    }

}
