package by.paa.tasks3.firstHalf;


/**
 * 9. Написать метод(методы), проверяющий, являются ли данные три числа взаимно простыми.
 */
public class TaskNine {

    public void showIsMutuallySimpleNumber(int a, int b) {
        if (isMutuallySimpleNumbers(a, b)) {
            System.out.println(String.format("%d и %d числа. взаимно простые", a, b));
        } else {
            System.out.println(String.format("%d и %d числа. Не взаимно простые!", a, b));
        }
    }

    public boolean isMutuallySimpleNumbers(int a, int b) {
        return gcd(a, b) == 1;
    }

    private int gcd(int a, int b) {
        while (b != 0) {
            int tmp = a % b;
            a = b;
            b = tmp;
        }
        return a;
    }

}
