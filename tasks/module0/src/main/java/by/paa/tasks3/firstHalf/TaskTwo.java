package by.paa.tasks3.firstHalf;


/**
 * 2. Написать метод(методы) для нахождения наибольшего общего делителя и наименьшего общего кратного двух
 * натуральных чисел:
 */

public class TaskTwo {

    /**
     *
     * @param a
     * @param b
     * @return if a or b <= 0 return Incorrect values!
     */
    public String resultLcmGcd(int a, int b) {
        String result;
        if (isNotValid(a, b)) {
            result = "Incorrect values!";
        } else {
            result = String.format("НОД = %d, НОК = %d", gcd(a, b), lcm(a, b));
        }
        return result;

    }


    private int gcd(int a, int b) {
        while (b != 0) {
            int tmp = a % b;
            a = b;
            b = tmp;
        }
        return a;
    }


    private int lcm(int a, int b) {
        return a * b / gcd(a, b);
    }

    private boolean isNotValid(int a, int b) {

        return a <= 0 || b <= 0;
    }
}
