package by.paa.tasks3.secondHalf;


/**
 * 14. Написать метод(методы),
 * определяющий, в каком из данных двух чисел больше цифр.
 */
public class TaskFourteen {

    /**
     *
     * @param a
     * @param b
     * @return the number with the most digits
     */
    public int largestNumber(int a, int b) {
        String numA = String.valueOf(Math.abs(a));
        String numB = String.valueOf(Math.abs(b));
        return numA.length() > numB.length() ? a : b;

    }

}
