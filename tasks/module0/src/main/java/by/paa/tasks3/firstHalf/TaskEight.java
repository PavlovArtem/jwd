package by.paa.tasks3.firstHalf;


/**
 * 8. Составить программу, которая в массиве A[N] находит второе по величине число (вывести на печать число,
 * которое меньше максимального элемента массива, но больше всех других элементов).
 */
public class TaskEight {

    public void preMaxElement(int[] array) {

        int max = array[0];
        int preMax = max;

        for (int i = 0; i < array.length; i++){
            if (max < array[i]){
                preMax = max;
                max = array[i];
            } else if (preMax < array[i] && array[i] < max){
                preMax = array[i];
            } else if (preMax >= max && array[i] < max){
                preMax = array[i];
            }
        }

        System.out.println("Второй по величине элемент = " + preMax);
    }

}
