package by.paa.tasks3.firstHalf;

/**
 * 1. Треугольник задан координатами своих вершин.
 * Написать метод для вычисления его площади.
 */
public class TaskOne {
    private Point vertexA;
    private Point vertexB;
    private Point vertexC;

    public TaskOne(){
        this.vertexA = new Point();
        this.vertexB = new Point();
        this.vertexC = new Point();
    }

    public double getTriangleSquare(int xA, int yA, int xB, int yB, int xC, int yC) {
        setTriangleVertex(xA,yA,xB,yB,xC,yC);
        return calculateTriangleSquare();
    }


    private void setTriangleVertex(int xA, int yA, int xB, int yB, int xC, int yC) {
        vertexA.x = xA;
        vertexA.y = yA;
        vertexB.x = xB;
        vertexB.y = yB;
        vertexC.x = xC;
        vertexC.y = yC;
    }


    private double calculateTriangleSquare() {
        double[] params = {vertexA.x - vertexC.x, vertexB.x - vertexC.x, vertexA.y - vertexC.y, vertexB.y - vertexC.y};

        double step1 = params[0] * params[3];
        double step2 = params[2] * params[1];
        double diff = Math.abs(step1 - step2);

        return 0.5 * diff;
    }


    private static class Point {
        int x;
        int y;
    }
}
