package by.paa.tasks3.secondHalf;


import java.util.Arrays;

/**
 * 13. Дано натуральное число N.
 * Написать метод(методы) для формирования массива, элементами которого являются
 * цифры числа N.
 */
public class TaskThirteen {

    public int[] createArrayFromNumber(int n) {
        String string = String.valueOf(Math.abs(n));
        char[] digits = string.toCharArray();
        int[] result = new int[digits.length];

        for (int i = 0; i < digits.length;i++){
            result[i] = Character.getNumericValue(string.charAt(i));
        }

        return result;
    }


}
