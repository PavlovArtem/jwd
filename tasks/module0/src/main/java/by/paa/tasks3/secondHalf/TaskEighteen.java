package by.paa.tasks3.secondHalf;

import java.util.LinkedList;
import java.util.List;

/**
 * 18. Найти все натуральные n-значные числа, цифры в которых образуют строго возрастающую последовательность
 * (например, 1234, 5789). Для решения задачи использовать декомпозицию.
 */
public class TaskEighteen {

    public void showNumbersWithIncreaseDigits(List<Integer> input){
        System.out.println("Числа с возрастающей последовательностью цифр:");
        for (Integer num:input){
            System.out.print(num + " ");
        }
    }

    public List<Integer> numbersWithIncreasingDigits(int[] sequence){
        List<Integer> resultList = new LinkedList<>();

        for (int num: sequence){
            int[] digits = createArrayFromNumber(num);
            if (isIncreasingSequence(digits)){
                resultList.add(num);
            }
        }
        return resultList;
    }

    public int[] createArrayFromNumber(int n) {
        String string = String.valueOf(Math.abs(n));
        char[] digits = string.toCharArray();
        int[] result = new int[digits.length];

        for (int i = 0; i < digits.length;i++){
            result[i] = Character.getNumericValue(string.charAt(i));
        }

        return result;
    }

    private boolean isIncreasingSequence(int[] digits){
        int previous = digits[0];

        for (int i = 1; i < digits.length; i++){
            if (digits[i] < previous){
                return false;
            } else {
                previous = digits[i];
            }
        }

        return true;
    }
}
