package by.paa.tasks3.secondHalf;


/**
 * 12. Даны числа X, Y, Z, Т — длины сторон четырехугольника. Написать метод(методы) вычисления его площади,
 * если угол между сторонами длиной X и Y— прямой.
 */
public class TaskTwelve {

    public String resultOfQuadrangleArea(double X, double Y, double Z, double T) {
        return String.format("площадь четырехугольника со сторонами %f, %f, %f, %f = %f", X, Y, Z, T, quadrangleArea(X, Y, Z, T));
    }


    public double quadrangleArea(double X, double Y, double Z, double T) {
        double result = secondTriangleSquare(X, Y, Z, T) + squareOfRightTriangle(X, Y);
        return result;
    }

    /**
     * through X Y calc third side of triangle Z T and hypotenuse X Y
     *
     * @param X
     * @param Y
     * @param Z
     * @param T
     * @return
     */
    private double secondTriangleSquare(double X, double Y, double Z, double T) {
        double a = Z;
        double b = T;
        double c = hypotenuse(X, Y);
        double p = (a + b + c) / 2;
        double square = Math.sqrt(p * (p - a) * (p - b) * (p - c));
        return square;

    }

    private double hypotenuse(double X, double Y) {
        double hypotenuse = Math.sqrt(Math.pow(X, 2) * Math.pow(Y, 2));
        return hypotenuse;
    }


    private double squareOfRightTriangle(double X, double Y) {
        return 0.5 * X * Y;
    }


}
