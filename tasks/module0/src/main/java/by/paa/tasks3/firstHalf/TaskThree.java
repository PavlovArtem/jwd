package by.paa.tasks3.firstHalf;


/**
 * 3. Написать метод(методы) для нахождения наибольшего общего делителя четырех натуральных чисел.
 */
public class TaskThree {


    public void showGcd(int a, int b, int c, int d) {
        String answer = String.format("НОД для чисел %d, %d, %d, %d = %d", a,b,c,d,gcdFourNums(a,b,c,d));
        System.out.println(answer);
    }

    private int gcdFourNums(int a, int b, int c, int d) {
        int[] nums = {a, b, c, d};

        int gcd = nums[0];
        for (int i = 0; i < nums.length; i++) {
            gcd = getGcd(gcd, nums[i]);
        }
        return gcd;
    }


    private int getGcd(int a, int b) {
        while (b != 0) {
            int tmp = a % b;
            a = b;
            b = tmp;
        }
        return a;
    }

}
