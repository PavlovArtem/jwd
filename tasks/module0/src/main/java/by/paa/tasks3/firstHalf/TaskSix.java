package by.paa.tasks3.firstHalf;


/**
 * 6. Вычислить площадь правильного шестиугольника со стороной а,
 * используя метод вычисления площади
 * треугольника.
 */
public class TaskSix {

    public double hexagonSquare(int side) {
        side = Math.abs(side);
        return isoscelesTriangleSquare(side) * 6;
    }


    private double isoscelesTriangleSquare(int side) {
        return 0.5 * side * apothemOfHexagon(side);
    }


    private double apothemOfHexagon(int side) {
        double apothem = side * (Math.sqrt(3) / 2);
        return apothem;
    }

}
