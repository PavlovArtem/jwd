package by.paa.tasks3.secondHalf;


import java.util.LinkedList;
import java.util.List;

/**
 * 17. Натуральное число, в записи которого n цифр, называется числом Армстронга, если сумма его цифр, возведенная
 * в степень n, равна самому числу. Найти все числа Армстронга от 1 до k. Для решения задачи использовать
 * декомпозицию. !!! некорректно написано задание !!!
 */
public class TaskSeventeen {

    public static void main(String[] args) {

    }


    public List<Integer> armstrongNumbersInRange(int k) {
        List<Integer> armstrongNumbers = new LinkedList<>();
        for (int i = 1; i < k; i++){
            if (isArmstrongNumber(i)){
                armstrongNumbers.add(i);
            }
        }
        return armstrongNumbers;
    }

    private boolean isArmstrongNumber(int a){
        int[] digits = createArrayFromNumber(a);
        return a == sumOfPoweredDigits(digits);
    }

    private int sumOfPoweredDigits(int[] digits) {
        int result = 0;
        for (int digit : digits) {
            result += Math.pow(digit, digits.length);
        }
        return result;

    }


    private int[] createArrayFromNumber(int n) {
        String string = String.valueOf(Math.abs(n));
        char[] digits = string.toCharArray();
        int[] result = new int[digits.length];

        for (int i = 0; i < digits.length; i++) {
            result[i] = Character.getNumericValue(string.charAt(i));
        }
        return result;
    }


}
