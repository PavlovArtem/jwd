package by.paa.tasks3;

import by.paa.tasks3.firstHalf.*;
import by.paa.tasks3.secondHalf.*;

import java.util.Arrays;

public class DecompositionTasksTest {


    public static void main(String[] args) {


        System.out.println("==============================");

        System.out.println("1. Треугольник задан координатами своих вершин.\n" +
                "Написать метод для вычисления его площади.");
        TaskOne taskOne = new TaskOne();
        System.out.println("Triangle square = " + taskOne.getTriangleSquare(1, 3, 2, -5, -8, 4));

        System.out.println("==============================");

        System.out.println("2. Написать метод(методы) для нахождения наибольшего общего делителя и наименьшего общего кратного двух\n" +
                "натуральных чисел:");
        TaskTwo taskTwo = new TaskTwo();
        taskTwo.resultLcmGcd(15, 30);

        System.out.println("==============================");

        System.out.println("3. Написать метод(методы) для нахождения наибольшего общего делителя четырех натуральных чисел.");
        TaskThree taskThree = new TaskThree();
        taskThree.showGcd(12, 24, 84, 26);
        taskThree.showGcd(15, 30, 45, 50);
        taskThree.showGcd(0, 5, 13, 20);

        System.out.println("==============================");

        System.out.println("4. Написать метод(методы) для нахождения наименьшего\n" +
                "общего кратного трех натуральных чисел.");
        TaskFour taskFour = new TaskFour();
        taskFour.showGcdAndLcm(15, 30, 45);

        System.out.println("==============================");

        System.out.println(" 5. Написать метод(методы) для нахождения\n" +
                "суммы большего и меньшего из трех чисел.");
        TaskFive taskFive = new TaskFive();
        taskFive.calculateSum(1, 5, 10);

        System.out.println("==============================");

        System.out.println("6. Вычислить площадь правильного шестиугольника со стороной а,\n" +
                "используя метод вычисления площади\n" +
                "треугольника.");
        TaskSix taskSix = new TaskSix();
        System.out.println(String.format("Площадь правильного шестиуголька со стороной %f = %f", 6d, taskSix.hexagonSquare(6)));
        System.out.println(String.format("Площадь правильного шестиуголька со стороной %f = %f", -6d, taskSix.hexagonSquare(6)));
        System.out.println(String.format("Площадь правильного шестиуголька со стороной %f = %f", 0d, taskSix.hexagonSquare(0)));

        System.out.println("==============================");

        System.out.println("7. На плоскости заданы своими координатами n точек. Написать метод(методы), определяющие, между какими из\n" +
                "пар точек самое большое расстояние. Указание. Координаты точек занести в массив.");
        TaskSeven taskSeven = new TaskSeven();
        System.out.println(Arrays.toString(taskSeven.graitestDistanceBetweenPoints(1, 1, 2, 2, 6, 6, 5, -2, -3, -3)));

        System.out.println("==============================");

        System.out.println("8. Составить программу, которая в массиве A[N] находит второе по величине число (вывести на печать число,\n" +
                "которое меньше максимального элемента массива, но больше всех других элементов).");

        TaskEight taskEight = new TaskEight();
        int[] test = {1, 2, 3, 4, 5, 6, 7, 8, 13, 9, 1, 2};
        int[] test1 = {-1, -2, -3, 0};
        taskEight.preMaxElement(test);
        taskEight.preMaxElement(test1);

        System.out.println("==============================");

        System.out.println("9. Написать метод(методы), проверяющий, являются ли данные три числа взаимно простыми.");
        TaskNine taskNine = new TaskNine();
        taskNine.showIsMutuallySimpleNumber(16, 17);

        System.out.println("==============================");

        System.out.println("10. Написать метод(методы) для вычисления суммы факториалов всех нечетных чисел от 1 до 9.");
        TaskTen taskTen = new TaskTen();
        taskTen.factorialOfOddNums(1, 9);

        System.out.println("==============================");

        System.out.println("11. Задан массив D. Определить следующие суммы: D[l] + D[2] + D[3]; D[3] + D[4] + D[5]; D[4] +D[5] +D[6].\n" +
                "Пояснение. Составить метод(методы) для вычисления суммы трех последовательно расположенных элементов\n" +
                "массива с номерами от k до m.");
        TaskEleven taskEleven = new TaskEleven();
        int[] test11 = {1, 1, 1, 2, 2, 2, 3, 3, 3};
        taskEleven.getResult(test11, 0, 9);
        System.out.println("==============================");

        System.out.println(" 12. Даны числа X, Y, Z, Т — длины сторон четырехугольника. Написать метод(методы) вычисления его площади,\n" +
                "если угол между сторонами длиной X и Y— прямой.");
        TaskTwelve taskTwelve = new TaskTwelve();
        System.out.println(taskTwelve.resultOfQuadrangleArea(2, 3, 4, 5));

        System.out.println("==============================");

        System.out.println("13. Дано натуральное число N.\n" +
                "Написать метод(методы) для формирования массива, элементами которого являются\n" +
                "цифры числа N.");
        TaskThirteen taskThirteen = new TaskThirteen();
        System.out.println(Arrays.toString(taskThirteen.createArrayFromNumber(45879)));
        System.out.println(Arrays.toString(taskThirteen.createArrayFromNumber(1258597896)));
        System.out.println(Arrays.toString(taskThirteen.createArrayFromNumber(0)));

        System.out.println("==============================");

        System.out.println("14. Написать метод(методы),\n" +
                "определяющий, в каком из данных двух чисел больше цифр.");
        TaskFourteen taskFourteen = new TaskFourteen();
        System.out.println(taskFourteen.largestNumber(-423, 123));
        System.out.println(taskFourteen.largestNumber(1025, 123));
        System.out.println(taskFourteen.largestNumber(10, 123));

        System.out.println("==============================");

        System.out.println("15. Даны натуральные числа К и N. Написать метод(методы) формирования массива А, элементами которого\n" +
                "являются числа, сумма цифр которых равна К и которые не большее N.");
        TaskFifteen taskFifteen = new TaskFifteen();
        System.out.println("сумма цифр которых равна К и которые не большее N:");
        for (int n : taskFifteen.sequenceOfDigitsEqualsK(15, 100)) {
            System.out.print(n + " ");
        }
        System.out.println();

        System.out.println("==============================");

        System.out.println("16. Два простых числа называются «близнецами», если они отличаются друг от друга на 2 (например, 41 и 43). Найти\n" +
                "и напечатать все пары «близнецов» из отрезка [n,2n], где n - заданное натуральное число больше 2. Для решения\n" +
                "задачи использовать декомпозицию.");
        TaskSixteen taskSixteen = new TaskSixteen();
        System.out.println();
        for (int num : taskSixteen.getResultList(50)) {
            System.out.print(num + " ");
        }
        System.out.println();

        System.out.println("==============================");

        System.out.println("17. Натуральное число, в записи которого n цифр, называется числом Армстронга, если сумма его цифр, возведенная\n" +
                "в степень n, равна самому числу. Найти все числа Армстронга от 1 до k. Для решения задачи использовать\n" +
                "декомпозицию.");
        TaskSeventeen taskSeventeen = new TaskSeventeen();
        taskSeventeen.armstrongNumbersInRange(100_000).iterator().forEachRemaining(System.out::println);

        System.out.println("==============================");

        System.out.println("18. Найти все натуральные n-значные числа, цифры в которых образуют строго возрастающую последовательность\n" +
                "(например, 1234, 5789). Для решения задачи использовать декомпозицию.");
        TaskEighteen taskEighteen = new TaskEighteen();
        int[] testFor18 = {123, 253, 2458, 1478, 9546, 1591, 6789};
        taskEighteen.showNumbersWithIncreaseDigits(taskEighteen
                .numbersWithIncreasingDigits(testFor18));

        System.out.println("==============================");

        System.out.println("19. Написать программу, определяющую сумму n - значных чисел, содержащих только нечетные цифры. Определить\n" +
                "также, сколько четных цифр в найденной сумме. Для решения задачи использовать декомпозицию.");
        TaskNineteen taskNineteen = new TaskNineteen();
        System.out.println(taskNineteen.showResults(4));

        System.out.println("==============================");

        System.out.println("20. Из заданного числа вычли сумму его цифр. Из результата вновь вычли сумму его цифр и т.д. Сколько таких\n" +
                "действий надо произвести, чтобы получился нуль? Для решения задачи использовать декомпозицию.");
        TaskTwenty taskTwenty = new TaskTwenty();
        System.out.println(taskTwenty.diference(115));
        System.out.println(taskTwenty.diference(-115));
        System.out.println(taskTwenty.diference(12_000_154));

        System.out.println("==============================");


    }


}
