package by.paa.tasks3.firstHalf;




/**
 * 10. Написать метод(методы) для вычисления суммы факториалов всех нечетных чисел от 1 до 9.
 */
public class TaskTen {


    public void factorialOfOddNums(int rangeStart, int rangeEnd){
        int[] result = new int[rangeEnd];
        for (int i = rangeStart; i <= rangeEnd; i++){
            if (i % 2 != 0){
                System.out.println(String.format("factorial of %d = %d",i,getFactorial(i)));
            }
        }
    }


    private long getFactorial(int n){
        int delimeter = n - 1;
        long result = n;

        while (delimeter > 0){
            result *= delimeter;
            --delimeter;
        }

        return result;
    }

}
