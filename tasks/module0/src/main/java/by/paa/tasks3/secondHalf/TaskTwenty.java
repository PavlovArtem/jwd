package by.paa.tasks3.secondHalf;


/**
 * 20. Из заданного числа вычли сумму его цифр. Из результата вновь вычли сумму его цифр и т.д. Сколько таких
 * действий надо произвести, чтобы получился нуль? Для решения задачи использовать декомпозицию.
 */
public class TaskTwenty {

    public int diference(int n) {
        n = Math.abs(n);
        int counter = 0;
        while (n != 0) {
            n -= sumOfDigits(n);
            counter++;
        }
        return counter;
    }

    private int sumOfDigits(int n) {
        String string = String.valueOf(Math.abs(n));
        char[] digits = string.toCharArray();
        //int[] result = new int[digits.length];
        int result = 0;

        for (int i = 0; i < digits.length; i++) {
            result += Character.getNumericValue(string.charAt(i));
        }

        return result;
    }


}
