package by.paa.tasks4.oop.task2.factory;

import by.paa.tasks4.oop.task2.factory.Engine;

public class GasEngine implements Engine {

    private final int power;

    public GasEngine(int power) {
        this.power = power;
    }

    @Override
    public boolean start() {
        System.out.println("Gas engine is running");
        return true;
    }

    @Override
    public int run() {
        return power;
    }

    @Override
    public void stop() {
        System.out.println("Engine was stopped");
    }
}
