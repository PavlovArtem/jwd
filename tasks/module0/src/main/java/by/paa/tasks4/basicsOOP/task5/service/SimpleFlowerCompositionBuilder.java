package by.paa.tasks4.basicsOOP.task5.service;


import by.paa.tasks4.basicsOOP.task5.entity.Flower;
import by.paa.tasks4.basicsOOP.task5.entity.FlowerComposition;
import by.paa.tasks4.basicsOOP.task5.entity.FlowerWrapper;

public class SimpleFlowerCompositionBuilder implements Builder {



    @Override
    public void reset() {

    }

    @Override
    public void addFlower(Flower flower) {

    }

    @Override
    public void addWrapper(FlowerWrapper flowerWrapper) {

    }

    @Override
    public FlowerComposition getFlowerComposition() {

        return new FlowerComposition();
    }
}
