package by.paa.tasks4.oop.task2.factory;

import by.paa.tasks4.oop.task2.factory.Wheel;

public class SimpleWheel implements Wheel {

    private final int diameter;

    public SimpleWheel(int diameter){
        this.diameter = diameter;
    }

    @Override
    public int spin(int energy) {
        return diameter / energy;
    }
}
