package by.paa.tasks4.simpleObjects.task9;

import by.paa.tasks4.simpleObjects.task9.entity.Book;
import by.paa.tasks4.simpleObjects.task9.entity.BookCollection;
import by.paa.tasks4.simpleObjects.task9.service.SearchBooksService;


/**
 *
 */
public class BooksSearchConsolePresentation {

    private SearchBooksService searchBooksService;


    public BooksSearchConsolePresentation(SearchBooksService searchBooksService) {
        this.searchBooksService = searchBooksService;
    }


    /**
     *
     * @param key -a : author, -p : publisher, -y: year
     * @param param
     * @return
     */
    public BookCollection searchBooks(String key, String param) {
        BookCollection bookCollection = new BookCollection();
        switch (key) {
            case "-a":
                printCollection(searchBooksService.findBooksByAuthor(param));
                break;
            case "-p":
                printCollection(searchBooksService.findBooksByPublisher(param));
                break;
            case "-y":
                int year = 0;
                try {
                    year = Integer.parseInt(param);
                } catch (NumberFormatException ex) {
                    System.out.println("Wrong number format");
                    break;
                }
                printCollection(searchBooksService.findBooksReleasedAfterYear(year));
                break;
            default:
                System.out.println("Wrong key");
                break;
        }
        return bookCollection;
    }


    public void printCollection(BookCollection bookCollection) {
        for(Book book: bookCollection.getBooks()){
            System.out.println(book);
        }

    }


}
