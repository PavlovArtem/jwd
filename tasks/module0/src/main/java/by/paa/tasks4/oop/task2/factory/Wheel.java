package by.paa.tasks4.oop.task2.factory;

public interface Wheel {

    int spin(int energy);

}
