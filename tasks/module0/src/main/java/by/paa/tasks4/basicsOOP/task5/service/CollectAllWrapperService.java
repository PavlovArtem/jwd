package by.paa.tasks4.basicsOOP.task5.service;


import by.paa.tasks4.basicsOOP.task5.entity.Flower;
import by.paa.tasks4.basicsOOP.task5.entity.FlowerWrapper;

import java.util.List;

/**
 *
 */
public interface CollectAllWrapperService {

    List<Flower> getAllWrapper();

    FlowerWrapper getConcreteFlower();
}
