package by.paa.tasks4.basicsOOP.task4;

import by.paa.tasks4.basicsOOP.task4.entity.Treasure;
import by.paa.tasks4.basicsOOP.task4.service.CaveTreasureInfoService;

import java.util.List;
import java.util.Scanner;

public class ConsolePresentation {

    private CaveTreasureInfoService caveTreasureInfoService;

    public ConsolePresentation(CaveTreasureInfoService caveTreasureInfoService) {
        this.caveTreasureInfoService = caveTreasureInfoService;
    }

    public void runGame() {
        boolean gameInProcess = true;

        while (gameInProcess) {
            showMenu();
            Scanner scanner = new Scanner(System.in);
            final String userInput = scanner.next();
            switch (userInput) {
                case "-a":
                    printTreasureList(caveTreasureInfoService.getAllTreasure());
                    break;
                case "-b":
                    System.out.println(caveTreasureInfoService.getMostExpensiveTreasure());
                    break;
                case "-c":
                    System.out.println("Type amount");
                    Scanner scanner1 = new Scanner(System.in);
                    int amount = 0;
                    if (scanner1.hasNextInt()) {
                        amount = scanner1.nextInt();
                        printTreasureList(caveTreasureInfoService.getTreasuresInTheAmount(amount));
                    } else {
                        System.out.println("Wrong input of amount");
                    }
                    break;
                case "-exit":
                    gameInProcess = false;
                    break;
                default:
                    System.out.println("Wrong input");
                    break;

            }
        }

    }

    private void showMenu() {
        System.out.println("Welcome to dragon cave");
        System.out.println("Please choose what do you want to do");
        System.out.println("-a : show all treasures");
        System.out.println("-b : show most expensive treasure");
        System.out.println("-c : show treasure in amount");
        System.out.println("-exit : exit");
    }

    private void printTreasureList(List<Treasure> treasureList) {
        for (Treasure treasure : treasureList) {
            System.out.println(treasure);
        }
    }

}
