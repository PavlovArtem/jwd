package by.paa.tasks4.oop.task2.factory;

public interface Engine {
    boolean start();
    int run();
    void stop();

}
