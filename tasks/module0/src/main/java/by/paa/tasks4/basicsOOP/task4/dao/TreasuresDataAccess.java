package by.paa.tasks4.basicsOOP.task4.dao;


import by.paa.tasks4.basicsOOP.task4.entity.Treasure;

import java.util.List;

public interface TreasuresDataAccess {
    List<Treasure> getData();
    void addData();

}
