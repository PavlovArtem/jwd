package by.paa.tasks4.basicsOOP.task3;

public interface DaySupplier {

    int days(int year);

}
