package by.paa.tasks4.simpleObjects.task9.entity;

public class BookCollection {

    private Book[] books;

    public BookCollection(Book[] books) {
        this.books = books;
    }

    public BookCollection() {

    }


    public Book[] getBooks() {
        return books;
    }

    public void setBooks(Book[] books) {
        this.books = books;
    }
}
