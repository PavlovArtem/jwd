package by.paa.tasks4.basicsOOP.task3;

public enum Month implements DaySupplier {

    JANUARY("January",1) {
        @Override
        public int days(int year){
            return 31;
        }
    },
    FEBRUARY("February",2) {
        @Override
        public int days(int year) {
            int daysInMonth;
            if (year % 4 !=0 || year % 100 == 0 || year % 400 != 0){
                daysInMonth = 29;
            } else {
                daysInMonth = 28;
            }
            return daysInMonth;
        }
    },
    MARCH("March", 3) {
        @Override
        public int days(int year){
            return 31;
        }
    },
    APRIL("April",4){
        @Override
        public int days(int year){
            return 30;
        }
    },
    MAY("May",5){
        @Override
        public int days(int year) {
            return 31;
        }
    },
    JUNE("June",6) {
        @Override
        public int days(int year) {
            return 30;
        }
    },
    JULY("July",7) {
        @Override
        public int days(int year) {
            return 31;
        }
    },
    AUGUST("August",8) {
        @Override
        public int days(int year) {
            return 31;
        }
    },
    SEPTEMBER("September",9) {
        @Override
        public int days(int year) {
            return 30;
        }
    },
    OCTOBER("October",10){
        @Override
        public int days(int year) {
            return 31;
        }
    },
    NOVEMBER("November",11) {
        @Override
        public int days(int year) {
            return 30;
        }
    },
    DECEMBER("December",12) {
        @Override
        public int days(int year) {
            return 31;
        }
    };


    Month(String monthName, int monthCount) {
    }

}
