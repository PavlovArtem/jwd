package by.paa.tasks4.basicsOOP.task4.dao;


import by.paa.tasks4.basicsOOP.task4.entity.Treasure;

import java.io.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class TreasureDataFromFile implements TreasuresDataAccess {

    @Override
    public List<Treasure> getData() {
        List<Treasure> treasureList = new ArrayList<>();

        try(ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("treasures.dat"))){
            treasureList = ((List<Treasure>) objectInputStream.readObject());

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return treasureList;
    }

    @Override
    public void addData()  {
        try(ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("treasures.dat"))){
            objectOutputStream.writeObject(initData());


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<Treasure> initData(){
        List<Treasure> treasureList = new LinkedList<>();

        for (int i = 0; i < 100; i++) {
            double price = (Math.random() * 1000 - 1) + 1;
            Treasure treasure = new Treasure(price, "Какое-то сокровище номер " + (i + 1));
            treasureList.add(treasure);
        }
        return treasureList;
    }
}
