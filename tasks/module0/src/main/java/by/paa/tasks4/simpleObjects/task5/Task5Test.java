package by.paa.tasks4.simpleObjects.task5;

public class Task5Test {

    public static void main(String[] args) {
        DecimalCounter decimalCounter = new DecimalCounter(0,2,10);

        for (int i = 0; i < 10; i++) {
            decimalCounter.increaseCounter();
            System.out.println(decimalCounter.getCurrentState());
        }
    }


}
