package by.paa.tasks4.simpleObjects.task9.repository;

import by.paa.tasks4.simpleObjects.task9.entity.Book;
import by.paa.tasks4.simpleObjects.task9.entity.BookCollection;

import java.util.List;

public interface BooksRepository {

    List<Book> getAllBook();

}
