package by.paa.tasks4.oop.task5.repository;

import java.math.BigDecimal;
import java.util.Map;

public interface CollectTourData {

    Map<String, BigDecimal> getTours();
    Map<String, Double> getTransport();
    Map<String, Double> getMeal();


}
