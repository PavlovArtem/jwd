package by.paa.tasks4.simpleObjects.task5;


import java.util.Objects;

/**
 * 5. Опишите класс, реализующий десятичный счетчик,
 * который может увеличивать или уменьшать свое значение на единицу в заданном диапазоне.
 * Предусмотрите инициализацию счетчика значениями по умолчанию и произвольными значениями.
 * Счетчик имеет методы увеличения и уменьшения состояния, и метод позволяющее получить его текущее состояние.
 * Написать код, демонстрирующий все возможности класса.
 */
public class DecimalCounter {

    private int minValue;
    private int counter;
    private int maxValue;


    public DecimalCounter(int minValue, int counter, int maxValue) {
        this.minValue = minValue;
        this.counter = counter;
        this.maxValue = maxValue;
    }


    public DecimalCounter(){
        minValue = 0;
        counter = 0;
        maxValue = 10;
    }


    public void increaseCounter() {
        if (counter < maxValue) {
            ++counter;
        }
    }


    public void decreaseCounter() {
        if (counter > minValue){
            --counter;
        }
    }


    public int getCurrentState() {
        return counter;
    }

}
