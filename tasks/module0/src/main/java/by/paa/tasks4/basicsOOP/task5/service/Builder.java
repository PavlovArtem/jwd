package by.paa.tasks4.basicsOOP.task5.service;

import by.paa.tasks4.basicsOOP.task5.entity.Flower;
import by.paa.tasks4.basicsOOP.task5.entity.FlowerComposition;
import by.paa.tasks4.basicsOOP.task5.entity.FlowerWrapper;

public interface Builder {
    void reset();
    void addFlower(Flower flower);
    void addWrapper(FlowerWrapper flowerWrapper);
    FlowerComposition getFlowerComposition();

}
