package by.paa.tasks4.simpleObjects.task3;

public class TestApp {

    public static void main(String[] args) {
        Student[] students = new Student[10];
        students[0] = new Student("Иванов И.С.", "2405",new int[] {8,8,9,9,10});
        students[1] = new Student("Сидоров А.Г.", "2405",new int[] {9,8,9,9,10});
        students[2] = new Student("Каркасов Д.И.", "2405",new int[] {10,9,9,9,10});
        students[3] = new Student("Куприянов И.С.", "2405",new int[] {7,8,9,9,10});
        students[4] = new Student("Неважно И.С.", "2405",new int[] {8,8,9,9,10});
        students[5] = new Student("Важно И.С.", "2001",new int[] {9,9,9,9,10});
        students[6] = new Student("Суровый И.С.", "1587",new int[] {9,8,9,9,5});
        students[7] = new Student("Суровый И.С.", "1587",new int[] {9,8,9,9,5});
        students[8] = new Student("Суровый И.С.", "1587",new int[] {9,8,9,9,5});
        students[9] = new Student("Суровый И.С.", "1587",new int[] {9,8,9,9,5});


        StudentsInfo studentsInfo = new StudentsInfo();
        studentsInfo.showStudentsInfoWithHighestMarks(students);
    }


}
