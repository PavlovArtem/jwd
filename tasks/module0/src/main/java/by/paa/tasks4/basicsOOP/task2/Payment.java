package by.paa.tasks4.basicsOOP.task2;


import java.math.BigDecimal;
import java.util.List;

/**
 * Задача 2.
 * Создать класс Payment с внутренним классом,
 * с помощью объектов которого можно сформировать
 * покупку из нескольких товаров.
 */
//TODO: доделать
public class Payment {

    private BigDecimal amount;
    private List<Product> productList;

    public void purchase(){

    }


    private class Product {
        private String productName;
        private BigDecimal price;

        public Product(String productName, BigDecimal price) {
            this.productName = productName;
            this.price = price;
        }
    }

}
