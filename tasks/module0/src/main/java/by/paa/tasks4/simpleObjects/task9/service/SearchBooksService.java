package by.paa.tasks4.simpleObjects.task9.service;


import by.paa.tasks4.simpleObjects.task9.entity.Book;
import by.paa.tasks4.simpleObjects.task9.entity.BookCollection;

/**
 * * Найти и вывести:
 *  * a) список книг заданного автора;
 *  * b) список книг, выпущенных заданным издательством;
 *  * c) список книг, выпущенных после заданного года.
 */
public interface SearchBooksService {

    BookCollection findBooksByAuthor(String author);
    BookCollection findBooksByPublisher(String publisher);
    BookCollection findBooksReleasedAfterYear(int year);



}
