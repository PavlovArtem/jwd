package by.paa.tasks4.simpleObjects.task9;

import by.paa.tasks4.simpleObjects.task9.repository.BooksInMemory;
import by.paa.tasks4.simpleObjects.task9.repository.BooksRepository;
import by.paa.tasks4.simpleObjects.task9.service.SearchBooksImpl;
import by.paa.tasks4.simpleObjects.task9.service.SearchBooksService;

public class BookApp {

    public static void main(String[] args) {
        BooksRepository booksRepository = new BooksInMemory();
        SearchBooksService booksService = new SearchBooksImpl(booksRepository);
        BooksSearchConsolePresentation booksSearchConsolePresentation = new BooksSearchConsolePresentation(booksService);

        booksSearchConsolePresentation.searchBooks("-a", "George Orwell");
        booksSearchConsolePresentation.searchBooks("-p", "Питер");
        booksSearchConsolePresentation.searchBooks("-y", "1900");


    }

}
