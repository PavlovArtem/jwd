package by.paa.tasks4.basicsOOP.task4.entity;

import java.io.Serializable;
import java.util.Objects;

public class Treasure implements Serializable {

    private final double price;
    private final String name;

    public Treasure(double price, String name) {
        this.price = price;
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Treasure treasure = (Treasure) o;
        return Double.compare(treasure.price, price) == 0 &&
                Objects.equals(name, treasure.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(price, name);
    }

    @Override
    public String toString() {
        return "Treasure{" +
                "price=" + price +
                ", name='" + name + '\'' +
                '}';
    }
}
