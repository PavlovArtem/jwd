package by.paa.tasks4.simpleObjects.task9.repository;

import by.paa.tasks4.simpleObjects.task9.entity.Book;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class BooksInMemory implements BooksRepository{

    List<Book> books = new ArrayList<>();

    public BooksInMemory(){
        init();
    }


    private void init(){

        books.add(new Book(1,"1984", Collections.singletonList("George Orwell"),"Secer and Warburg",1949,300, BigDecimal.valueOf(100L),"Hard cover"));
        books.add(new Book(1,"Анна Каренина", Collections.singletonList("Лев Толстой"),"Питер",1878,1120, BigDecimal.valueOf(120L),"Hard cover"));
        books.add(new Book(3,"Animal Farm: A Fairy Story", Collections.singletonList("George Orwell"),"Secer and Warburg",1945,150, BigDecimal.valueOf(50L),"Hard cover"));

    }

    @Override
    public List<Book> getAllBook() {
        return books;
    }
}
