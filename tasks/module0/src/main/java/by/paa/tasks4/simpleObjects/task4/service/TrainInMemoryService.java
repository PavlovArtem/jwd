package by.paa.tasks4.simpleObjects.task4.service;


import by.paa.tasks4.simpleObjects.task4.dao.TrainsInMemoryStorage;
import by.paa.tasks4.simpleObjects.task4.entity.Train;

import java.util.Arrays;
import java.util.Comparator;

public class TrainInMemoryService {

    private TrainsInMemoryStorage storage;

    public TrainInMemoryService(TrainsInMemoryStorage trainsInMemoryStorage) {
        this.storage = trainsInMemoryStorage;
    }


    public Train[] sortTrainsByNumber() {
        Train[] sortedTrains = storage.getTrains();
        Comparator<Train> comparator = ((Comparator.comparing(Train::getTrainNumber)));
        Arrays.sort(sortedTrains, comparator);
        return sortedTrains;
    }


    public Train findTrainByNumber(String number) {
        for(Train train : storage.getTrains()) {
            if (train.getTrainNumber().equals(number)){
                return train;
            }
        }

        return new Train();
    }

    public Train[] sortTrainsByDestinationThanByDeparture() {
        Train[] sortedTrains = storage.getTrains();
        Comparator<Train> comparator = (Comparator.comparing(Train::getDestinationName)
                .thenComparing(Train::getDepartureTime));
        Arrays.sort(sortedTrains,comparator);
        return sortedTrains;
    }


}
