package by.paa.tasks4.simpleObjects.task3;


import java.util.LinkedList;
import java.util.List;

/**
 * 3. Создайте класс с именем Student, содержащий поля:
 * фамилия и инициалы, номер группы, успеваемость (массив из пяти элементов).
 * Создайте массив из десяти элементов такого типа.
 * Добавьте возможность вывода фамилий и номеров групп студентов, имеющих оценки, равные только 9 или 10.
 */
public class StudentsInfo {

    public void showStudentsInfoWithHighestMarks(Student[] students) {
        for (Student student: getStudentsWithHighestMarks(students)) {
            System.out.println("студент: " + student.getLastNameAndInitials() + " номер группы: " + student.getGroupNumber());
        }
    }

    public List<Student> getStudentsWithHighestMarks(Student[] students) {
        List<Student> studentList = new LinkedList<>();
        for (Student student : students) {
            if (isHighestMarks(student.getMarks())) {
                studentList.add(student);
            }
        }

        return studentList;
    }


    private boolean isHighestMarks(int[] marks) {
        for (int mark : marks) {
            if (mark < 9) {
                return false;
            }
        }
        return true;
    }


}
