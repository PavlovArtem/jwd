package by.paa.tasks4.basicsOOP.task4.controller;

public interface TreasureCaveController {

    void getAllTreasures();
    void getMostExpensiveTreasure();
    void getTreasuresInAmount(int amount);


}
