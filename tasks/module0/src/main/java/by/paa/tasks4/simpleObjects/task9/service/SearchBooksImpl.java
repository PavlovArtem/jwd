package by.paa.tasks4.simpleObjects.task9.service;

import by.paa.tasks4.simpleObjects.task9.entity.Book;
import by.paa.tasks4.simpleObjects.task9.entity.BookCollection;
import by.paa.tasks4.simpleObjects.task9.repository.BooksRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class SearchBooksImpl implements SearchBooksService {

    private final BooksRepository booksRepository;

    public SearchBooksImpl(BooksRepository booksRepository) {
        this.booksRepository = booksRepository;
    }

    @Override
    public BookCollection findBooksByAuthor(String author) {
        ArrayList<Book> booksToAdd = new ArrayList<>();
        for (Book book: booksRepository.getAllBook()){
            if (book.getAuthors().contains(author)){
                booksToAdd.add(book);
            }
        }
        return new BookCollection(booksToAdd.toArray(new Book[0]));
    }

    public BookCollection findBooksByPublisher(String publisher) {
        ArrayList<Book> booksToAdd = new ArrayList<>();
        for (Book book: booksRepository.getAllBook()){
            if (book.getPublisher().equals(publisher)){
                booksToAdd.add(book);
            }
        }

        return new BookCollection(booksToAdd.toArray(new Book[0]));
    }

    @Override
    public BookCollection findBooksReleasedAfterYear(int year) {
        ArrayList<Book> booksToAdd = new ArrayList<>();
        for (Book book: booksRepository.getAllBook()){
            if (book.getYear() > year){
                booksToAdd.add(book);
            }
        }

        return new BookCollection(booksToAdd.toArray(new Book[0]));
    }
}
