package by.paa.tasks4.basicsOOP.task5.controller;

public interface FlowerCreateController {


    void showAllFlower();
    void showAllWrapper();
    void addFlower();
    void addWrapper();
    void reset();
    void createComposition();

}
