package by.paa.tasks4.oop.task2;

import by.paa.tasks4.oop.task2.core.Car;
import by.paa.tasks4.oop.task2.factory.CarFactory;

public class CarAppTest {

    public static void main(String[] args) {
        CarFactory carFactory = new CarFactory();
        Car ferrari = carFactory.createCar("GAS","SIMPLE","ferrari",10);
        Car bmw = carFactory.createCar("DIESEL","SIMPLE","bmw530", 10);

        ferrari.move();
        ferrari.showCarModel();
        ferrari.fillTank(3);
        ferrari.changeWheel();
        bmw.move();
        bmw.showCarModel();
    }

}
