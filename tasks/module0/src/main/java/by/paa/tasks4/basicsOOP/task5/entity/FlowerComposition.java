package by.paa.tasks4.basicsOOP.task5.entity;

import java.util.List;

public class FlowerComposition {
    private List<Flower> flowers;
    private List<FlowerWrapper> wrappers;

    public List<Flower> getFlowers() {
        return flowers;
    }

    public void setFlowers(List<Flower> flowers) {
        this.flowers = flowers;
    }

    public List<FlowerWrapper> getWrappers() {
        return wrappers;
    }

    public void setWrappers(List<FlowerWrapper> wrappers) {
        this.wrappers = wrappers;
    }
}
