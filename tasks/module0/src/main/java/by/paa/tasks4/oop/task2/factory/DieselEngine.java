package by.paa.tasks4.oop.task2.factory;

public class DieselEngine implements Engine{

    private final int power;

    public DieselEngine(int power) {
        this.power = power;
    }

    @Override
    public boolean start() {
        System.out.println("Diesel engine start");
        return true;
    }

    @Override
    public int run() {

        return power;
    }

    @Override
    public void stop() {
        System.out.println("Engine was stopped");
    }
}
