package by.paa.tasks4.oop.task2.core;

import by.paa.tasks4.oop.task2.factory.Engine;
import by.paa.tasks4.oop.task2.factory.Wheel;


/**
 * 2. Создать объект класса Автомобиль,
 * используя классы Колесо, Двигатель.
 * Методы: ехать, заправляться, менять колесо,
 * вывести на консоль марку автомобиля.
 */
public class Car {

    private final String model;
    private final Wheel wheels;
    private final Engine engine;
    private int fuelTankCapacity;

    public Car(String model, Wheel wheels, Engine engine, int fuelTankCapacity) {
        this.model = model;
        this.wheels = wheels;
        this.engine = engine;
        this.fuelTankCapacity = fuelTankCapacity;
    }


    public void move(){
        boolean isStart = engine.start();
        if (!isStart){
            System.out.println("problems with engine start");
        }

        boolean keepMoving = true;
        while (keepMoving){
            int energy = engine.run();
            wheels.spin(energy);
            --fuelTankCapacity;
            if (fuelTankCapacity == 0){
                System.out.println("Out of fuel!");
                engine.stop();
                keepMoving = false;
            }
        }


    }

    public void fillTank(int capacity){
        this.fuelTankCapacity += capacity;
    }


    public void showCarModel(){
        System.out.println(this.model);
    }

    public void changeWheel(){
        System.out.println("change wheel");
    }

}
