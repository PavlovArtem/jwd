package by.paa.tasks4.oop.task2.factory;

import by.paa.tasks4.oop.task2.core.Car;

public class CarFactory {

    private Engine engineFactory;
    private Wheel wheelFactory;

    public Car createCar(String engineParam, String wheelsParam, String model, int fuel){
        Car car;
        Engine engine;
        Wheel wheel;

        switch (engineParam){
            case "GAS":
                engine = new GasEngine(10);
                break;
            case "DIESEL":
                engine = new DieselEngine(15);
                break;
            default:
                engine = new GasEngine(5);
                System.out.println("No such parameter");
                break;
        }


        switch (wheelsParam){
            case "SIMPLE":
                wheel = new SimpleWheel(30);
                break;
            default:
                wheel = new SimpleWheel(30);
                System.out.println("No such wheels");
                break;
        }
        car = new Car(model,wheel,engine,fuel);
        return car;
    }




}
