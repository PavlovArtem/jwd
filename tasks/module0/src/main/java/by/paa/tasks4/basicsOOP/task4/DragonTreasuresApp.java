package by.paa.tasks4.basicsOOP.task4;

import by.paa.tasks4.basicsOOP.task4.dao.TreasureDataFromFile;
import by.paa.tasks4.basicsOOP.task4.dao.TreasuresDataAccess;
import by.paa.tasks4.basicsOOP.task4.service.CaveTreasureInfoImpl;
import by.paa.tasks4.basicsOOP.task4.service.CaveTreasureInfoService;


public class DragonTreasuresApp {

    public static void main(String[] args) {
        TreasuresDataAccess treasuresDataAccess = new TreasureDataFromFile();
        CaveTreasureInfoService caveInfoService = new CaveTreasureInfoImpl(treasuresDataAccess);

        ConsolePresentation consolePresentation = new ConsolePresentation(caveInfoService);
        consolePresentation.runGame();
    }

}
