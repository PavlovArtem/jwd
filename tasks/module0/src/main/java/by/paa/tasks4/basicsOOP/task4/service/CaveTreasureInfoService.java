package by.paa.tasks4.basicsOOP.task4.service;

import by.paa.tasks4.basicsOOP.task4.entity.Treasure;

import java.util.List;

public interface CaveTreasureInfoService {

    List<Treasure> getAllTreasure();
    Treasure getMostExpensiveTreasure();
    List<Treasure> getTreasuresInTheAmount(double amount);
}
