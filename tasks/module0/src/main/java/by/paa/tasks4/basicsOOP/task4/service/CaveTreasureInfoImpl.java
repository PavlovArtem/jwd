package by.paa.tasks4.basicsOOP.task4.service;

import by.paa.tasks4.basicsOOP.task4.dao.TreasuresDataAccess;
import by.paa.tasks4.basicsOOP.task4.entity.Treasure;

import java.util.*;

public class CaveTreasureInfoImpl implements CaveTreasureInfoService {

    private TreasuresDataAccess treasuresDataAccess;

    public CaveTreasureInfoImpl(TreasuresDataAccess treasuresDataAccess) {
        this.treasuresDataAccess = treasuresDataAccess;
    }

    @Override
    public List<Treasure> getAllTreasure() {
        return treasuresDataAccess.getData();
    }

    @Override
    public Treasure getMostExpensiveTreasure() {

        Treasure treasure = null;
        List<Treasure> treasureList = treasuresDataAccess.getData();
        Comparator<Treasure> comparator = (Comparator.comparingDouble(Treasure::getPrice));

        for (final Treasure cur : treasureList) {
            if (treasure == null) {
                treasure = cur;
            } else if (comparator.compare(cur, treasure) > 0) {
                treasure = cur;
            }
        }

        return treasure;
    }

    @Override
    public List<Treasure> getTreasuresInTheAmount(double amount) {
        double currentAmount = 0d;
        List<Treasure> treasureList = treasuresDataAccess.getData();
        List<Treasure> treasureInAmount = new LinkedList<>();

        for (final Treasure currentTreasure : treasureList) {
            currentAmount += currentTreasure.getPrice();
            if (currentAmount < amount) {
                treasureInAmount.add(currentTreasure);
            } else {
                currentAmount -= currentTreasure.getPrice();
            }
        }

        return treasureInAmount;
    }
}
