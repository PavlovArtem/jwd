package by.paa.tasks4.simpleObjects.task4;

import by.paa.tasks4.simpleObjects.task4.dao.TrainsInMemoryStorage;
import by.paa.tasks4.simpleObjects.task4.entity.Train;
import by.paa.tasks4.simpleObjects.task4.service.TrainInMemoryService;

public class TrainApp {

    public static void main(String[] args) {

        TrainsInMemoryStorage trainsInMemoryStorage = new TrainsInMemoryStorage();
        TrainInMemoryService trainInMemoryService = new TrainInMemoryService(trainsInMemoryStorage);

        System.out.println(trainInMemoryService.findTrainByNumber("1048А"));
        System.out.println("Sort by Numbers");
        showTrains(trainInMemoryService.sortTrainsByNumber());
        System.out.println("Sort by Destination");
        showTrains(trainInMemoryService.sortTrainsByDestinationThanByDeparture());



    }


    private static void showTrains(Train[] trains){
        for (Train train: trains) {
            System.out.println(train);
        }
    }

}
