package by.paa.tasks4.basicsOOP.task5.service;

import by.paa.tasks4.basicsOOP.task5.entity.Flower;

import java.util.List;

public interface CollectAllFlowerService {

    List<Flower> getAllFlowers();

    Flower getConcreteFlower();
}
