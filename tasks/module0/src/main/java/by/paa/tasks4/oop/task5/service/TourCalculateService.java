package by.paa.tasks4.oop.task5.service;

import by.paa.tasks4.oop.task5.entity.Voucher;

import java.util.List;

public interface TourCalculateService {

    List<Voucher> calculateTours();



}
