package by.paa.tasks4.oop.task5.repository;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class ToursDataInMemoryStorage implements CollectTourData {

    private Map<String, BigDecimal> tourType;
    private Map<String, Double> transport;
    private Map<String, Double> meal;

    public ToursDataInMemoryStorage() {
        init();
    }

    private void init() {

        tourType = new HashMap<>();
        tourType.put("Recreation", new BigDecimal(20));
        tourType.put("Excursion", new BigDecimal(30));
        tourType.put("Treatment", new BigDecimal(40));
        tourType.put("Shopping", new BigDecimal(25));
        tourType.put("Cruise", new BigDecimal(100));

        transport = new HashMap<>();
        transport.put("Bus", 0.5);
        transport.put("Train", 1.1);
        transport.put("Plain", 1.2);
        transport.put("Ship", 1.5);

        meal = new HashMap<>();
        meal.put("Only breakfast", 0.9);
        meal.put("Breakfast and dinner", 1.1);
        meal.put("Eat all day", 1.5);
        meal.put("Diet", 1.5);

    }


    @Override
    public Map<String, BigDecimal> getTours() {
        return tourType;
    }

    @Override
    public Map<String, Double> getTransport() {
        return transport;
    }

    @Override
    public Map<String, Double> getMeal() {
        return meal;
    }
}
