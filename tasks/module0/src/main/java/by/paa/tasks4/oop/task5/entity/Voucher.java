package by.paa.tasks4.oop.task5.entity;

import java.util.Objects;

public class Voucher {

    private final String tourType;
    private final String transport;
    private final String meal;
    private final int days;


    public Voucher(String tourType, String transport, String meal, int days) {
        this.tourType = tourType;
        this.transport = transport;
        this.meal = meal;
        this.days = days;
    }


    public String getTourType() {
        return tourType;
    }

    public String getTransport() {
        return transport;
    }

    public String getMeal() {
        return meal;
    }

    public int getDays() {
        return days;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Voucher voucher = (Voucher) o;
        return days == voucher.days &&
                Objects.equals(tourType, voucher.tourType) &&
                Objects.equals(transport, voucher.transport) &&
                Objects.equals(meal, voucher.meal);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tourType, transport, meal, days);
    }


    @Override
    public String toString() {
        return "Voucher{" +
                "tourType='" + tourType + '\'' +
                ", transport='" + transport + '\'' +
                ", meal='" + meal + '\'' +
                ", days=" + days +
                '}';
    }
}
