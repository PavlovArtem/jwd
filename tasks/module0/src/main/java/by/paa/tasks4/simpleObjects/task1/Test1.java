package by.paa.tasks4.simpleObjects.task1;


/**
 * 1. Создайте класс Test1 двумя переменными.
 * Добавьте метод вывода на экран и методы изменения этих переменных.
 * Добавьте метод, который находит сумму значений этих переменных,
 * и метод, который находит наибольшее значение из этих двух переменных.
 */
public class Test1 {

    private int firstVariable;
    private int secondVariable;

    public int getFirstVariable() {
        return firstVariable;
    }

    public void setFirstVariable(int firstVariable) {
        this.firstVariable = firstVariable;
    }

    public int getSecondVariable() {
        return secondVariable;
    }

    public void setSecondVariable(int secondVariable) {
        this.secondVariable = secondVariable;
    }

    public void showVariables() {
        System.out.println("Первая переменная = " + this.firstVariable + " Вторая переменная = " + this.secondVariable);
    }

    public int sumOfVariables() {
        return firstVariable + secondVariable;
    }

    public int biggestOfVariables() {
        return this.firstVariable > this.secondVariable ? firstVariable : secondVariable;
    }
}
