package by.paa.tasks4.simpleObjects.task4.dao;

import by.paa.tasks4.simpleObjects.task4.entity.Train;

import java.time.LocalTime;

public class TrainsInMemoryStorage {

    private Train[] trains;


    public TrainsInMemoryStorage() {
        init();
    }


    public Train[] getTrains() {
        return trains;
    }

    public void setTrains(Train[] trains) {
        this.trains = trains;
    }


    public void init(){
        Train t1 = new Train("Гродно", "1048А",LocalTime.of(15,0,0,0));
        Train t2 = new Train("Брест", "2014",LocalTime.of(20,30,0,0));
        Train t3 = new Train("Симферополь", "102C",LocalTime.of(21,30,0,0));
        Train t4 = new Train("Москва", "1048А",LocalTime.of(15,0,0,0));
        Train t5 = new Train("Гродно", "502А",LocalTime.of(11,30,0,0));

        this.trains = new Train[]{t1,t2,t3,t4,t5};
    }
}
