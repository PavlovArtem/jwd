package by.paa.tasks1.structures;

public interface MyList<T> {

    boolean add(T t);
    boolean remove(T t);

    int size();

    int indexOf(T t);


}
