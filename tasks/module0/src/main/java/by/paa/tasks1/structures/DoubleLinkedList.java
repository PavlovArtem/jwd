package by.paa.tasks1.structures;

public class DoubleLinkedList<T> implements MyList<T> {
    private int size;
    private LinkedNode<T> tail;
    private LinkedNode<T> head;


    @Override
    public boolean add(T t) {
        if (t == null) {
            return false;
        }


        if (head == null){
            head = new LinkedNode<>();
            head.value = t;
            tail = head;
        } else {
            LinkedNode<T> node = new LinkedNode<>();
            this.tail.next = node;
            node.previos = this.tail;
            node.value = t;

        }

        this.size++;
        return true;
    }

    @Override
    public boolean remove(T t) {

        LinkedNode<T> toRemove = tail;


        while (toRemove != null) {
            if (toRemove.value.equals(t)) {
                if (toRemove.previos != null) {
                    toRemove.previos.next = toRemove.next;
                    toRemove.next.previos = toRemove.previos;
                } else {
                    toRemove.next.previos = null;
                }
                this.size--;
                return true;
            } else {
                toRemove = toRemove.previos;
            }
        }
        return false;
    }


    @Override
    public int size() {
        return this.size;
    }


    @Override
    public int indexOf(T t) {
        return 0;
    }

    private class LinkedNode<T> {
        private T value;
        private LinkedNode<T> previos;
        private LinkedNode<T> next;
    }
}
