package by.paa.tasks1.structures;

public class MyStack<T> {
    private Element<T> topElement;
    private int length;


    public MyStack() {
        topElement = null;
        length = 0;
    }

    public void push(T data) {
        Element element = new Element();
        element.data = data;

        element.nextElement = topElement;
        topElement = element;

        length++;
    }

    public T pop() {
        T elementData = (T) topElement.data;
        topElement = topElement.nextElement;
        length--;

        return elementData;
    }


    public T peek() {
        return topElement.data;
    }


    public int size() {
        return length;
    }

    private class Element<T> {
        private T data;
        private Element nextElement;

        public Element() {
            data = null;
            nextElement = null;
        }
    }


}
