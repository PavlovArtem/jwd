package by.paa.tasks1.arrays;

public class ArraysTest {
    public static void main(String[] args) {
        MultidimensionalArrays multiArraySolutions = new MultidimensionalArrays();
        int[][] testMatrix = {{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}, {13, 14, 15, 16}};
        int[][] testMatrix5x5 = {{-1, 2, 3, 4, 1},
                {5, 6, -7, 8, 2},
                {9, -5, 11, 12, 9},
                {13, -6, 7, 1, 4},
                {7, -10, -5, 16, -3}};

        multiArraySolutions.taskOne();
        System.out.println("=========================");
        multiArraySolutions.taskTwo();
        System.out.println("=========================");
        multiArraySolutions.taskThree(testMatrix);
        System.out.println("=========================");
        multiArraySolutions.taskFour(testMatrix);
        System.out.println("=========================");
        multiArraySolutions.taskFive(testMatrix);
        System.out.println("=========================");
        multiArraySolutions.taskSix(testMatrix);
        System.out.println("=========================");
        multiArraySolutions.taskSeven(testMatrix5x5);
        System.out.println("=========================");
        multiArraySolutions.taskEight(testMatrix5x5);
        System.out.println("=========================");
        multiArraySolutions.taskNine(testMatrix5x5);
        System.out.println("=========================");
        multiArraySolutions.taskTen(3, 4, testMatrix5x5);
        System.out.println("=========================");
        multiArraySolutions.taskEleven(testMatrix);
        System.out.println("=========================");
        multiArraySolutions.taskTwelve(6);
        System.out.println("=========================");
        multiArraySolutions.taskThirteen(4);
        System.out.println("=========================");
        multiArraySolutions.taskFourteen(6);
        System.out.println("=========================");
        multiArraySolutions.taskFifteen(6);
        System.out.println("=========================");
        multiArraySolutions.taskSixteen(6);
        System.out.println("=========================");
        multiArraySolutions.taskSeventeen(6);
        System.out.println("=========================");
        multiArraySolutions.taskEighteen(6);
        System.out.println("=========================");
        multiArraySolutions.taskNineteen(6);


        System.out.println("*****************************");


        ArraysSolutions arraysSolutions = new ArraysSolutions();

        int[] basicIntArray = {1,2,3,4,5,6};
        double[] basicDoubleArray = {1d,2.5d,3d,3.5d,4d};


        arraysSolutions.taskOne(basicIntArray, 2);
        System.out.println("===============================");
        arraysSolutions.taskTwo();
        System.out.println("===============================");
        arraysSolutions.taskThree(0, 1, -1);
        System.out.println("===============================");
        arraysSolutions.taskFour(1, 2, 3, 3.5, 4);
        System.out.println("===============================");
        arraysSolutions.taskFive(0, 5, 0, 3);
        arraysSolutions.taskFive(1, 2, 3, 4);
        System.out.println("===============================");
        int[] testSix = {-1, 2, 4, 6};
        arraysSolutions.taskSix(testSix);
        System.out.println("===============================");
        arraysSolutions.taskSeven(5, 4, 5, 6, 8, 9, 3, 4);
        System.out.println("===============================");
        arraysSolutions.taskEight(-1, -2, 0, 5, 6, 1);
        System.out.println("===============================");
        arraysSolutions.taskNine(1, 2, 3, 4, 5, 6, 7, 2, 15, 0);
        System.out.println("===============================");
        arraysSolutions.taskTen(1, 1, 3, 4, 5, 2, 7, 5, 8);
        System.out.println("===============================");
        int[] testEleven = {1,2,3,4,5,6};
        arraysSolutions.taskEleven(2,testEleven );
        System.out.println("===============================");
        arraysSolutions.taskThirteen(2,2,6,basicIntArray);
        System.out.println("===============================");
        arraysSolutions.taskFourteen(basicIntArray);
        System.out.println("===============================");
        arraysSolutions.taskFifteen(basicDoubleArray,1.6d,3d);
        System.out.println("===============================");
        arraysSolutions.taskSeventeen(basicIntArray);
        System.out.println("===============================");
        arraysSolutions.taskTwenty(basicIntArray);
        System.out.println("===============================");
    }
}
