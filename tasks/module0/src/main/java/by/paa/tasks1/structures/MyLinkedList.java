package by.paa.tasks1.structures;

public class MyLinkedList<T> {
    private Node head;
    private int size;

    public MyLinkedList() {
        head = null;
        size = 0;
    }

    public void add(T data) {
        Node node = new Node(data);

        if (head == null) {
            head = node;

        } else {
            Node currentNode = head;

            while (currentNode.next != null) {
                currentNode = currentNode.getNext();
            }
            currentNode.next = node;
        }

        size++;
    }

    public void remove(T data) {
        Node currentNode = head;
        Node previousNode;
        if (currentNode.getData().equals(data)) {
            head = currentNode.next;
        } else {
            previousNode = null;
            while (!currentNode.getData().equals(data)) {
                previousNode = currentNode;
                currentNode = currentNode.next;
            }

            previousNode.next = currentNode.next;
        }
        size--;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public int indexOf(T data) {
        Node currentNode = head;
        int index = -1;

        while (currentNode != null) {
            index++;
            if (currentNode.getData().equals(data)) {
                return index;
            }
            currentNode = currentNode.next;
        }

        return -1;
    }

    public T elementAt(int index) {
        Node currendNode = head;
        int count = 0;
        while (count < index) {
            count++;
            currendNode = currendNode.next;
        }

        return (T) currendNode.getData();
    }



    public boolean addAt(int index, T data) {
        Node node = new Node(data);

        Node currentNode = head;
        Node previousNode = null;
        int currentIndex = 0;

        if (index > size || index < 0) {
            return false;
        }

        if (index == 0) {
            node.next = currentNode;
            head = node;

        } else {
            while (currentIndex < index) {
                currentIndex++;
                previousNode = currentNode;
                currentNode = currentNode.next;
            }
            node.next = currentNode;
            previousNode.next = node;
        }

        size++;
        return true;
    }

    public void removeAt(int index){
        Node currentNode = head;
        Node previousNode = null;
        int currentIndex = 0;

        if (index > size || index < 0) {
            return;
        }

        if (index == 0) {
            head = currentNode.next;
        } else {
            while (currentIndex < index) {
                currentIndex++;
                previousNode = currentNode;
                currentNode = currentNode.next;
            }

            previousNode.next = currentNode.next;
        }

        size--;

    }



    private class Node<T> {
        private T data;
        private Node next;

        public Node(T data) {
            this.data = data;
        }

        public T getData() {
            return data;
        }

        public void setData(T data) {
            this.data = data;
        }

        public void setNext(Node next) {
            this.next = next;
        }

        public Node getNext() {
            return next;
        }
    }
}
