package by.paa.tasks1.arrays;

import java.util.Arrays;

public class ArraysSolutions {


    /**
     * 1. В массив A [N] занесены натуральные числа.
     * Найти сумму тех элементов, которые кратны данному К.
     */
    public void taskOne(int[] inputArray, int k) {
        int sum = 0;

        for (int num : inputArray) {
            if (num % k == 0) {
                sum += num;
            }
        }

        System.out.println("Сумма элементов кратных числу K = " + sum);
    }


    /**
     * 2. В целочисленной последовательности есть нулевые элементы.
     * Создать массив из номеров этих элементов.
     */
    public void taskTwo(int... inputArr) {
        int[] zeroIndexesArr = new int[inputArr.length];

        for (int i = 0, j = 0; i < inputArr.length; i++) {
            if (inputArr[i] == 0) {
                zeroIndexesArr[j] = i;
                ++j;
            }
        }

        System.out.println(Arrays.toString(zeroIndexesArr));
    }

    /**
     * 3. Дана последовательность целых чисел а1 а2,..., аn .
     * Выяснить, какое число встречается раньше - положительное или
     * отрицательное.
     */
    public void taskThree(int... sequence) {

        for (int number : sequence) {
            if (number > 0) {
                System.out.println("positive first");
                break;
            } else if (number < 0) {
                System.out.println("negative first");
                break;
            }
        }
    }


    /**
     * 4. Дана последовательность действительных чисел а1 а2 ,..., аn .
     * Выяснить, будет ли она возрастающей.
     */
    public void taskFour(double... sequence) {

        for (int i = 1; i < sequence.length; i++) {
            if (sequence[i] < sequence[i - 1]) {
                System.out.println("Sequence is decrease!");
                return;
            }
        }
        System.out.println("Sequence is increase");
    }

    /**
     * 5. Дана последовательность натуральных чисел а1 , а2 ,..., ап.
     * Создать массив из четных чисел этой последовательности.
     * Если таких чисел нет, то вывести сообщение об этом факте.
     */
    public int[] taskFive(int... sequence) {
        int[] evenNums = new int[sequence.length];

        for (int i = 0, j = 0; i < sequence.length; i++) {
            if (sequence[i] % 2 == 0) {
                evenNums[j] = sequence[i];
                ++j;
            }
        }

        if (evenNums[0] == 0) {
            System.out.println("Even numbers not found in input sequence");
        }
        return evenNums;
    }


    /**
     * 6. Дана последовательность чисел а1 ,а2 ,..., ап.
     * Указать наименьшую длину числовой оси, содержащую все эти числа.
     */
    public void taskSix(int[] sequence) {
        if (sequence.length == 0) {
            System.out.println("Empty sequence");
        }
        int min = sequence[0];
        int max = sequence[0];

        for (int num : sequence) {
            if (max < num) {
                max = num;
            } else if (min > num) {
                min = num;
            }
        }

        System.out.println("Длинна оси составляет = " + (max - min));
    }

    /**
     * 7. Дана последовательность действительных чисел а1 ,а2 ,..., ап.
     * Заменить все ее члены, большие данного Z, этим числом.
     * Подсчитать количество замен.
     */
    public int[] taskSeven(int z, int... sequence) {
        int[] resultSeq = new int[sequence.length];
        int replaceCounter = 0;

        for (int i = 0; i < sequence.length; i++) {
            if (sequence[i] > z) {
                sequence[i] = z;
                ++replaceCounter;
            }
        }

        System.out.println("Count of replaces with Z: " + replaceCounter);

        return resultSeq;
    }


    /**
     * 8. Дан массив действительных чисел, размерность которого N.
     * Подсчитать, сколько в нем отрицательных,
     * положительных и нулевых элементов.
     */
    public void taskEight(double... inArr) {
        int negativeCounter = 0;
        int positiveCounter = 0;
        int zeroCounter = 0;

        for (double num : inArr) {
            if (num > 0) {
                ++positiveCounter;
            } else if (num < 0) {
                ++negativeCounter;
            } else {
                ++zeroCounter;
            }
        }

        System.out.println("Positive numbers: " + positiveCounter);
        System.out.println("Negative numbers: " + negativeCounter);
        System.out.println("Zero numbers: " + zeroCounter);
    }


    /**
     * 9. Даны действительные числа а1 ,а2 ,..., аn . Поменять местами наибольший и наименьший элементы.
     */
    public void taskNine(double... inArr) {
        int lowestNumIndex = 0;
        int highestNumIndex = 0;

        for (int i = 0; i < inArr.length; i++) {
            if (inArr[i] > inArr[highestNumIndex]) {
                highestNumIndex = i;
            } else if (inArr[i] < inArr[lowestNumIndex]) {
                lowestNumIndex = i;
            }
        }

        double tmp = 0;
        tmp = inArr[lowestNumIndex];
        inArr[lowestNumIndex] = inArr[highestNumIndex];
        inArr[highestNumIndex] = tmp;

        System.out.println(Arrays.toString(inArr));
    }


    /**
     * 10. Даны целые числа а1 ,а2 ,..., аn .
     * Вывести на печать только те числа, для которых аi > i.
     */
    public void taskTen(int... inputArr) {

        final String message = "input array: " + Arrays.toString(inputArr);
        System.out.println(message);
        System.out.print("output: ");

        for (int i = 0; i < inputArr.length; i++) {
            if (inputArr[i] > i) {
                System.out.print(inputArr[i] + " , ");
            }
        }
    }


    /**
     * 11. Даны натуральные числа а1 ,а2 ,..., аn .
     * Указать те из них,
     * у которых остаток от деления на М равен L (0 < L < М-1).
     */
    public void taskEleven(int m, int... array) {
        int L = 0;
        for (int num : array) {
            L = num % m;
            if (L > 0 && L < (m - 1)) {
                System.out.println(L);
            }
        }
    }


    /**
     * 13. Определить количество элементов последовательности натуральных чисел, кратных числу М и заключенных в
     * промежутке от L до N.
     */
    public void taskThirteen(int M, int L, int N, int[] inputArr) {
        if (L < 0 || N > inputArr.length - 1) {
            System.out.println("Incorrect input range");
            return;
        }

        //счетчик кратных элементов
        int counter = 0;

        for (int i = L; i < N; i++) {
            if (inputArr[i] % M == 0) {
                ++counter;
            }
        }

        System.out.println("Количество кратных элементов в промежутке = " + counter);
    }

    /**
     * 14. Дан одномерный массив A[N]. Найти:
     * max( a2,a4 ,... ,a2k ) min( a1,a3 ,... ,a2k+1 )
     */
    public void taskFourteen(int... inputArr) {
        int maxOfEvenIndexEl = inputArr[0];
        int minOfOddIndexEl = inputArr[0];

        for (int i = 0; i < inputArr.length; i++) {
            if ((i + 1) % 2 == 0) {
                if (maxOfEvenIndexEl < inputArr[i]) {
                    maxOfEvenIndexEl = inputArr[i];
                }
            } else {
                if (minOfOddIndexEl > inputArr[i]) {
                    minOfOddIndexEl = inputArr[i];
                }
            }
        }

        System.out.println("max = " + maxOfEvenIndexEl + " min = " + minOfOddIndexEl);
    }


    /**
     * 15. Дана последовательность действительных чисел n
     * Указать те ее элементы, которые принадлежат отрезку
     * [с, d].
     */
    public void taskFifteen(double[] sequence, double c, double d) {


        System.out.println(String.format("elements belong to range [%f,%f]", c, d));

        for (int i = 0; i < sequence.length; i++) {
            if (sequence[i] >= c && sequence[i] <= d) {
                System.out.print(sequence[i] + " ");
            }
        }
    }

    /**
     *
     */
    public void taskSixteen() {

    }


    /**
     * 17. Дана последовательность целых чисел.
     * Образовать новую последовательность, выбросив из исходной
     * те члены, которые равны min( , , , ) .
     */
    public void taskSeventeen(int... sequence) {
        if (sequence.length == 0) {
            System.out.println("массив пуст");
            return;
        }

        int minNumber = sequence[0];
        int[] resultArray = new int[sequence.length];

        for (int i = 0; i < sequence.length; i++) {
            if (minNumber < sequence[i]) {
                minNumber = sequence[i];
            }
        }

        for (int i = 0; i < sequence.length; i++) {
            if (sequence[i] > minNumber) {
                resultArray[i] = sequence[i];
            }
            System.out.println(resultArray[i] + " ");
        }
    }


    /**
     * 18*. «Суперзамок». Секретный замок для сейфа состоит из 10 расположенных в рад ячеек, в которые надо вставить
     * игральные кубики. Но дверь открывается только в том случае, когда в любых трех соседних ячейках сумма точек на
     * передних гранях кубиков равна 10. (Игральный кубик имеет на каждой грани от 1 до 6 точек). Напишите программу,
     * которая разгадывает код замка при условии, что два кубика уже вставлены в ячейки.
     */
    public void taskEighteen() {


    }


    /**
     * 19. В массиве целых чисел с количеством элементов
     * n найти наиболее часто встречающееся число. Если таких чисел
     * несколько, то определить наименьшее из них.
     */
    public void taskNineteen() {

    }


    /**
     * 20. Дан целочисленный массив с количеством элементов п.
     * Сжать массив, выбросив из него каждый второй элемент
     * (освободившиеся элементы заполнить нулями).
     * Примечание. Дополнительный массив не использовать.
     */
    public void taskTwenty(int[] array) {
        int secondElementFlag = 1;

        for (int i = 0; i < array.length; i++) {
            if (secondElementFlag == 1) {
                secondElementFlag++;
            } else if (secondElementFlag == 2) {
                array[i] = 0;
                secondElementFlag = 1;
            }

            System.out.println(array[i] + " ");
        }
    }


}
