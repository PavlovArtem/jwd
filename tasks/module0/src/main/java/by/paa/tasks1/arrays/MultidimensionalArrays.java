package by.paa.tasks1.arrays;


public class MultidimensionalArrays {


    /**
     * 1. Cоздать матрицу 3 x 4,
     * заполнить ее числами 0 и 1 так,
     * чтобы в одной строке была ровно одна единица,
     * и вывести на экран.
     */
    public void taskOne() {
        int[][] matrix = new int[3][4];

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (j == 0) {
                    matrix[i][j] = 1;
                }
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
    }


    /**
     * 2. Создать и вывести на экран матрицу 2 x 3,
     * заполненную случайными числами из [0, 9].
     */
    public void taskTwo() {
        int[][] matrix = new int[2][3];

        final int max = 9;
        final int min = 0;
        final int range = max - min;


        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                matrix[i][j] = (int) (Math.random() * range);
            }
        }

        showMatrix(matrix);
    }


    /**
     * 3. Дана матрица.
     * Вывести на экран первый и последний столбцы.
     * 1  2  3  4
     * 5  6  7  8
     * 9 10 11 12
     * 13 14 15 16
     */
    public void taskThree(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (j == 0 || j == matrix[i].length - 1) {
                    System.out.print(matrix[i][j]);
                } else {
                    System.out.print(" . ");
                }
            }
            System.out.println();
        }
    }

    /**
     * 4. Дана матрица.
     * Вывести на экран первую и последнюю строки.
     * 1  2  3  4
     * 5  6  7  8
     * 9 10 11 12
     * 13 14 15 16
     *
     * @param matrix
     */
    public void taskFour(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            if (i == 0 || i == matrix.length - 1) {
                for (int j = 0; j < matrix[i].length; j++) {
                    System.out.print(matrix[i][j] + " ");
                }
            }
            System.out.println();
        }
    }


    /**
     * 5. Дана матрица.
     * Вывести на экран все четные строки,
     * то есть с четными номерами.
     * 1  2  3  4
     * 5  6  7  8
     * 9 10 11 12
     * 13 14 15 16
     */
    public void taskFive(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            if ((i + 1) % 2 == 0) {
                for (int j = 0; j < matrix[i].length; j++) {
                    System.out.print(matrix[i][j] + " ");
                }
            }
            System.out.println();
        }
    }

    /**
     * 6. Дана матрица.
     * Вывести на экран все нечетные столбцы,
     * у которых первый элемент больше последнего.
     *
     * @param matrix
     */
    public void taskSix(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            if ((i + 1) % 2 != 0) {
                for (int j = 0; j < matrix[i].length; j++) {
                    System.out.print(matrix[i][j] + " ");
                }
            }
            System.out.println();
        }
    }


    /**
     * 7. Дан двухмерный массив 5×5.
     * Найти сумму модулей отрицательных нечетных элементов.
     */
    public void taskSeven(int[][] matrix) {

        int result = 0;

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (matrix[i][j] < 0 && Math.abs(matrix[i][j]) % 2 != 0) {
                    result += Math.abs(matrix[i][j]);
                }
            }
        }

        System.out.println("Task 7 result = " + result);
    }


    /**
     * 8. Дан двухмерный массив n×m элементов.
     * Определить, сколько раз встречается число 7 среди элементов
     * массива.
     */
    public void taskEight(int[][] matrix) {
        int concreteNumber = 7;
        int numberCounter = 0;

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (matrix[i][j] == concreteNumber) {
                    ++numberCounter;
                }
            }
        }

        System.out.println("Number " + concreteNumber + " found " + numberCounter + " times ");

    }

    /**
     * 9. Дана квадратная матрица.
     * Вывести на экран элементы, стоящие на диагонали.
     */
    public void taskNine(int[][] matrix) {

        if (matrix.length != matrix[0].length) {
            return;
        }

        int leftTopCorner = 0;
        int rightTopCorner = matrix[0].length - 1;

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (j == leftTopCorner || j == rightTopCorner) {
                    System.out.print(matrix[i][j]);
                } else {
                    System.out.print(" . ");
                }
            }
            ++leftTopCorner;
            --rightTopCorner;
            System.out.println();
        }

    }


    /**
     * 10. Дана матрица.
     * Вывести k-ю строку и p-й столбец матрицы.
     */
    public void taskTen(int k, int p, int[][] matrix) {

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (i == k - 1 || j == p - 1) {
                    System.out.print(matrix[i][j] + " ");
                } else {
                    System.out.print(". ");
                }
            }
            System.out.println();
        }
    }

    /**
     * 11. Дана матрица размера m x n.
     * Вывести ее элементы в следующем порядке:
     * первая строка справа налево, вторая
     * строка слева направо, третья строка справа налево
     * и так далее.
     */
    public void taskEleven(int[][] matrix) {

        for (int i = 0; i < matrix.length; i++) {
            if ((i + 1) % 2 != 0) {
                for (int j = matrix[i].length - 1; j >= 0; j--) {
                    System.out.print(matrix[i][j] + " ");
                }
            } else {
                for (int j = 0; j < matrix[i].length; j++) {
                    System.out.print(matrix[i][j] + " ");
                }
            }
            System.out.println();
        }
    }

    /**
     * 12. Получить квадратную матрицу порядка n:
     * n[i] +1
     */
    public void taskTwelve(int n) {
        int[][] squareMatrix = new int[n][n];

        for (int i = 0; i < squareMatrix.length; i++) {
            squareMatrix[i][i] = i;
        }

        showMatrix(squareMatrix);
    }


    /**
     * 13. Сформировать квадратную
     * матрицу порядка n по заданному образцу(n - четное):
     */
    public void taskThirteen(int n) {
        if ((n % 2) != 0) {
            System.out.println("Input n is odd!");
            return;
        }

        int[][] matrix = new int[n][n];

        for (int i = 0; i < matrix.length; i++) {
            int reverse = n;
            for (int j = 0; j < matrix[i].length; j++) {
                if ((i + 1) % 2 == 0) {
                    matrix[i][j] = reverse;
                    --reverse;
                } else {
                    matrix[i][j] = j + 1;
                }
            }
        }
        showMatrix(matrix);
    }


    /**
     * 14. Сформировать квадратную
     * матрицу порядка n по заданному образцу(n - четное):
     */
    public void taskFourteen(int n) {
        if ((n % 2) != 0) {
            System.out.println("Input n is odd!");
            return;
        }

        int[][] squareMatrix = new int[n][n];
        int j = n - 1;

        for (int i = 0; i < squareMatrix.length; i++) {
            squareMatrix[i][j] = i + 1;
            --j;
        }

        showMatrix(squareMatrix);
    }


    /**
     * 15. Сформировать квадратную
     * матрицу порядка n по заданному образцу(n - четное):
     */
    public void taskFifteen(int n) {
        if ((n % 2) != 0) {
            System.out.println("Input n is odd!");
            return;
        }

        int[][] matrix = new int[n][n];
        int j = n;

        for (int i = 0; i < matrix.length; i++) {
            matrix[i][i] = j;
            --j;
        }
        showMatrix(matrix);
    }


    /**
     * 16. Сформировать квадратную
     * матрицу порядка n по заданному образцу(n - четное):
     */
    public void taskSixteen(int n) {
        if ((n % 2) != 0) {
            System.out.println("Input n is odd!");
            return;
        }

        int[][] squareMatrix = new int[n][n];
        int j = 1;

        for (int i = 0; i < n; i++) {
            squareMatrix[i][i] = j * ++j;
        }

        showMatrix(squareMatrix);
    }


    /**
     * 17. Сформировать квадратную матрицу порядка n по заданному образцу(n - четное):
     *
     * @param n
     */
    public void taskSeventeen(int n) {

        if ((n % 2) != 0) {
            System.out.println("Input n is odd!");
            return;
        }

        int[][] squareMatrix = new int[n][n];

        for (int i = 0; i < squareMatrix.length; i++) {
            for (int j = 0; j < squareMatrix[i].length; j++) {
                if (i == 0 || i == squareMatrix.length - 1) {
                    squareMatrix[i][j] = 1;
                } else if (j == 0 || j == squareMatrix[i].length - 1) {
                    squareMatrix[i][j] = 1;
                }
            }
        }

        showMatrix(squareMatrix);
    }

    /**
     * 18. Сформировать квадратную матрицу
     * порядка n по заданному образцу(n - четное):
     *
     * @param n
     */
    public void taskEighteen(int n) {
        if ((n % 2) != 0) {
            System.out.println("Input n is odd!");
            return;
        }

        int[][] squareMatrix = new int[n][n];

        for (int i = 0; i < squareMatrix.length; i++) {
            for (int j = 0; j < squareMatrix[i].length - i; j++) {
                squareMatrix[i][j] = i + 1;
            }
        }

        showMatrix(squareMatrix);
    }


    /**
     * 19. Сформировать квадратную
     * матрицу порядка n по заданному образцу(n - четное):
     */
    public void taskNineteen(int n) {
        if ((n % 2) != 0) {
            System.out.println("Input n is odd!");
            return;
        }

        int[][] squareMatrix = new int[n][n];
        int rowStart = 0;
        int rowEnd = squareMatrix.length;

        boolean reverse = false;


        for (int i = 0; i < squareMatrix.length; i++) {
            if (rowStart == rowEnd){
                reverse = true;
                --rowStart;
                ++rowEnd;
            }
            for (int j = rowStart; j < rowEnd; j++) {
                squareMatrix[i][j] = 1;
            }

            if (reverse){
                --rowStart;
                ++rowEnd;
            } else {
                ++rowStart;
                --rowEnd;
            }
        }

        showMatrix(squareMatrix);

    }


    /**
     * print matrix into console
     *
     * @param matrix
     */
    private static void showMatrix(int[][] matrix) {
        for (int[] ints : matrix) {
            for (int anInt : ints) {
                System.out.print(anInt + " ");
            }
            System.out.println();
        }
    }


}
