
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="by.pavlov.drugsapp.command.CommandType" %>
<%@ page import="by.pavlov.drugsapp.application.ApplicationConstants" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page errorPage="/jsp/error.jsp" %>
<html>
<head>
    <title>Medicines title</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/bulma.css">
</head>
<body>

<section class="hero is-dark">
    <div class="hero-head">

    </div>
    <div class="hero-body">
        <div class="container">
            <h1 class="title">
                Medicines
            </h1>
            <h2 class="subtitle">
                Medicines upload & download app
            </h2>
        </div>
    </div>
</section>
<section class="is-fullheight is-medium">
    <div class="columns">
        <div class="column is-one-quarter">
            <aside class="menu">
                <p class="menu-label">
                    NAVIGATION
                </p>
                <ul class="menu-list">
                    <li>
                        <a href="?${ApplicationConstants.COMMAND_PARAM}=${CommandType.DISPLAY_MEDICINES}">
                            SHOW MEDICINES
                        </a>
                    </li>
                    <li>
                        <a href="?${ApplicationConstants.COMMAND_PARAM}=${CommandType.DOWNLOAD_XML}">
                            DOWNLOAD XML
                        </a>
                    </li>
                </ul>
            </aside>
        </div>
        <div class="column is-three-quarters">
            <div class="container">
                <div class="content">
                    <jsp:include page="view/${viewName}.jsp"/>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="footer">
    <div class="content has-text-centered">
        <p>
            THANKS
        </p>
    </div>
</section>
</body>
</html>
