<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <title>Something</title>
</head>
<body>
<div class="Upload">
    <form action="upload" method="post" enctype="multipart/form-data">
        <label>Please upload file:
        <input type="file" name="uploadFile">
    </label>
        <p>Choose parser</p>
        <p><input type="radio" id="parser1" name="parser" value="DOM_PARSER"><label for="parser1"> DOMParser </label>
            <input type="radio" id="parser2" name="parser" value="SAX_PARSER" checked ><label for="parser2">SAXParser</label>
            <input type="radio" id="parser3" name="parser" value="STAX_PARSER"><label for="parser3">StAXParser</label>

            <input type="hidden" name="_command" value="UPLOAD_FILE">
            <button>upload</button>
    </form>
</div>
<div>
    <h2>Medicine table</h2>
    <table>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Pharm</th>
            <th>Group</th>
            <th>Analogs</th>
            <th>Versions</th>
        </tr>
        <c:if test="${not empty medicines}">
            <c:forEach items="${medicines}" var="medicine" >
                <tr>
                    <td>${medicine.id}</td>
                    <td>${medicine.name}</td>
                    <td>
                        <ul>
                            <li>${medicine.pharm.name}</li>
                            <li>${medicine.pharm.country}</li>
                        </ul>
                    </td>
                    <td>${medicine.group.name()}</td>
                    <td> <c:forEach items="${medicine.analogNames}" var="name" >
                            <ul>
                                <li>${name}</li>
                            </ul>
                         </c:forEach>
                    </td>
                    <td>
                        <c:forEach items="${medicine.medicineVersions}" var="version" >
                            <ul>
                                <li>${version}</li>
                            </ul>
                        </c:forEach>
                    </td>

                </tr>
            </c:forEach>
        </c:if>
    </table>
</div>

</body>
</html>