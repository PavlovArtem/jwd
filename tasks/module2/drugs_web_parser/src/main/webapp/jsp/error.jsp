<%@ page isErrorPage="true" %>

<!DOCTYPE html>
<html>

<head>
    <title>ERROR</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/bulma.css">
</head>

<body>
    <section class="hero is-danger">
        <div class="hero-body">
            <div class="container">
                <h1 class="title">
                    Medicines upload & download
                </h1>
                <h2 class="subtitle">
                    SOMETHING WENT WRONG
                </h2>
            </div>
        </div>
    </section>
    <section class="section">
        <div class="container">
            <h1 class="title">SORRY AN EXCEPTION OCCURED</h1>
            <h2 class="subtitle">
                Exception is: <%= exception %>
            </h2>
        </div>
    </section>
</body>

</html>