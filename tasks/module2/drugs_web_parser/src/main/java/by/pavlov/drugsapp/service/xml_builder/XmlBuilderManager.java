package by.pavlov.drugsapp.service.xml_builder;

public interface XmlBuilderManager<T> {

    XmlStringBuilder createXmlBuilder(T entity);


}
