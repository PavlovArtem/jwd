package by.pavlov.drugsapp.parser;

public interface ParserProvider {

    void register(ParserType parserType, XmlParser xmlParser);

    void remove(ParserType parserType);

    XmlParser getParser(ParserType parserType);


}
