package by.pavlov.drugsapp.entity;

import java.util.Date;
import java.util.Objects;

public class Certificate {
    private String number;
    private Date issueDate;
    private Date expiredDate;
    private String certificationOrganization;

    public Certificate(String number, Date issueDate, Date expiredDate, String certificationOrganization) {
        this.number = number;
        this.issueDate = issueDate;
        this.expiredDate = expiredDate;
        this.certificationOrganization = certificationOrganization;
    }

    public Certificate() {
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Date getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    public Date getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(Date expiredDate) {
        this.expiredDate = expiredDate;
    }

    public String getCertificationOrganization() {
        return certificationOrganization;
    }

    public void setCertificationOrganization(String certificationOrganization) {
        this.certificationOrganization = certificationOrganization;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Certificate that = (Certificate) o;
        return Objects.equals(number, that.number) &&
                Objects.equals(issueDate, that.issueDate) &&
                Objects.equals(expiredDate, that.expiredDate) &&
                Objects.equals(certificationOrganization, that.certificationOrganization);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, issueDate, expiredDate, certificationOrganization);
    }

    @Override
    public String toString() {
        return "Certificate{" +
                "number='" + number + '\'' +
                ", issueDate=" + issueDate +
                ", expiredDate=" + expiredDate +
                ", certificationOrganization='" + certificationOrganization + '\'' +
                '}';
    }
}
