package by.pavlov.drugsapp.service.xml_builder;

import java.util.ArrayList;
import java.util.List;

public class MedicinesListXmlBuilder extends AbstractXmlStringBuilder {

    private final List<XmlStringBuilder> childBuilders = new ArrayList<>();


    @Override
    public void addChild(XmlStringBuilder child) {
        childBuilders.add(child);
    }

    @Override
    public String build() {
        StringBuilder resultXml = new StringBuilder();

        resultXml.append("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n")
                .append(addOpenTag("Medicines"));


        resultXml.append(buildChild(childBuilders));

        resultXml.append("</Medicines");


        return resultXml.toString();
    }
}
