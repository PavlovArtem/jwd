package by.pavlov.drugsapp.entity;

import java.math.BigDecimal;
import java.util.Objects;

public class Packaging {
    private PackageType packageType;
    private Units units;
    private Double quantity;
    private BigDecimal price;

    public Packaging(PackageType packageType, Units units, Double quantity, BigDecimal price) {
        this.packageType = packageType;
        this.units = units;
        this.quantity = quantity;
        this.price = price;
    }


    public Packaging() {
    }


    public PackageType getPackageType() {
        return packageType;
    }

    public void setPackageType(PackageType packageType) {
        this.packageType = packageType;
    }

    public Units getUnits() {
        return units;
    }

    public void setUnits(Units units) {
        this.units = units;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Packaging packaging = (Packaging) o;
        return packageType == packaging.packageType &&
                units == packaging.units &&
                Objects.equals(quantity, packaging.quantity) &&
                Objects.equals(price, packaging.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(packageType, units, quantity, price);
    }


    @Override
    public String toString() {
        return "Packaging{" +
                "packageType=" + packageType +
                ", units=" + units +
                ", quantity=" + quantity +
                ", price=" + price +
                '}';
    }


}
