package by.pavlov.drugsapp.command;


import by.pavlov.drugsapp.application.ApplicationConstants;

import javax.servlet.http.HttpServletRequest;

public class CommandUtil {

    public static String getCommandFromRequest(HttpServletRequest request) {

        return request.getAttribute(ApplicationConstants.COMMAND_PARAM) != null ? String.valueOf(request.getAttribute(
                ApplicationConstants.COMMAND_PARAM)) :
                request.getParameter(ApplicationConstants.COMMAND_PARAM);
    }
}
