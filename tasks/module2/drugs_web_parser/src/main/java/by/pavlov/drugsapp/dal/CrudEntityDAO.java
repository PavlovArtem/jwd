package by.pavlov.drugsapp.dal;

import java.util.List;

public interface CrudEntityDAO<ENTITY, KEY> {

    List<ENTITY> findAll();

    void save(ENTITY entity);

    void update(KEY key, ENTITY entity);

    ENTITY get(KEY key);

    void delete(KEY key);

}
