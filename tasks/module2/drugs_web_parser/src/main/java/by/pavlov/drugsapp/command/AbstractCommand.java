package by.pavlov.drugsapp.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public abstract class AbstractCommand implements Command{

    private final Logger logger = LogManager.getLogger(AbstractCommand.class);

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) {
        try{
            executeWrapped(req, resp);
        } catch (Exception e){
            logger.error(e.getMessage(),e);
        }

    }

    protected abstract void executeWrapped(HttpServletRequest req, HttpServletResponse resp) throws Exception;


    protected void forward(HttpServletRequest req, HttpServletResponse resp, String viewName) {

        try {
            req.setAttribute("viewName", viewName);
            req.getRequestDispatcher("/jsp/main_layout.jsp").forward(req, resp);
        } catch (Exception e) {
            throw new IllegalStateException("Failed to forward view", e);
        }
    }

    protected void redirect(HttpServletResponse resp, String redirect) {
        try {
            resp.sendRedirect(redirect);
        } catch (IOException e) {
            throw new IllegalStateException("Failed to redirect", e);
        }
    }
}
