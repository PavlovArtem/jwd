package by.pavlov.drugsapp.entity;

import java.util.Objects;

public class Pills extends MedicineVersion {

    private String shape;
    private String color;


    public Pills(String versionName, Certificate certificate, Packaging packaging, Dosage dosage, String shape, String color) {
        super(versionName, certificate, packaging, dosage);
        this.shape = shape;
        this.color = color;
    }

    public Pills() {
    }

    public String getShape() {
        return shape;
    }

    public void setShape(String shape) {
        this.shape = shape;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Pills pills = (Pills) o;

        if (!Objects.equals(shape, pills.shape)) return false;
        return Objects.equals(color, pills.color);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (shape != null ? shape.hashCode() : 0);
        result = 31 * result + (color != null ? color.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return  super.toString() +
                "Pills{" +
                "shape='" + shape + '\'' +
                ", color='" + color + '\'' +
                '}';
    }
}
