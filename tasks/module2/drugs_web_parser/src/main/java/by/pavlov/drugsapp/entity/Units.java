package by.pavlov.drugsapp.entity;

public enum  Units {
    MILLIGRAMS("mg"),
    GRAMS("g"),
    LITRE("l"),
    MILLILITRES("ml"),
    PIECE("pc");


    private final String shortDescription;

    Units(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getShortDescription(){
        return shortDescription;
    }


}
