package by.pavlov.drugsapp.service.xml_builder;

import java.util.List;

public abstract class AbstractXmlStringBuilder implements XmlStringBuilder {


    @Override
    public abstract String build();

    protected String addElementWithAttributes(String tagName, String content, String... attrs) {
        String openTag = "<" + tagName;
        StringBuilder attributes = new StringBuilder();
        for (String attr : attrs) {
            attributes.append("  " + attr + " ");
        }
        attributes.append(">\n");
        String closeTag = "</" + tagName + ">\n";


        return openTag +
                attributes +
                content +
                "\n" +
                closeTag;
    }

    protected String addElement(String tagName, String content) {

        String openTag = "\t<" + tagName + ">\n";
        String closeTag = "\t</" + tagName + ">\n";

        StringBuilder element = new StringBuilder();
        element.append(openTag)
                .append("\t\t")
                .append(content)
                .append("\n")
                .append(closeTag);

        return element.toString();
    }

    protected String addElements(String... elements) {

        StringBuilder elementsToAdd = new StringBuilder();

        for (String element : elements) {
            elementsToAdd.append(element);
        }

        return elementsToAdd.toString();

    }


    protected String addOpenTag(String tagName) {
        return "<" + tagName + ">\n";
    }

    protected String addOpenTagWithAttrs(String tagName, String... attrs) {
        String openTag = "<" + tagName;
        StringBuilder attributes = new StringBuilder();
        for (String attr : attrs) {
            attributes.append("  " + attr + " ");
        }
        attributes.append(">\n");

        return openTag +
                attributes;

    }

    protected String addCloseTag(String tagName) {
        return "</" + tagName + ">\n";
    }

    protected String buildChild(List<XmlStringBuilder> builders) {
        StringBuilder xmlFragment = new StringBuilder();

        for (XmlStringBuilder builder : builders) {
            xmlFragment.append("\t");
            xmlFragment.append(builder.build());
            //xmlFragment.append("\t");
        }

        return xmlFragment.toString();
    }


}
