package by.pavlov.drugsapp.service.xml_builder;

import by.pavlov.drugsapp.entity.Group;

public class GroupXmlBuilder extends AbstractXmlStringBuilder {

    private final Group group;

    public GroupXmlBuilder(Group group) {
        this.group = group;
    }


    @Override
    public String build() {
        return addElement("Group", group.name());
    }
}
