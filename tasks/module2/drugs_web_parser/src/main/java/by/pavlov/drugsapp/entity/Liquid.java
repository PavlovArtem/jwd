package by.pavlov.drugsapp.entity;

import java.util.Objects;

public class Liquid extends MedicineVersion {

    private String consistence;

    public Liquid(String versionName, Certificate certificate, Packaging packaging, Dosage dosage, String consistence) {
        super(versionName, certificate, packaging, dosage);
        this.consistence = consistence;
    }

    public Liquid() {
    }


    public String getConsistence() {
        return consistence;
    }

    public void setConsistence(String consistence) {
        this.consistence = consistence;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Liquid liquid = (Liquid) o;
        return Objects.equals(consistence, liquid.consistence);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), consistence);
    }

    @Override
    public String toString() {
        return "Liquid{" +
                "consistence='" + consistence + '\'' +
                '}';
    }
}
