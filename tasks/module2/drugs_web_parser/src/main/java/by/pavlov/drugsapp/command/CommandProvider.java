package by.pavlov.drugsapp.command;

public interface CommandProvider {

    void register(CommandType commandType,Command command);

    void remove(CommandType commandType);

    Command getCommand(CommandType commandType);


}
