package by.pavlov.drugsapp.parser;

import by.pavlov.drugsapp.entity.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class StAXParserMed implements XmlParser {

    private static final Logger logger = LogManager.getLogger(StAXParserMed.class);

    private final List<Medicine> medicineList = new ArrayList<>();
    //Medicine
    private Medicine.Builder builder;
    private String name;
    private Pharm pharm;
    private String pharmName;
    private String country;
    private Group group;
    private List<String> analogs;
    private List<MedicineVersion> medicineVersions;
    //Medicine
    //MedicineVersion
    private MedicineVersion medVersion;
    private String medicineType;
    //certificate
    private Certificate certificate;
    private String cerNumber;
    private Date issueDate;
    private Date expiredDate;
    private String cerOrg;
    //----------
    //packaging
    private Packaging packaging;
    private PackageType packageType;
    private Units units;
    private Double qty;
    private BigDecimal price;
    //-----------
    //dosage
    private Dosage dosage;
    private Units dosageUnits;
    private Double dosageQty;
    private String usageDescription;

    private String content;


    @Override
    public List parse(InputStream stream) throws XmlParseException {


        XMLInputFactory factory = XMLInputFactory.newInstance();

        try {
            XMLStreamReader reader = factory.createXMLStreamReader(stream);
            while (reader.hasNext()) {
                int event = reader.next();

                switch (event) {
                    case XMLStreamConstants.START_ELEMENT:
                        startElement(reader);
                        break;
                    case XMLStreamConstants.END_ELEMENT:
                        endElement(reader);
                        break;
                    case XMLStreamConstants.CHARACTERS:
                        content = reader.getText().trim();
                        break;
                }

            }


        } catch (XMLStreamException e) {
            logger.error("Error in StAXparser", e);
            throw new XmlParseException(e);
        }


        return medicineList;
    }


    private void startElement(XMLStreamReader reader) {
        if ("Medicine".equalsIgnoreCase(reader.getLocalName())) {
            builder = new Medicine.Builder();
            String attrValue = reader.getAttributeValue("", "id");
            builder.id(Long.parseLong(attrValue));
        }
        if ("Pharm".equalsIgnoreCase(reader.getLocalName())) {
            pharm = new Pharm();
        }
        if ("Analogs".equalsIgnoreCase(reader.getLocalName())) {
            analogs = new ArrayList<>();
        }
        if ("Versions".equalsIgnoreCase(reader.getLocalName())) {
            medicineVersions = new ArrayList<>();
        }
        //med versions
        if ("Pills".equalsIgnoreCase(reader.getLocalName())) {
            Pills pills = new Pills();
            String colorAttr = reader.getAttributeValue("", "color");
            String shapeAttr = reader.getAttributeValue("", "shape");
            pills.setColor(colorAttr);
            pills.setShape(shapeAttr);
            medVersion = pills;
        }
        if ("Liquid".equalsIgnoreCase(reader.getLocalName())) {
            Liquid liquid = new Liquid();
            String consistence = reader.getAttributeValue("", "consistence");
            liquid.setConsistence(consistence);
        }
        //------------
        if ("Certificate".equalsIgnoreCase(reader.getLocalName())) {
            certificate = new Certificate();
        }
        if ("Packaging".equalsIgnoreCase(reader.getLocalName())) {
            packaging = new Packaging();
        }
        if ("Dosage".equalsIgnoreCase(reader.getLocalName())) {
            dosage = new Dosage();
        }


    }


    private void endElement(XMLStreamReader reader) throws XMLStreamException {
        if ("Name".equalsIgnoreCase(reader.getLocalName())) {
            builder.name(name);
        }
        if ("Pharm".equalsIgnoreCase(reader.getLocalName())) {
            pharm.setName(pharmName);
            pharm.setCountry(country);
            builder.pharm(pharm);
        }
        if ("Group".equalsIgnoreCase(reader.getLocalName())) {
            builder.group(group);
        }
        if ("Analogs".equalsIgnoreCase(reader.getLocalName())) {
            builder.analogNames(analogs);
        }
        if ("Certificate".equalsIgnoreCase(reader.getLocalName())) {
            certificate.setNumber(cerNumber);
            certificate.setIssueDate(issueDate);
            certificate.setExpiredDate(expiredDate);
            certificate.setCertificationOrganization(cerOrg);
            medVersion.setCertificate(certificate);
        }
        if ("Packaging".equalsIgnoreCase(reader.getLocalName())) {
            packaging.setPackageType(packageType);
            packaging.setUnits(units);
            packaging.setQuantity(qty);
            packaging.setPrice(price);
            medVersion.setPackaging(packaging);
        }
        if ("Dosage".equalsIgnoreCase(reader.getLocalName())) {
            dosage.setQuantity(dosageQty);
            dosage.setUnits(dosageUnits);
            dosage.setUsageDescription(usageDescription);
            medVersion.setDosage(dosage);
        }
        if ("Pills".equalsIgnoreCase(reader.getLocalName())
                || "Liquid".equalsIgnoreCase(reader.getLocalName())) {
            medVersion.setVersionName(medicineType);
            medicineVersions.add(medVersion);
        }
        if ("Versions".equalsIgnoreCase(reader.getLocalName())) {
            builder.medicineVersion(medicineVersions);
        }


        if ("Medicine".equalsIgnoreCase(reader.getLocalName())) {
            medicineList.add(builder.build());
        }

        endElementP2(reader);
        endElementP3(reader);
    }


    private void endElementP2(XMLStreamReader reader) throws XMLStreamException {


        if ("name".equalsIgnoreCase(reader.getLocalName())) {
            name = content;
        }
        if ("PharmName".equalsIgnoreCase(reader.getLocalName())) {
            pharmName = content;
        }
        if ("Country".equalsIgnoreCase(reader.getLocalName())) {
            country = content;
        }
        if ("Group".equalsIgnoreCase(reader.getLocalName())) {
            group = Group.valueOf(content.toUpperCase());
        }
        if ("AnalogName".equalsIgnoreCase(reader.getLocalName())) {
            analogs.add(content);
        }


    }


    private void endElementP3(XMLStreamReader reader) throws XMLStreamException {
        if ("MedicineType".equalsIgnoreCase(reader.getLocalName())) {
            medicineType = content;
        }
        if ("Number".equalsIgnoreCase(reader.getLocalName())) {
            cerNumber = content;
        }
        if ("IssueDate".equalsIgnoreCase(reader.getLocalName())) {
            try {
                issueDate = new SimpleDateFormat("yyyy-MM-dd").parse(content);
            } catch (ParseException e) {
                logger.error("An error occured while parsing date", e);
                throw new XMLStreamException("Invalid issueDate");
            }
        }
        if ("ExpiredDate".equalsIgnoreCase(reader.getLocalName())) {
            try {
                expiredDate = new SimpleDateFormat("yyyy-MM-dd").parse(content);
            } catch (ParseException e) {
                logger.error("An error occured while parsing date", e);
                throw new XMLStreamException("Invalid expiredDate");
            }
        }
        if ("CertificationOrganization".equalsIgnoreCase(reader.getLocalName())) {
            cerOrg = content;
        }

        if ("PackageType".equalsIgnoreCase(reader.getLocalName())) {
            packageType = PackageType.valueOf(content.toUpperCase());
        }
        if ("Unit".equalsIgnoreCase(reader.getLocalName())) {
            units = Arrays.stream(Units.values())
                    .filter(e -> e.getShortDescription().equalsIgnoreCase(content))
                    .findFirst()
                    .orElseThrow(() -> new XMLStreamException("incorrect units value"));
        }
        if ("Qty".equalsIgnoreCase(reader.getLocalName())) {
            try {
                qty = Double.parseDouble(content);
            } catch (Exception e) {
                logger.error("An error occured while parsing qty", e);
                throw new XMLStreamException(e);
            }

        }
        if ("Price".equalsIgnoreCase(reader.getLocalName())) {
            try {
                price = BigDecimal.valueOf(Double.parseDouble(content));
            } catch (Exception e) {
                logger.error("An error occured while parsing qty", e);
                throw new XMLStreamException(e);
            }
        }

        if ("DosageUnit".equalsIgnoreCase(reader.getLocalName())) {
            dosageUnits = Arrays.stream(Units.values())
                    .filter(e -> e.getShortDescription().equalsIgnoreCase(content))
                    .findFirst()
                    .orElseThrow(() -> new XMLStreamException("incorrect units value"));
        }
        if ("DosageQty".equalsIgnoreCase(reader.getLocalName())) {
            try {
                dosageQty = Double.parseDouble(content);
            } catch (NumberFormatException e) {
                logger.error("Wrong dosage", e);
                throw new XMLStreamException(e);
            }
        }
        if ("UsageDescription".equalsIgnoreCase(reader.getLocalName())) {
            usageDescription = content;
        }
    }
}
