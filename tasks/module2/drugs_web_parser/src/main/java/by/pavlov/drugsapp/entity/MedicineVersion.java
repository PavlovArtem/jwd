package by.pavlov.drugsapp.entity;

import java.util.Objects;

public abstract class MedicineVersion {

    private String versionName;
    private Certificate certificate;
    private Packaging packaging;
    private Dosage dosage;

    public MedicineVersion(String versionName, Certificate certificate, Packaging packaging, Dosage dosage) {
        this.versionName = versionName;
        this.certificate = certificate;
        this.packaging = packaging;
        this.dosage = dosage;
    }

    public MedicineVersion() {
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    public Certificate getCertificate() {
        return certificate;
    }

    public void setCertificate(Certificate certificate) {
        this.certificate = certificate;
    }

    public Packaging getPackaging() {
        return packaging;
    }

    public void setPackaging(Packaging packaging) {
        this.packaging = packaging;
    }

    public Dosage getDosage() {
        return dosage;
    }

    public void setDosage(Dosage dosage) {
        this.dosage = dosage;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MedicineVersion that = (MedicineVersion) o;
        return Objects.equals(versionName, that.versionName) &&
                Objects.equals(certificate, that.certificate) &&
                Objects.equals(packaging, that.packaging) &&
                Objects.equals(dosage, that.dosage);
    }

    @Override
    public int hashCode() {
        return Objects.hash(versionName, certificate, packaging, dosage);
    }

    @Override
    public String toString() {
        return "MedicineVersion{" +
                "versionName='" + versionName + '\'' +
                ", certificate=" + certificate +
                ", packaging=" + packaging +
                ", dosage=" + dosage +
                '}';
    }
}
