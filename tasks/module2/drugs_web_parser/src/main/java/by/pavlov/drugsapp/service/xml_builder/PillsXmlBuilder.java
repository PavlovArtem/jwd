package by.pavlov.drugsapp.service.xml_builder;

import by.pavlov.drugsapp.entity.Pills;

import java.util.ArrayList;
import java.util.List;

public class PillsXmlBuilder extends AbstractXmlStringBuilder {
    private final Pills pills;
    private final List<XmlStringBuilder> childBuilders = new ArrayList<>();

    public PillsXmlBuilder(Pills pills) {
        this.pills = pills;
    }

    @Override
    public void addChild(XmlStringBuilder child) {
        childBuilders.add(child);
    }

    @Override
    public String build() {

        StringBuilder xmlFragment = new StringBuilder();

        xmlFragment.append(addOpenTagWithAttrs("Pills",
                "color=" + "\"" + pills.getColor() + "\""
                , "shape=" + "\"" + pills.getShape() + "\""));

        xmlFragment.append(addElement("MedicineType",pills.getVersionName()));

        xmlFragment.append(buildChild(childBuilders));

        xmlFragment.append(addCloseTag("Pills"));


        return xmlFragment.toString();
    }
}
