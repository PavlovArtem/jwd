package by.pavlov.drugsapp.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class MainPageCommand extends AbstractCommand {
    private static final Logger logger = LogManager.getLogger(MainPageCommand.class);



    @Override
    protected void executeWrapped(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        forward(req,resp,"medicines");
    }
}
