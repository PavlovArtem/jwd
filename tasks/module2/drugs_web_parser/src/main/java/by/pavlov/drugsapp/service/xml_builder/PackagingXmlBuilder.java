package by.pavlov.drugsapp.service.xml_builder;

import by.pavlov.drugsapp.entity.Packaging;

public class PackagingXmlBuilder extends AbstractXmlStringBuilder {
    private final Packaging packaging;


    public PackagingXmlBuilder(Packaging packaging) {
        this.packaging = packaging;
    }

    @Override
    public String build() {

        StringBuilder xmlFragment = new StringBuilder();
        xmlFragment.append(addOpenTag("Packaging"));
        xmlFragment.append(addElement("PackageType", packaging.getPackageType().name()));
        xmlFragment.append(addElement("Unit", packaging.getUnits().getShortDescription()));
        xmlFragment.append(addElement("Qty", packaging.getQuantity().toString()));
        xmlFragment.append(addElement("Price", packaging.getPrice().toString()));
        xmlFragment.append(addCloseTag("Packaging"));

        return xmlFragment.toString();
    }
}
