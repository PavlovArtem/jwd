package by.pavlov.drugsapp.service.xml_builder;

import by.pavlov.drugsapp.entity.Pharm;

public class PharmXmlBuilder extends AbstractXmlStringBuilder {

    private final Pharm pharm;

    public PharmXmlBuilder(Pharm pharm) {
        this.pharm = pharm;
    }


    @Override
    public String build() {

        StringBuilder xmlFragment = new StringBuilder();
        xmlFragment.append(addOpenTag("Pharm"));
        xmlFragment.append(addElement("PharmName", pharm.getName()));
        xmlFragment.append(addElement("Country", pharm.getCountry()));

        xmlFragment.append(addCloseTag("Pharm"));
        return xmlFragment.toString();
    }
}
