package by.pavlov.drugsapp.dal;

import by.pavlov.drugsapp.entity.Medicine;
import by.pavlov.drugsapp.entity.Pharm;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

public class MedicineDaoInMemory implements MedicineDao {

    private final Map<Long, Medicine> storage = new ConcurrentHashMap<>();
    private final AtomicLong sequence = new AtomicLong(1);


    @Override
    public List<Medicine> findAll() {
        return new ArrayList<>(this.storage.values());
    }

    @Override
    public void save(Medicine medicine) {
        Medicine value = cloneEntity(medicine);
        value.setId(sequence.incrementAndGet());
        this.storage.put(value.getId(), value);
    }


    @Override
    public void update(Long key, Medicine medicine) {

        if (this.storage.containsKey(key)) {
            this.storage.put(key, cloneEntity(medicine));
        } else {
            throw new EntityNotFoundException("Entity not found key: " + key);
        }
    }

    @Override
    public Medicine get(Long key) {
        if (this.storage.containsKey(key)) {
            return this.get(key);
        } else {
            throw new EntityNotFoundException("Entity not found key: " + key);
        }
    }

    @Override
    public void delete(Long key) {
        this.storage.remove(key);
    }


    private Medicine cloneEntity(Medicine medicine) {

        Medicine clone = new Medicine();
        clone.setId(medicine.getId());
        clone.setName(medicine.getName());
        clone.setPharm(new Pharm(
                medicine.getPharm().getName(),
                medicine.getPharm().getCountry()
        ));
        clone.setGroup(medicine.getGroup());
        clone.setAnalogNames(
                new ArrayList<>(medicine.getAnalogNames())
        );
        clone.setMedicineVersions(
                new ArrayList<>(medicine.getMedicineVersions())
        );

        return clone;
    }
}
