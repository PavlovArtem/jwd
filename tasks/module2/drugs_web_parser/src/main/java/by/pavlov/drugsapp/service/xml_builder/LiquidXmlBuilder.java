package by.pavlov.drugsapp.service.xml_builder;

import by.pavlov.drugsapp.entity.Liquid;

import java.util.ArrayList;
import java.util.List;

public class LiquidXmlBuilder extends AbstractXmlStringBuilder {
    private final Liquid liquid;
    private final List<XmlStringBuilder> childBuilders = new ArrayList<>();

    public LiquidXmlBuilder(Liquid liquid) {
        this.liquid = liquid;
    }

    @Override
    public String build() {

        StringBuilder xmlFragment = new StringBuilder();

        xmlFragment.append(addOpenTagWithAttrs("Liquid", "consistence=" + "\"" + liquid.getConsistence() + "\""));

        xmlFragment.append(addElement("MedicineType",liquid.getVersionName()));

        xmlFragment.append(buildChild(childBuilders));

        xmlFragment.append(addCloseTag("Liquid"));

        return xmlFragment.toString();

    }

    @Override
    public void addChild(XmlStringBuilder child) {
        childBuilders.add(child);
    }
}
