package by.pavlov.drugsapp.command;

import java.util.EnumMap;
import java.util.Map;

public class CommandProviderImpl implements CommandProvider {

    private final Map<CommandType, Command> commandMap;

    public CommandProviderImpl() {
        this.commandMap = new EnumMap<>(CommandType.class);
    }


    @Override
    public void register(CommandType commandType, Command command) {
        commandMap.put(commandType, command);
    }

    @Override
    public void remove(CommandType commandType) {
        commandMap.remove(commandType);
    }

    @Override
    public Command getCommand(CommandType commandType) {
        return this.commandMap.get(commandType);
    }
}
