package by.pavlov.drugsapp.service.xml_builder;

import by.pavlov.drugsapp.entity.Liquid;
import by.pavlov.drugsapp.entity.Medicine;
import by.pavlov.drugsapp.entity.MedicineVersion;
import by.pavlov.drugsapp.entity.Pills;

import java.util.List;

public class MedicineXmlBuilderManager implements XmlBuilderManager<List<Medicine>> {


    @Override
    public XmlStringBuilder createXmlBuilder(List<Medicine> medicines) {

        XmlStringBuilder medicinesListXmlBuilder = new MedicinesListXmlBuilder();
        for (Medicine medicine : medicines) {
            medicinesListXmlBuilder.addChild(getMedicineBuilder(medicine));
        }


        return medicinesListXmlBuilder;
    }

    private XmlStringBuilder getMedicineBuilder(Medicine entity) {
        XmlStringBuilder medicineXmlBuilder = new MedicineXmlBuilder(entity);

        medicineXmlBuilder.addChild(new PharmXmlBuilder(entity.getPharm()));
        medicineXmlBuilder.addChild(new GroupXmlBuilder(entity.getGroup()));
        medicineXmlBuilder.addChild(new AnalogsXmlBuilder(entity.getAnalogNames()));


        XmlStringBuilder versionsBuilder = new VersionsXmlBuilder(entity.getMedicineVersions());
        for (MedicineVersion medicineVersion : entity.getMedicineVersions()) {
            XmlStringBuilder versionBuilder;
            if (medicineVersion instanceof Pills) {
                versionBuilder = new PillsXmlBuilder((Pills) medicineVersion);
            } else {
                versionBuilder = new LiquidXmlBuilder((Liquid) medicineVersion);
            }
            versionBuilder.addChild(new CertificateXmlBuilder(medicineVersion.getCertificate()));
            versionBuilder.addChild(new PackagingXmlBuilder(medicineVersion.getPackaging()));
            versionBuilder.addChild(new DosageXmlBuilder(medicineVersion.getDosage()));

            versionsBuilder.addChild(versionBuilder);
        }

        medicineXmlBuilder.addChild(versionsBuilder);

        return medicineXmlBuilder;
    }


}
