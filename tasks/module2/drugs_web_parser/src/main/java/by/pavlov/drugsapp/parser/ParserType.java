package by.pavlov.drugsapp.parser;

import java.util.Optional;
import java.util.stream.Stream;

public enum ParserType {
    DOM_PARSER,
    SAX_PARSER,
    STAX_PARSER;

    public static Optional<ParserType> of(String name) {
        return Stream.of(ParserType.values())
                .filter(parserType -> parserType.name()
                        .equalsIgnoreCase(name))
                .findFirst();
    }
}
