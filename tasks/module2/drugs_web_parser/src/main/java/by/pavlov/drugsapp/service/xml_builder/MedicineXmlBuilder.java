package by.pavlov.drugsapp.service.xml_builder;

import by.pavlov.drugsapp.entity.Medicine;

import java.util.ArrayList;
import java.util.List;

public class MedicineXmlBuilder extends AbstractXmlStringBuilder {

    private final Medicine medicine;
    private final List<XmlStringBuilder> childBuilders = new ArrayList<>();

    public MedicineXmlBuilder(Medicine medicine) {
        this.medicine = medicine;
    }

    @Override
    public void addChild(XmlStringBuilder child) {
        childBuilders.add(child);

    }

    @Override
    public String build() {
        StringBuilder resultXml = new StringBuilder();

        resultXml.append(addOpenTagWithAttrs("Medicine", "id=" + "\"" + medicine.getId() + "\""))
                .append(addElement("Name", medicine.getName()));


        resultXml.append(buildChild(childBuilders));

        resultXml.append(addCloseTag("Medicine"));

        return resultXml.toString();
    }
}
