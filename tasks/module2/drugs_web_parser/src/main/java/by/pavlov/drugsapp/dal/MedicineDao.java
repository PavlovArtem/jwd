package by.pavlov.drugsapp.dal;

import by.pavlov.drugsapp.entity.Medicine;

public interface MedicineDao extends CrudEntityDAO<Medicine, Long> {
}
