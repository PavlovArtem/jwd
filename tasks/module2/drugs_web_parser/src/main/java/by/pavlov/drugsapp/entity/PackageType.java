package by.pavlov.drugsapp.entity;

public enum PackageType{
    BOX,
    BOTTLE,
    TUBE,
    SPRAY;

}
