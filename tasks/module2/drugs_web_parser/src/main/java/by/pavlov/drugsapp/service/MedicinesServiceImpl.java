package by.pavlov.drugsapp.service;

import by.pavlov.drugsapp.dal.MedicineDao;
import by.pavlov.drugsapp.entity.Medicine;

import java.util.List;

public class MedicinesServiceImpl implements MedicinesService {

    private final MedicineDao medicineDao;

    public MedicinesServiceImpl(MedicineDao medicineDao) {
        this.medicineDao = medicineDao;
    }

    @Override
    public void saveAll(List<Medicine> medicines) {
        medicines.forEach(medicineDao::save);

    }

    @Override
    public void save(Medicine medicine) {
        medicineDao.save(medicine);
    }

    @Override
    public List<Medicine> findAll() {
        return medicineDao.findAll();
    }

    @Override
    public void update(Long id, Medicine medicine) {
        medicineDao.update(id, medicine);
    }

    @Override
    public void delete(Long id) {
        medicineDao.delete(id);
    }
}
