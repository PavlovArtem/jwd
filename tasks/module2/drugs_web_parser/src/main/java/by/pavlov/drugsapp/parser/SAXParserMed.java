package by.pavlov.drugsapp.parser;

import by.pavlov.drugsapp.entity.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class SAXParserMed extends DefaultHandler implements XmlParser {

    private static final Logger logger = LogManager.getLogger(SAXParserMed.class);

    private List<Medicine> medicineList;
    //Medicine
    private Medicine.Builder builder;
    private String name;
    private Pharm pharm;
    private String pharmName;
    private String country;
    private Group group;
    private List<String> analogs;
    private List<MedicineVersion> medicineVersions;
    //Medicine
    //MedicineVersion
    private MedicineVersion medVersion;
    private String medicineType;
    //certificate
    private Certificate certificate;
    private String cerNumber;
    private Date issueDate;
    private Date expiredDate;
    private String cerOrg;
    //----------
    //packaging
    private Packaging packaging;
    private PackageType packageType;
    private Units units;
    private Double qty;
    private BigDecimal price;
    //-----------
    //dosage
    private Dosage dosage;
    private Units dosageUnits;
    private Double dosageQty;
    private String usageDescription;
    private String lastElementName;



    @Override
    public List parse(InputStream stream) throws XmlParseException {



        SAXParserFactory parserFactory =
                SAXParserFactory.newInstance();
        try {
            SAXParser parser = parserFactory.newSAXParser();
            parser.parse(stream, this);


        } catch (ParserConfigurationException | SAXException | IOException e) {
            logger.error("Error while parsing xml file", e);
            throw new XmlParseException(e);
        }

        return medicineList;
    }
    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        lastElementName = qName;

        if ("Medicines".equalsIgnoreCase(qName)) {
            medicineList = new ArrayList<>();
        }
        if ("Medicine".equalsIgnoreCase(qName)) {
            builder = new Medicine.Builder();
            builder.id(Long.parseLong(attributes.getValue("id")));
        }
        if ("Pharm".equalsIgnoreCase(qName)) {
            pharm = new Pharm();
        }
        if ("Analogs".equalsIgnoreCase(qName)) {
            analogs = new ArrayList<>();
        }
        if ("Versions".equalsIgnoreCase(qName)) {
            medicineVersions = new ArrayList<>();
        }
        //med versions
        if ("Pills".equalsIgnoreCase(qName)) {
            Pills pills = new Pills();
            pills.setColor(attributes.getValue("color"));
            pills.setShape(attributes.getValue("shape"));
            medVersion = pills;
        }
        if ("Liquid".equalsIgnoreCase(qName)) {
            Liquid liquid = new Liquid();
            liquid.setConsistence(attributes.getValue("consistence"));
            medVersion = liquid;
        }
        //------------
        if ("Certificate".equalsIgnoreCase(qName)) {
            certificate = new Certificate();
        }
        if ("Packaging".equalsIgnoreCase(qName)) {
            packaging = new Packaging();
        }
        if ("Dosage".equalsIgnoreCase(qName)) {
            dosage = new Dosage();
        }


    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if ("Name".equalsIgnoreCase(qName)) {
            builder.name(name);
        }
        if ("Pharm".equalsIgnoreCase(qName)) {
            pharm.setName(pharmName);
            pharm.setCountry(country);
            builder.pharm(pharm);
        }
        if ("Group".equalsIgnoreCase(qName)) {
            builder.group(group);
        }
        if ("Analogs".equalsIgnoreCase(qName)) {
            builder.analogNames(analogs);
        }
        if ("Certificate".equalsIgnoreCase(qName)) {
            certificate.setNumber(cerNumber);
            certificate.setIssueDate(issueDate);
            certificate.setExpiredDate(expiredDate);
            certificate.setCertificationOrganization(cerOrg);
            medVersion.setCertificate(certificate);
        }
        if ("Packaging".equalsIgnoreCase(qName)) {
            packaging.setPackageType(packageType);
            packaging.setUnits(units);
            packaging.setQuantity(qty);
            packaging.setPrice(price);
            medVersion.setPackaging(packaging);
        }
        if ("Dosage".equalsIgnoreCase(qName)) {
            dosage.setQuantity(dosageQty);
            dosage.setUnits(dosageUnits);
            dosage.setUsageDescription(usageDescription);
            medVersion.setDosage(dosage);
        }
        if ("Pills".equalsIgnoreCase(qName)
                || "Liquid".equalsIgnoreCase(qName)) {
            medVersion.setVersionName(medicineType);
            medicineVersions.add(medVersion);
        }
        if ("Versions".equalsIgnoreCase(qName)) {
            builder.medicineVersion(medicineVersions);
        }


        if ("Medicine".equalsIgnoreCase(qName)) {
            medicineList.add(builder.build());
        }

    }


    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        String content = String.copyValueOf(ch, start, length).trim();
        if (!content.isEmpty()) {
            if ("name".equalsIgnoreCase(lastElementName)) {
                name = content;
            }
            if ("PharmName".equalsIgnoreCase(lastElementName)) {
                pharmName = content;
            }
            if ("Country".equalsIgnoreCase(lastElementName)) {
                country = content;
            }
            if ("Group".equalsIgnoreCase(lastElementName)) {
                group = Group.valueOf(content.toUpperCase());
            }
            if ("AnalogName".equalsIgnoreCase(lastElementName)) {
                analogs.add(content);
            }

            medicineVersionCharacters(content);


        }
    }


    private void medicineVersionCharacters(String content) throws SAXException {
        if ("MedicineType".equalsIgnoreCase(lastElementName)) {
            medicineType = content;
        }
        if ("Number".equalsIgnoreCase(lastElementName)) {
            cerNumber = content;
        }
        if ("IssueDate".equalsIgnoreCase(lastElementName)) {
            try {
                issueDate = new SimpleDateFormat("yyyy-MM-dd").parse(content);
            } catch (ParseException e) {
                logger.error("An error occured while parsing date", e);
                throw new SAXException("Invalid issueDate");
            }
        }
        if ("ExpiredDate".equalsIgnoreCase(lastElementName)) {
            try {
                expiredDate = new SimpleDateFormat("yyyy-MM-dd").parse(content);
            } catch (ParseException e) {
                logger.error("An error occured while parsing date", e);
                throw new SAXException("Invalid expiredDate");
            }
        }
        if ("CertificationOrganization".equalsIgnoreCase(lastElementName)) {
            cerOrg = content;
        }

        if ("PackageType".equalsIgnoreCase(lastElementName)) {
            packageType = PackageType.valueOf(content.toUpperCase());
        }
        if ("Unit".equalsIgnoreCase(lastElementName)) {
            units = Arrays.stream(Units.values())
                    .filter(e -> e.getShortDescription().equalsIgnoreCase(content))
                    .findFirst()
                    .orElseThrow(() -> new SAXException("incorrect units value"));
        }
        if ("Qty".equalsIgnoreCase(lastElementName)) {
            try {
                qty = Double.parseDouble(content);
            } catch (Exception e) {
                logger.error("An error occured while parsing qty", e);
                throw new SAXException(e);
            }

        }
        if ("Price".equalsIgnoreCase(lastElementName)) {
            try {
                price = BigDecimal.valueOf(Double.parseDouble(content));
            } catch (Exception e) {
                logger.error("An error occured while parsing qty", e);
                throw new SAXException(e);
            }
        }

        if ("DosageUnit".equalsIgnoreCase(lastElementName)) {
            dosageUnits = Arrays.stream(Units.values())
                    .filter(e -> e.getShortDescription().equalsIgnoreCase(content))
                    .findFirst()
                    .orElseThrow(() -> new SAXException("incorrect units value"));
        }
        if ("DosageQty".equalsIgnoreCase(lastElementName)) {
            try {
                dosageQty = Double.parseDouble(content);
            } catch (NumberFormatException e) {
                logger.error("Wrong dosage", e);
                throw new SAXException(e);
            }
        }
        if ("UsageDescription".equalsIgnoreCase(lastElementName)) {
            usageDescription = content;
        }
    }
}
