package by.pavlov.drugsapp.service.xml_builder;

public interface XmlStringBuilder {

    String build();

    default void addChild(XmlStringBuilder child) {

    }
}
