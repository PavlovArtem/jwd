package by.pavlov.drugsapp.service;

import by.pavlov.drugsapp.entity.Medicine;

import java.util.List;

public interface MedicinesService {


    void saveAll(List<Medicine> medicines);

    void save(Medicine medicine);

    List<Medicine> findAll();

    void update(Long id, Medicine medicine);

    void delete(Long id);


}
