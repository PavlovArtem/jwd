package by.pavlov.drugsapp.entity;

import java.util.List;

public class Medicine {

    private Long id;
    private String name;
    private Pharm pharm;
    private Group group;
    private List<String> analogNames;
    private List<MedicineVersion> medicineVersions;

    public Medicine(Long id, String name, Pharm pharm, Group group, List<String> analogNames, List<MedicineVersion> medicineVersions) {
        this.id = id;
        this.name = name;
        this.pharm = pharm;
        this.group = group;
        this.analogNames = analogNames;
        this.medicineVersions = medicineVersions;
    }

    public Medicine() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Pharm getPharm() {
        return pharm;
    }

    public void setPharm(Pharm pharm) {
        this.pharm = pharm;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public List<String> getAnalogNames() {
        return analogNames;
    }

    public void setAnalogNames(List<String> analogNames) {
        this.analogNames = analogNames;
    }

    public List<MedicineVersion> getMedicineVersions() {
        return medicineVersions;
    }

    public void setMedicineVersions(List<MedicineVersion> medicineVersions) {
        this.medicineVersions = medicineVersions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Medicine medicine = (Medicine) o;

        if (id != null ? !id.equals(medicine.id) : medicine.id != null) return false;
        if (name != null ? !name.equals(medicine.name) : medicine.name != null) return false;
        if (pharm != null ? !pharm.equals(medicine.pharm) : medicine.pharm != null) return false;
        if (group != medicine.group) return false;
        if (analogNames != null ? !analogNames.equals(medicine.analogNames) : medicine.analogNames != null)
            return false;
        return medicineVersions != null ? medicineVersions.equals(medicine.medicineVersions) : medicine.medicineVersions == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (pharm != null ? pharm.hashCode() : 0);
        result = 31 * result + (group != null ? group.hashCode() : 0);
        result = 31 * result + (analogNames != null ? analogNames.hashCode() : 0);
        result = 31 * result + (medicineVersions != null ? medicineVersions.hashCode() : 0);
        return result;
    }


    @Override
    public String toString() {
        return "Medicine{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", pharm=" + pharm +
                ", group=" + group +
                ", analogNames=" + analogNames +
                ", medicineVersions=" + medicineVersions +
                '}';
    }


    public static class Builder{
        private Medicine medicine;

        public Builder(){
            medicine = new Medicine();
        }

        public Builder id(Long id){
            medicine.id = id;
            return this;
        }

        public Builder name(String name){
            medicine.name = name;
            return this;
        }

        public Builder pharm(Pharm pharm){
            medicine.pharm = pharm;
            return this;
        }

        public Builder group(Group group){

            medicine.group = group;
            return this;
        }

        public Builder analogNames(List<String> analogs){
            medicine.analogNames = analogs;
            return  this;
        }

        public Builder medicineVersion(List<MedicineVersion> medicineVersion){
            medicine.medicineVersions = medicineVersion;
            return this;
        }

        public Medicine build(){
            return this.medicine;
        }





    }
}
