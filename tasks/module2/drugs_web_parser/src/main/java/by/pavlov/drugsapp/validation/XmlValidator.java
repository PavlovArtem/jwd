package by.pavlov.drugsapp.validation;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.IOException;
import java.io.InputStream;


public class XmlValidator {

    private static final Logger logger = LogManager.getLogger(XmlValidator.class);

    public boolean validate(InputStream inputStream) {
        try {
            SchemaFactory factory =
                    SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = factory.newSchema(ClassLoader.getSystemResource("xsdTemplate.xsd"));
            Validator validator = schema.newValidator();
            validator.validate(new StreamSource(inputStream));
        } catch (IOException | org.xml.sax.SAXException e) {
            logger.error("Exception: " + e.getMessage());
            return false;
        }
        return true;
    }

}
