package by.pavlov.drugsapp.application;

import by.pavlov.drugsapp.command.Command;
import by.pavlov.drugsapp.command.CommandProvider;
import by.pavlov.drugsapp.command.CommandType;
import by.pavlov.drugsapp.command.CommandUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@MultipartConfig(fileSizeThreshold = 6291456,
        maxFileSize = 10485760L,
        maxRequestSize = 20971520L
)
@WebServlet(urlPatterns = "/", name = "app")
public class AppServlet extends HttpServlet {

    public static final Logger logger = LogManager.getLogger(AppServlet.class);
    public static final String GET_RESPONSE = "Hello";
    private CommandProvider commandProvider;

    @Override
    public void init() throws ServletException {
        this.commandProvider = ApplicationContext.getInstance().getBean(CommandProvider.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String commandFromRequest = CommandUtil.getCommandFromRequest(req);
        CommandType commandType = CommandType.of(commandFromRequest).orElse(CommandType.MAIN_PAGE);
        commandProvider.getCommand(commandType);
        Command command = commandProvider.getCommand(commandType);
        command.execute(req, resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String commandFromRequest = CommandUtil.getCommandFromRequest(req);
        CommandType commandType = CommandType.of(commandFromRequest).orElse(CommandType.MAIN_PAGE);
        commandProvider.getCommand(commandType);
        Command command = commandProvider.getCommand(commandType);
        command.execute(req, resp);

    }


}
