package by.pavlov.drugsapp.service;

import by.pavlov.drugsapp.entity.Medicine;
import by.pavlov.drugsapp.service.xml_builder.XmlBuilderManager;
import by.pavlov.drugsapp.service.xml_builder.XmlStringBuilder;

import java.util.List;

public class XmlCreationService implements FileCreationService {

    private final XmlBuilderManager xmlBuilderManager;
    private final MedicinesService medicinesService;

    public XmlCreationService(XmlBuilderManager xmlBuilderManager, MedicinesService medicinesService) {
        this.xmlBuilderManager = xmlBuilderManager;
        this.medicinesService = medicinesService;
    }

    @Override
    public String formData() {
        List<Medicine> medicines = medicinesService.findAll();
        XmlStringBuilder xmlBuilder = xmlBuilderManager.createXmlBuilder(medicines);
        return xmlBuilder.build();
    }
}
