package by.pavlov.drugsapp.command;

import by.pavlov.drugsapp.service.FileCreationService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DownloadXmlCommand extends AbstractCommand {

    private final FileCreationService fileCreationService;

    public DownloadXmlCommand(FileCreationService fileCreationService) {
        this.fileCreationService = fileCreationService;
    }


    @Override
    protected void executeWrapped(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        resp.setContentType("text/plain");
        resp.setHeader("Content-Disposition", "attachment; filename=" + "medicines.xml");
        String result = fileCreationService.formData();
        resp.getOutputStream().write(result.getBytes());
    }
}
