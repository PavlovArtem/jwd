package by.pavlov.drugsapp.application;

import by.pavlov.drugsapp.command.*;
import by.pavlov.drugsapp.dal.MedicineDao;
import by.pavlov.drugsapp.dal.MedicineDaoInMemory;
import by.pavlov.drugsapp.parser.*;
import by.pavlov.drugsapp.service.FileCreationService;
import by.pavlov.drugsapp.service.MedicinesService;
import by.pavlov.drugsapp.service.MedicinesServiceImpl;
import by.pavlov.drugsapp.service.XmlCreationService;
import by.pavlov.drugsapp.service.xml_builder.MedicineXmlBuilderManager;
import by.pavlov.drugsapp.service.xml_builder.XmlBuilderManager;

import java.util.HashMap;
import java.util.Map;

public class ApplicationContext {

    private static final ApplicationContext INSTANCE = new ApplicationContext();

    private final Map<Class<?>, Object> beans = new HashMap<>();

    public static ApplicationContext getInstance() {
        return INSTANCE;
    }

    public void initialize() {
        MedicineDao medicineDao = new MedicineDaoInMemory();
        MedicinesService medicinesService = new MedicinesServiceImpl(medicineDao);
        XmlBuilderManager xmlBuilderManager = new MedicineXmlBuilderManager();
        FileCreationService fileCreationService = new XmlCreationService(xmlBuilderManager, medicinesService);


        ParserProvider parserProvider = new ParserProviderImpl();
        parserProvider.register(ParserType.DOM_PARSER, new DOMParserMed());
        parserProvider.register(ParserType.SAX_PARSER, new SAXParserMed());
        parserProvider.register(ParserType.STAX_PARSER, new StAXParserMed());


        CommandProvider commandProvider = new CommandProviderImpl();
        commandProvider.register(CommandType.MAIN_PAGE, new MainPageCommand());
        commandProvider.register(CommandType.DISPLAY_MEDICINES, new DisplayMedicinesCommand(medicinesService));
        commandProvider.register(CommandType.UPLOAD_FILE, new UploadFileCommand(medicinesService, parserProvider));
        commandProvider.register(CommandType.DOWNLOAD_XML, new DownloadXmlCommand(fileCreationService));


        beans.put(medicineDao.getClass().getSuperclass(), medicineDao);
        beans.put(medicinesService.getClass(), medicinesService);
        beans.put(CommandProvider.class, commandProvider);
        beans.put(ParserProvider.class, parserProvider);
    }


    public void destroy() {
        beans.clear();
    }

    public <T> T getBean(Class<T> clazz) {
        return (T) this.beans.get(clazz);
    }


}
