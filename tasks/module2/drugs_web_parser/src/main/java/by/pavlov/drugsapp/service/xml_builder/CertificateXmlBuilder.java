package by.pavlov.drugsapp.service.xml_builder;

import by.pavlov.drugsapp.entity.Certificate;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CertificateXmlBuilder extends AbstractXmlStringBuilder {

    private final Certificate certificate;

    public CertificateXmlBuilder(Certificate certificate) {
        this.certificate = certificate;
    }

    @Override
    public String build() {

        StringBuilder xmlFragment = new StringBuilder();

        xmlFragment.append(addOpenTag("Certificate"));
        xmlFragment.append(addElement("Number", certificate.getNumber()));
        xmlFragment.append(addElement("IssueDate", getFormattedDate(certificate.getIssueDate())));
        xmlFragment.append(addElement("ExpiredDate", getFormattedDate(certificate.getExpiredDate())));
        xmlFragment.append(addElement("CertificationOrganization", certificate.getCertificationOrganization()));
        xmlFragment.append(addCloseTag("Certificate"));

        return xmlFragment.toString();
    }

    private String getFormattedDate(Date date) {

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return dateFormat.format(date);
    }


}
