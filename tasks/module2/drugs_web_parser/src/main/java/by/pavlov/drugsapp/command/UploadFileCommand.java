package by.pavlov.drugsapp.command;

import by.pavlov.drugsapp.entity.Medicine;
import by.pavlov.drugsapp.parser.ParserProvider;
import by.pavlov.drugsapp.parser.ParserType;
import by.pavlov.drugsapp.parser.XmlParseException;
import by.pavlov.drugsapp.parser.XmlParser;
import by.pavlov.drugsapp.service.MedicinesService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;


public class UploadFileCommand extends AbstractCommand {
    public static final Logger logger = LogManager.getLogger(UploadFileCommand.class);

    private final MedicinesService medicinesService;
    private final ParserProvider parserProvider;

    public UploadFileCommand(MedicinesService medicinesService, ParserProvider parserProvider) {
        this.medicinesService = medicinesService;
        this.parserProvider = parserProvider;
    }

    @Override
    protected void executeWrapped(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {

        Part xmlFile = req.getPart("uploadFile");

        Part partChosenParser = req.getPart("parser");

        if (xmlFile == null || xmlFile.getSize() <= 0L) {
            req.setAttribute("error", Collections.singleton("File doesn't exist or empty"));
            forward(req, resp, "medicines");
        }

        InputStream is = req.getPart("uploadFile").getInputStream();

        String parserName = new BufferedReader(
                new InputStreamReader(partChosenParser.getInputStream(), StandardCharsets.UTF_8))
                .lines()
                .collect(Collectors.joining("\n"));
        XmlParser parser = parserProvider.getParser(ParserType.of(parserName).orElse(ParserType.SAX_PARSER));

        List<Medicine> medicines = parseFile(is, parser);
        medicinesService.saveAll(medicines);


        redirect(resp, req.getContextPath() + "?_command=" + CommandType.DISPLAY_MEDICINES);

    }

    private List<Medicine> parseFile(InputStream stream, XmlParser parser) {


        List<Medicine> medicines = new ArrayList<>();

        try {

            medicines = parser.parse(stream);
        } catch (XmlParseException e) {
            logger.error("Error occured while parsing file", e);
        }

        return medicines;

    }


}
