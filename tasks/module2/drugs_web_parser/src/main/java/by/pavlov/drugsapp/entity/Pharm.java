package by.pavlov.drugsapp.entity;

import java.util.Objects;

public class Pharm {

    private String name;
    private String country;

    public Pharm(String name, String country) {
        this.name = name;
        this.country = country;
    }

    public Pharm() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pharm pharm = (Pharm) o;
        return Objects.equals(name, pharm.name) &&
                Objects.equals(country, pharm.country);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, country);
    }

    @Override
    public String toString() {
        return "Pharm{" +
                "name='" + name + '\'' +
                ", country='" + country + '\'' +
                '}';
    }
}
