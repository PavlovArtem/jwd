package by.pavlov.drugsapp.command;

import by.pavlov.drugsapp.service.MedicinesService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DisplayMedicinesCommand extends AbstractCommand {

    private static final Logger logger = LogManager.getLogger(DisplayMedicinesCommand.class);

    private final MedicinesService medicinesService;

    public DisplayMedicinesCommand(MedicinesService medicinesService) {
        this.medicinesService = medicinesService;
    }


    @Override
    public void executeWrapped(HttpServletRequest req, HttpServletResponse resp) {

        req.setAttribute("medicines",medicinesService.findAll());

        forward(req, resp, "medicines");

    }
}
