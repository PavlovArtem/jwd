package by.pavlov.drugsapp.entity;

import java.util.Objects;

public class Dosage {
    private Units units;
    private Double quantity;
    private String usageDescription;


    public Dosage(Units units, Double quantity, String usageDescription) {
        this.units = units;
        this.quantity = quantity;
        this.usageDescription = usageDescription;
    }

    public Dosage() {
    }

    public Units getUnits() {
        return units;
    }

    public void setUnits(Units units) {
        this.units = units;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public String getUsageDescription() {
        return usageDescription;
    }

    public void setUsageDescription(String usageDescription) {
        this.usageDescription = usageDescription;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Dosage dosage = (Dosage) o;
        return units == dosage.units &&
                Objects.equals(quantity, dosage.quantity) &&
                Objects.equals(usageDescription, dosage.usageDescription);
    }

    @Override
    public int hashCode() {
        return Objects.hash(units, quantity, usageDescription);
    }

    @Override
    public String toString() {
        return "Dosage{" +
                "units=" + units +
                ", quantity=" + quantity +
                ", usageDescription='" + usageDescription + '\'' +
                '}';
    }
}
