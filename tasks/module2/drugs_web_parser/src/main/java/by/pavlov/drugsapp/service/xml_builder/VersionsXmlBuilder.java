package by.pavlov.drugsapp.service.xml_builder;

import by.pavlov.drugsapp.entity.MedicineVersion;

import java.util.ArrayList;
import java.util.List;

public class VersionsXmlBuilder extends AbstractXmlStringBuilder {

    private final List<XmlStringBuilder> childBuilders = new ArrayList<>();
    private final List<MedicineVersion> medicineVersions;


    public VersionsXmlBuilder(List<MedicineVersion> medicineVersions) {
        this.medicineVersions = medicineVersions;
    }

    @Override
    public void addChild(XmlStringBuilder child) {
        childBuilders.add(child);
    }

    @Override
    public String build() {

        StringBuilder xmlFragment = new StringBuilder();

        xmlFragment.append(addOpenTag("Versions"));
        for (XmlStringBuilder builder : childBuilders) {
            xmlFragment.append(builder.build());
        }
        xmlFragment.append(addCloseTag("Versions"));

        return xmlFragment.toString();

    }
}
