package by.pavlov.drugsapp.service.xml_builder;

import by.pavlov.drugsapp.entity.Dosage;

public class DosageXmlBuilder extends AbstractXmlStringBuilder {
    private final Dosage dosage;

    public DosageXmlBuilder(Dosage dosage) {
        this.dosage = dosage;
    }

    @Override
    public String build() {

        StringBuilder xmlFragment = new StringBuilder();
        xmlFragment.append(addOpenTag("Dosage"));
        xmlFragment.append(addElement("DosageUnit", dosage.getUnits().getShortDescription()));
        xmlFragment.append(addElement("DosageQty", dosage.getQuantity().toString()));
        xmlFragment.append(addElement("UsageDescription", dosage.getUsageDescription()));
        xmlFragment.append(addCloseTag("Dosage"));

        return xmlFragment.toString();
    }
}
