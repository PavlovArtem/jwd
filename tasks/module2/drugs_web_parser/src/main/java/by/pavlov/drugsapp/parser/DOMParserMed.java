package by.pavlov.drugsapp.parser;

import by.pavlov.drugsapp.entity.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DOMParserMed implements XmlParser<Medicine> {

    public static final Logger logger = LogManager.getLogger(DOMParserMed.class);


    @Override
    public List parse(InputStream stream) throws XmlParseException {

//        XmlValidator validator = new XmlValidator();
//        if (!validator.validate(stream)){
//            throw new XmlParseException();
//        }

        List<Medicine> medicines = new ArrayList<>();


        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(stream);

            document.getDocumentElement().normalize();
            NodeList nodes = document.getElementsByTagName("Medicine");
            for (int i = 0; i < nodes.getLength(); i++) {

                Medicine.Builder medBuilder = new Medicine.Builder();


                Node node = nodes.item(i);

                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;

                    medBuilder.id(Long.parseLong(element.getAttribute("id")));
                    medBuilder.name(element.getElementsByTagName("Name").item(0).getTextContent());
                    Pharm pharm = new Pharm();
                    pharm.setName(element.getElementsByTagName("PharmName").item(0).getTextContent());
                    pharm.setCountry(element.getElementsByTagName("Country").item(0).getTextContent());
                    medBuilder.pharm(pharm);
                    medBuilder.group(Group.valueOf(element.getElementsByTagName("Group")
                            .item(0)
                            .getTextContent()
                            .toUpperCase()));

                    List<String> analogs = new ArrayList<>();
                    NodeList aNodes = element.getElementsByTagName("Analogs").item(0).getChildNodes();
                    for (int j = 0; j < aNodes.getLength(); j++) {
                        Node node1 = aNodes.item(j);
                        if (node1.getNodeType() == Node.ELEMENT_NODE) {
                            analogs.add(node1.getTextContent());
                        }
                    }
                    medBuilder.analogNames(analogs);

                    List<MedicineVersion> versions = getMedicineVersions(element);
                    medBuilder.medicineVersion(versions);


                    medicines.add(medBuilder.build());
                }
            }


        } catch (ParserConfigurationException | SAXException | IOException e) {
            logger.error("Error in dom parser", e);
            throw new XmlParseException(e);
        }


        return medicines;
    }

    private List<MedicineVersion> getMedicineVersions(Element element) throws XmlParseException, SAXException {
        List<MedicineVersion> versions = new ArrayList<>();
        NodeList vNodes = element.getElementsByTagName("Versions").item(0).getChildNodes();
        for (int j = 0; j < vNodes.getLength(); j++) {
            MedicineVersion medicineVersion = null;

            Node vNode = vNodes.item(j);
            if (vNode.getNodeType() == Node.ELEMENT_NODE) {
                Element versionElement = (Element) vNode;

                if ("Pills".equalsIgnoreCase(versionElement.getTagName())) {
                    Pills pills = new Pills();
                    pills.setColor(versionElement.getAttribute("color"));
                    pills.setShape(versionElement.getAttribute("shape"));
                    medicineVersion = pills;
                } else if ("Liquid".equalsIgnoreCase(versionElement.getTagName())) {
                    Liquid liquid = new Liquid();
                    liquid.setConsistence(versionElement.getAttribute("consistence"));
                    medicineVersion = liquid;
                } else {
                    break;
                }

                medicineVersion.setVersionName(versionElement
                        .getElementsByTagName("MedicineType")
                        .item(0)
                        .getTextContent());

                Certificate certificate = new Certificate();
                certificate.setNumber(versionElement
                        .getElementsByTagName("Number")
                        .item(0)
                        .getTextContent());
                try {
                    certificate.setIssueDate(new SimpleDateFormat("yyyy-MM-dd").parse(element
                            .getElementsByTagName("IssueDate")
                            .item(0)
                            .getTextContent()));
                    certificate.setExpiredDate(new SimpleDateFormat("yyyy-MM-dd").parse(element
                            .getElementsByTagName("ExpiredDate")
                            .item(0)
                            .getTextContent()));
                } catch (ParseException e) {
                    logger.error("An error occured while parsing date", e);
                    throw new XmlParseException("Invalid date");
                }
                certificate.setCertificationOrganization(element
                        .getElementsByTagName("CertificationOrganization")
                        .item(0)
                        .getTextContent());
                medicineVersion.setCertificate(certificate);

                Packaging packaging = new Packaging();
                packaging.setPackageType(PackageType.valueOf(
                        versionElement
                                .getElementsByTagName("PackageType")
                                .item(0).getTextContent().toUpperCase()));
                String unitStr = versionElement.getElementsByTagName("Unit").item(0).getTextContent();
                packaging.setUnits(Arrays.stream(Units.values())
                        .filter(e -> e.getShortDescription().equalsIgnoreCase(unitStr))
                        .findFirst()
                        .orElseThrow(() -> new SAXException("incorrect units value")));
                packaging.setQuantity(
                        Double.parseDouble(
                                versionElement.getElementsByTagName("Qty")
                                        .item(0)
                                        .getTextContent()));

                packaging.setPrice(BigDecimal.valueOf
                        (Double.parseDouble(versionElement
                                .getElementsByTagName("Price")
                                .item(0)
                                .getTextContent())));
                medicineVersion.setPackaging(packaging);

                Dosage dosage = new Dosage();
                String dosageUnitsStr = versionElement.getElementsByTagName("DosageUnit").item(0).getTextContent();
                dosage.setUnits(Arrays.stream(Units.values())
                        .filter(e -> e.getShortDescription().equalsIgnoreCase(dosageUnitsStr))
                        .findFirst()
                        .orElseThrow(() -> new SAXException("incorrect units value")));
                dosage.setQuantity(Double.parseDouble(versionElement
                        .getElementsByTagName("DosageQty")
                        .item(0)
                        .getTextContent()));
                dosage.setUsageDescription(element
                        .getElementsByTagName("UsageDescription")
                        .item(0)
                        .getTextContent());
                medicineVersion.setDosage(dosage);
            }


            if (medicineVersion != null) {
                versions.add(medicineVersion);
            }

        }
        return versions;
    }
}
