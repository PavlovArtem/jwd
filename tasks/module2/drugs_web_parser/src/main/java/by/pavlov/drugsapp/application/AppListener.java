package by.pavlov.drugsapp.application;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class AppListener implements ServletContextListener {

    public static final Logger LOGGER = LogManager.getLogger(AppListener.class);


    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        ApplicationContext.getInstance().initialize();

    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        ApplicationContext.getInstance().destroy();
    }
}
