package by.pavlov.drugsapp.service.xml_builder;

import java.util.List;

public class AnalogsXmlBuilder extends AbstractXmlStringBuilder {

    private final List<String> analogs;

    public AnalogsXmlBuilder(List<String> analogs) {
        this.analogs = analogs;
    }

    @Override
    public String build() {
        StringBuilder result = new StringBuilder();
        if (analogs.isEmpty()) {
            return addElement("Analogs", "");
        }

        result.append(addOpenTag("Analogs"));
        for (String analog : analogs) {
            result.append(addElement("AnalogName", analog));
        }
        result.append(addCloseTag("Analogs"));

        return result.toString();

    }
}
