package by.pavlov.drugsapp.parser;

import java.util.EnumMap;
import java.util.Map;

public class ParserProviderImpl implements ParserProvider {

    private final Map<ParserType, XmlParser> parserMap;

    public ParserProviderImpl() {
        parserMap = new EnumMap<>(ParserType.class);
    }

    @Override
    public void register(ParserType parserType, XmlParser xmlParser) {
        parserMap.put(parserType, xmlParser);
    }

    @Override
    public void remove(ParserType parserType) {
        parserMap.remove(parserType);
    }

    @Override
    public XmlParser getParser(ParserType parserType) {
        return this.parserMap.get(parserType);
    }
}
