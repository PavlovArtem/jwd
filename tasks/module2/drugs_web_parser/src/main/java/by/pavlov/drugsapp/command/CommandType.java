package by.pavlov.drugsapp.command;

import java.util.Optional;
import java.util.stream.Stream;

public enum CommandType {
    DISPLAY_MEDICINES,
    UPLOAD_FILE,
    DOWNLOAD_XML,
    MAIN_PAGE;

    public static Optional<CommandType> of(String name) {
        return Stream.of(CommandType.values())
                .filter(commandType -> commandType.name()
                        .equalsIgnoreCase(name))
                .findFirst();
    }


}
