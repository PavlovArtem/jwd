package by.pavlov.drugsapp.parser;


import by.pavlov.drugsapp.entity.*;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@RunWith(JUnit4.class)
public class ParsersTest {


    @Test
    public void parseTest(){
        InputStream inputStream = ClassLoader.getSystemResourceAsStream("medicinsXml.xml");
        int expectedSize = 2;
        XmlParser parser = new DOMParserMed();
        List<Medicine> medicines = new ArrayList<>();
        try {
            medicines = parser.parse(inputStream);
        } catch (XmlParseException e) {
            e.printStackTrace();
        }

        Assert.assertEquals(expectedSize,medicines.size());
    }

    @Test
    public void saxParseTest(){
        InputStream inputStream = ClassLoader.getSystemResourceAsStream("medicinsXml.xml");
        int expectedSize = 2;
        XmlParser parser = new SAXParserMed();
        List<Medicine> medicines = new ArrayList<>();
        try {
            medicines = parser.parse(inputStream);
        } catch (XmlParseException e) {
            e.printStackTrace();
        }

        Assert.assertEquals(expectedSize,medicines.size());
    }

    @Test
    public void StAXParseTest(){
        InputStream inputStream = ClassLoader.getSystemResourceAsStream("medicinsXml.xml");
        int expectedSize = 2;
        XmlParser parser = new StAXParserMed();
        List<Medicine> medicines = new ArrayList<>();
        try {
            medicines = parser.parse(inputStream);
        } catch (XmlParseException e) {
            e.printStackTrace();
        }

        Assert.assertEquals(expectedSize,medicines.size());
    }

}
