package by.pavlov.drugsapp.service;

import by.pavlov.drugsapp.entity.*;
import by.pavlov.drugsapp.service.xml_builder.MedicineXmlBuilderManager;
import by.pavlov.drugsapp.service.xml_builder.MedicinesListXmlBuilder;
import by.pavlov.drugsapp.service.xml_builder.XmlBuilderManager;
import by.pavlov.drugsapp.service.xml_builder.XmlStringBuilder;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RunWith(JUnit4.class)
public class XmlBuilderTest {


    @Test
    public void xmlBuildTest() throws ParseException {

        List<Medicine> medicines = new ArrayList<>();
        medicines.add(new Medicine(
                1L,
                "Cetrine",
                new Pharm(
                        "BorZavod",
                        "Blr"
                ),
                Group.ANTIHISTAMINES,
                Arrays.asList("Loratodin"),
                Arrays.asList(
                        new Pills(
                                "pills",
                                new Certificate(
                                        "asdghg12",
                                        new SimpleDateFormat("yyyy-MM-dd").parse("2018-12-10"),
                                        new SimpleDateFormat("yyyy-MM-dd").parse("2020-12-10"),
                                        "SomeOrg"
                                ),
                                new Packaging(PackageType.BOX,
                                              Units.PIECE,
                                              20d,
                                                new BigDecimal(9.75)
                                        ),
                                new Dosage(
                                        Units.MILLIGRAMS,
                                        10d,
                                        "1 pill in 24 h"
                                ),
                                "Round",
                                "White"
                        )
                )
        ));

        XmlBuilderManager builderManager = new MedicineXmlBuilderManager();

        XmlStringBuilder builder = builderManager.createXmlBuilder(medicines);

        String xml = builder.build();

        Assert.assertTrue(true);
    }


}
