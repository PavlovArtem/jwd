package by.pavlov.drugsapp.command;


import by.pavlov.drugsapp.application.ApplicationContext;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class CommandProviderTest {


    @Test
    public void providerTest() {
        ApplicationContext applicationContext = ApplicationContext.getInstance();

        applicationContext.initialize();

        CommandProvider commandProvider = applicationContext.getBean(CommandProvider.class);

        Command mainPageCommand = commandProvider.getCommand(CommandType.MAIN_PAGE);
        Command uploadCommand = commandProvider.getCommand(CommandType.UPLOAD_FILE);

        Assert.assertTrue(mainPageCommand instanceof MainPageCommand);
        Assert.assertTrue(uploadCommand instanceof UploadFileCommand);

        applicationContext.destroy();
    }


}
