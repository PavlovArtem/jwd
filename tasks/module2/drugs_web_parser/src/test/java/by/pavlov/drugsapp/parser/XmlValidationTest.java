package by.pavlov.drugsapp.parser;

import by.pavlov.drugsapp.validation.XmlValidator;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.InputStream;

@RunWith(JUnit4.class)
public class XmlValidationTest {


    @Test
    public void validationTest(){
        XmlValidator xmlValidator = new XmlValidator();

        InputStream inputStream = ClassLoader.getSystemResourceAsStream("medicinsXml.xml");
        boolean actual = xmlValidator.validate(inputStream);

        Assert.assertTrue(actual);
    }


}
